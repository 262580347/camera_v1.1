#ifndef  _BMP_280_H_
#define  _BMP_280_H_

#include "mytype.h"

#define BMP_SENSOR_STATUS_NORMAL	0
#define BMP_SENSOR_STATUS_ERR		1

typedef enum
{
	BMP_WORKING 	= 0,//???
	BMP_ERROR  		= 1,//?????
	BMP_IDLE 	 	= 2,//?????
	BMP_OTHER    	= 3,//????
}BMP_STATUS;

float GetBMPTemp( void );
float GetBMPPress( void );
float GetBMPAltitude( void );

void BMP280_Init(void);

void StartBMPSensor(void);

extern BMP_STATUS BmpStatus;

#endif



