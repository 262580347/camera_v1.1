#include "main.h"
#include "NB_IOT.h"

#define	NB_OK			0x01
#define	NB_OVERTIME		0x02
#define	NB_ERROR		0x03
#define	NB_MESSAGE		0x04

#define		MAX_NB_BUF  500




static unsigned char NB_RxdBuff[200];

unsigned char NB_SendCmd(char *Cmd, char *Result, unsigned short Timeout, unsigned char Retry, unsigned short WaitTime);

NB_CONNECTSTATUS NB_ConnectStatus = Unattach;

NB_RECSTATUS	NB_RecStatus;

static unsigned char s_Signal = 0;

NB_RECSTATUS GetNB_RecStatus(void)
{
	return NB_RecStatus;
}

void NB_Reset(void)
{
	NB_RESET_LOW();
	delay_ms(120);
	NB_RESET_HIGH();
	delay_ms(120);
}

void NB_Wakeup(void)
{
	NB_WAKEUP_LOW();
	delay_ms(300);
	NB_WAKEUP_HIGH();
}

void NB_PowerKey(void)
{
	NB_POWERKEY_LOW();
	delay_ms(1100);
	delay_ms(1100);
	NB_POWERKEY_HIGH();
	delay_ms(200);
}

void NB_PortInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
			
	GPIO_InitStructure.GPIO_Pin = NB_EN_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(NB_EN_TYPE, &GPIO_InitStructure);

	GPIO_SetBits(NB_EN_TYPE,NB_EN_PIN);
	
	GPIO_InitStructure.GPIO_Pin = NB_POWERKEY_PIN | NB_WAKEUP_PIN | NB_RESET_PIN;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_ResetBits(NB_POWERKEY_TYPE,NB_POWERKEY_PIN);
	GPIO_ResetBits(NB_WAKEUP_TYPE,NB_WAKEUP_PIN);
	GPIO_ResetBits(NB_RESET_TYPE,NB_RESET_PIN);

}
static void OnRecNBData(CLOUD_HDR *pMsg, u8 *data, u8 lenth)
{
	u8 i, sendbuf[100], sendlenth, SetTimeFlag = FALSE, ReturnVal = 0;
	CLOUD_HDR *hdr;
	u32 RTCTime = 0;
	RTC_TimeTypeDef RTC_TimeStructure;	
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
	//解析协议包
	u1_printf("\r\n [%02d:%02d:%02d][NB]<-Version:%02d Device:(%d)%04d Dir:%X Seq:%d Len:%d CMD:%02X Data:", \
	RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, pMsg->protocol, ((pMsg->device_id)&0x10000000) >> 28, (pMsg->device_id)&0xfffffff, pMsg->dir, pMsg->seq_no, pMsg->payload_len, pMsg->cmd);
	for(i=0; i<lenth; i++)
		u1_printf("%02X ", data[i]);
	u1_printf("\r\n");
	
	if(pMsg->cmd == CMD_HEATBEAT)
	{
		NB_RecStatus.AliveAckFlag = TRUE;
		RTC_TimeStructure.RTC_H12     = RTC_H12_AM;				
		RTC_TimeStructure.RTC_Hours   = data[2];
		RTC_TimeStructure.RTC_Minutes = data[1];
		RTC_TimeStructure.RTC_Seconds = data[0];;
		u1_printf(" 服务器时间:[%02d:%02d:%02d]\r\n", data[2], data[1], data[0]);
		RTCTime = data[2]*3600 + data[1]*60 + data[0];
		
		if(RTCTime > GetRTCSecond())		//与服务器误差超过120S则同步服务器时间
		{
			if(DifferenceOfRTCTime(RTCTime, GetRTCSecond()) > 20)					
			{
				SetTimeFlag = TRUE;
			}
			else 
			{
				SetTimeFlag = FALSE;
			}
		}
		else
		{
			if(DifferenceOfRTCTime(GetRTCSecond(), RTCTime) > 20)
			{
				
				SetTimeFlag = TRUE;
			}
			else 
			{
				SetTimeFlag = FALSE;
			}
		}
		
		if(SetTimeFlag)
		{
			if(RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure)  != ERROR)
			{
				u1_printf(" ———同步远程时间成功————\r\n");
				RTC_TimeShow();
			}
			else
			{
				u1_printf("   同步远程时间失败\r\n");
			}
		}
		else
		{
			u1_printf(" ———不同步服务器时间————\r\n");
		}
	}

	if(pMsg->cmd == CMD_CLOSE_SOCKET)	//关闭Socket指令
	{		
		u1_printf(" 收到(%d)服务器关闭Socket命令\r\n", ((int)pMsg->device_id)&0xfffffff);
		
		Mod_Send_Ack(NB_COM, pMsg, data, lenth);
							
		NB_RecStatus.ServerConfigCmdFlag = TRUE;	//收到关闭Socket指令，可以休眠
	}
	
	if(pMsg->cmd == CMD_REPORT_D)
	{
		u1_printf(" 收到服务器上报数据回复,上报数据成功\r\n");
		
		NB_RecStatus.DataAckFlag = TRUE;
	}
	
	if(pMsg->cmd == CMD_GET_CONFIG)//获取设备配置
	{
		Mod_Send_Ack(NB_COM, pMsg, data, lenth);
//		delay_ms(300);		//等待发送完成	
//		u1_printf("\r\n 收到服务端获取配置指令\r\n");		
		System_get_config(NB_COM, pMsg, data, lenth);

	}
	
	if(pMsg->cmd == CMD_SET_CONFIG) //设置设备配置
	{	
		Mod_Send_Ack(NB_COM, pMsg, data, lenth);			
//		u1_printf("\r\n 收到服务端修改配置指令\r\n");
		
		memcpy(sendbuf, pMsg, sizeof(CLOUD_HDR));
		sendlenth = sizeof(CLOUD_HDR);
		
		hdr = (CLOUD_HDR *)sendbuf;
		hdr->protocol = swap_word(hdr->protocol);	
//		hdr->device_id = swap_dword((hdr->device_id) | 0x10000000);
		hdr->device_id = swap_dword((hdr->device_id));
		hdr->seq_no = swap_word(hdr->seq_no);	
		hdr->dir = 1;
		hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG)+4);
		memcpy(&sendbuf[sizeof(CLOUD_HDR)], data, lenth);
		ReturnVal = System_set_config(NB_COM, sendbuf, lenth + sendlenth);
		if(ReturnVal == 0)
		{
			return;
		}
		u1_printf("\r\n 更新设备参数，等待设备重启\r\n");
		delay_ms(1500);
		delay_ms(1500);
		while (DMA_GetCurrDataCounter(DMA1_Channel4));
		Sys_Soft_Reset();
	}
}
	
static void WakeUpNBModules(void)
{
	NB_Wakeup();
	u3_printf("AT\r\n");
	delay_ms(200);
}

static unsigned char NB_SendCmd(char *Cmd, char *Result, unsigned short Timeout, unsigned char Retry, unsigned short WaitTime)
{
	static unsigned char s_State = 1, ErrCount = 0;
	static u16 s_LastTime = 0;
	u8 s_return = 0;
	
	switch(s_State)
	{
		case 1:
			u3_printf("%s", Cmd);
//			u1_printf("%s", Cmd);
			s_LastTime = GetSystem10msCount();
			if(Retry == 0)
			{
				s_return = NB_OK;
			}
			else
			{
				s_State++;
			}
		break;
		
		case 2:
			if((strstr((const char *)g_USART3_RX_BUF, "OK")) != NULL)
			{
				s_State = 4;
				s_LastTime = GetSystem10msCount();
				if(WaitTime == 0)
				{
					s_State = 1;
					ErrCount = 0;
					s_return = NB_OK;
				}
			}
			else if((strstr((const char *)g_USART3_RX_BUF, Result)) != NULL)
			{
				s_State = 4;
				s_LastTime = GetSystem10msCount();
				if(WaitTime == 0)
				{
					s_State = 1;
					ErrCount = 0;
					s_return = NB_MESSAGE;
				}
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= Timeout) 
			{
				s_State++;
				ErrCount++;
			}
		break;
			
		case 3:
			if(ErrCount < Retry)
			{
				s_State = 1;
			}
			else
			{
				s_State = 1;
				ErrCount = 0;
				s_return = NB_OVERTIME;
			}
		break;
			
		case 4:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= WaitTime) 
			{
				s_State = 1;
				ErrCount = 0;
				s_return = NB_OK;
			}
		break;
	}
	
	return s_return;
}

void NBIOT_Process(void)	//NB进程1 接收进程
{
	static u16 s_PackCount = 0, s_PackStartAddr[10], s_PackEndAddr[10];
	unsigned char i = 0, PackStartCount = 0, PackEndCount = 0;
	u16  nTemp, RecPackNum = 0, PackLength = 0, RecLength = 0;
	char buf[5], *p1;
	CLOUD_HDR *hdr;
	unsigned char PackBuff[MAX_NB_BUF], DataPack[50];
		
	if(g_Uart3RxFlag == TRUE)
	{
		g_Uart3RxFlag = FALSE;

		u1_printf("%s", g_USART3_RX_BUF);			
		
		if(g_ComTestFlag == FALSE)
		{
			NBIOT_Process2();
		}
		
		if((strstr((const char *)g_USART3_RX_BUF,"+CPIN: READY")) != NULL )
		{
			u1_printf(" M5311 Ready\r\n");
			NB_RecStatus.NB_InitFlag = TRUE;
			NB_ConnectStatus = Unattach;
		}
		else if((p1 = strstr((const char *)g_USART3_RX_BUF,"+CEREG:")) != NULL )
		{
//			nTemp = atoi( (const char *) (p1+10));
//			if(nTemp == 1 || nTemp == 5)
//			{
//				if(NB_ConnectStatus == Unattach)
//				{
//					NB_ConnectStatus = Attach;
//				}
//			}
//			else
//			{
//				NB_ConnectStatus = Unattach;
//			}
		}
		else if((p1 = strstr((const char *)g_USART3_RX_BUF,"+CSQ:")) != NULL )
		{
			strncpy(buf,p1+6,2);
			s_Signal = atoi( buf);
		}
		else if((strstr((const char *)g_USART3_RX_BUF,"+IP:")) != NULL )
		{
			u1_printf(" NB 获取IP成功\r\n");
			NB_ConnectStatus = Attach;
			delay_ms(100);
		}
		else if((strstr((const char *)g_USART3_RX_BUF,"CONNECT FAIL")) != NULL )
		{
			u1_printf(" NB 连接失败\r\n");
			NB_ConnectStatus = NotConnected;
		}
		else if((strstr((const char *)g_USART3_RX_BUF,"+IPCLOSE:")) != NULL )
		{
			u1_printf(" NB TCP连接被关闭\r\n");
			NB_ConnectStatus = NotConnected;
		}
		else if((strstr((const char *)g_USART3_RX_BUF,"CONNECT OK")) != NULL )
		{
			u1_printf(" NB TCP连接成功\r\n");
			NB_ConnectStatus = Connected;
		}
		else if((strstr((const char *)g_USART3_RX_BUF,"CONNECTED")) != NULL )
		{
			u1_printf(" NB TCP已连接\r\n");
			NB_ConnectStatus = Connected;
		}
		else if((strstr((const char *)g_USART3_RX_BUF,"CONNECTING")) != NULL )
		{
			u1_printf(" NB TCP正在连接\r\n");
			NB_ConnectStatus = Connecting;
		}
		else if((strstr((const char *)g_USART3_RX_BUF,"+IPSTATUS: 0,")) != NULL )
		{
			u1_printf(" NB TCP连接已关闭\r\n");
			NB_ConnectStatus = NotConnected;
		}
		else if((strstr((const char *)g_USART3_RX_BUF,"CLOSED")) != NULL )
		{
			u1_printf(" NB TCP连接已关闭\r\n");
			NB_ConnectStatus = NotConnected;
		}
		else if((strstr((const char *)g_USART3_RX_BUF,"+CME ERROR:")) != NULL )
		{
//			u1_printf(" NB TCP连接已关闭\r\n");
//			NB_ConnectStatus = NotConnected;
		}
		else if((p1 = strstr((const char *)g_USART3_RX_BUF,"+IPRD: 0,")) != NULL )//收到一帧数据包
		{
			RecPackNum = atoi( (const char *) (p1+9));
			p1 = strstr((const char *)g_USART3_RX_BUF,",7E");
			p1++;

			if(RecPackNum < MAX_NB_BUF)
			{
				for(i=0; i<RecPackNum; i++)
				{
					StrToHex(&PackBuff[i], (u8 *)(p1+i*2), RecPackNum);	//将接收到的字符串数据转换为hex
				}	
			}
			else
			{
				u1_printf(" NB Data Count Over\r\n");
				return;
			}
			
			PackStartCount = 0;
			PackEndCount = 0;
			
			for(i=0; i<RecPackNum; i++)
			{
				if(PackBuff[i] == 0x7E)	//寻找包头个数
				{
					s_PackStartAddr[PackStartCount] = i;
					PackStartCount++;
				}
				else if(PackBuff[i] == 0x21)	//寻找包尾个数
				{
					s_PackEndAddr[PackEndCount] = i;
					PackEndCount++;
				}
			}
			
			if(PackStartCount == PackEndCount)	//是完整的数据包
			{
				s_PackCount = PackStartCount;
			}
			else
			{
				u1_printf(" PackStartCount != PackEndCount\r\n");
				return;
			}
			
			if(s_PackCount >= 1 && s_PackCount <= 10)	//最多处理10个数据连包
			{
				for(i=0; i<s_PackCount; i++)
				{
					memset(NB_RxdBuff, 0 ,sizeof(NB_RxdBuff));
					UnPackMsg(PackBuff+s_PackStartAddr[i]+1, s_PackEndAddr[i] - s_PackStartAddr[i] - 1, NB_RxdBuff, &PackLength);	//解包
					
					if (NB_RxdBuff[(PackLength)-1] == Calc_Checksum(NB_RxdBuff, (PackLength)-1))
					{
						hdr = (CLOUD_HDR *)NB_RxdBuff;
						hdr->protocol = swap_word(hdr->protocol);
						hdr->device_id = swap_dword(hdr->device_id);
						hdr->seq_no = swap_word(hdr->seq_no);
						hdr->payload_len = swap_word(hdr->payload_len);
						
						memcpy(DataPack, &NB_RxdBuff[sizeof(CLOUD_HDR)], hdr->payload_len);
						RecLength = hdr->payload_len + sizeof(CLOUD_HDR) + 1;
									
						if (RecLength != (PackLength))
						{
							u1_printf(" Pack Length Err\r\n");
						}
						else
						{
							OnRecNBData(hdr, DataPack, hdr->payload_len);	//正确的数据包
						}	
					}		
				}				
			}
			else
			{
				u1_printf(" Packs too much\r\n");
				return;
			}
			
			NB_ConnectStatus = Connected;

		}	
		Clear_Uart3Buff(); 	
	}					
}

static void DetSignal(void)	//信号检测
{
	static u16 s_LastTime = 0;
	u8 err;
	
	if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 6000) 
	{
		err = NB_SendCmd("AT+CSQ\r\n", "+CSQ:", 100, 3,30);
		if(err == NB_OK)
		{
			NB_RecStatus.EnterSleepFlag = TRUE;
			s_LastTime = GetSystem10msCount();
		}
		else if(err == NB_OVERTIME)
		{
			s_LastTime = GetSystem10msCount();
			u1_printf(" CSQ无响应\r");
		}		
	}
}

static void EnterSleepMode(void)
{
	if(NB_RecStatus.EnterSleepFlag == TRUE)
	{
		NB_RecStatus.EnterSleepFlag = FALSE;
		u3_printf("AT*ENTERSLEEP\r\n");		
	}
}

void NBIOT_Process2(void)	//NB进程2，发送进程
{
	static u8 RunOnce = FALSE;
	u8 err = 0;
	static NB_CONNECTSTATUS s_LastStatus = Unattach;
	static u16 s_AliveCount = 0, s_TCPCount = 0, s_DataCount, s_WaitingTime, s_WaitCount;
	static u32 s_LastConnectTime, s_LastTCPTime, s_LastTimeData, s_LastTimeAlive, s_OverTime;
	static char str[50];
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;
	static unsigned char s_NBStatus = 1;
	
	if(RunOnce == FALSE)
	{	
		NB_ConnectStatus = Unattach;
		
		RunOnce = TRUE;
				
		u1_printf("\r\n Wait for NB Run...\r\n");		
		
		NB_PortInit();
		
		NB_PowerKey();
		
		USART3_Config(115200);		
				
		DMA_Usart3_Config();
		
		DMAUsart3RxConfig();
		
		u1_printf("\r\n Init NB IoT Finsh\r\n");	

		s_LastTimeData = GetRTCSecond();	
		
		s_LastTimeAlive = GetRTCSecond();	
		
		s_LastConnectTime = GetRTCSecond();
	}
	
	if(NB_ConnectStatus == Connected)
	{
		if(DifferenceOfRTCTime(GetRTCSecond(), s_LastConnectTime) >= 1)	
		{
			s_LastConnectTime = GetRTCSecond();
		}
	}
	else
	{
		if(DifferenceOfRTCTime(GetRTCSecond(), s_LastConnectTime) >= 1200)	
		{
			s_LastConnectTime = GetRTCSecond();
			s_NBStatus = 1;
			NB_ConnectStatus = Unattach;
			u1_printf(" NB 20min未连接，重启模块\r\n");
		}
	}
	
	EnterSleepMode();
	
	switch(s_NBStatus)
	{
		case 1:		//复位模组
			err = NB_SendCmd("AT+CMRB\r\n", "REBOOTING", 100, 3,100);
			NB_RecStatus.NB_InitFlag = FALSE;
			NB_ConnectStatus = Unattach;
			if(err == NB_OK)
			{
				memset(&NB_RecStatus, 0, sizeof(NB_RecStatus));
				s_NBStatus = 3;
			}
			else if(err == NB_MESSAGE)
			{
				s_NBStatus = 3;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 3;
				u1_printf(" RESET无响应,重启电源\r\n");
				NB_POWER_OFF();
				delay_ms(1000);
				delay_ms(1000);
				delay_ms(1000);
				NB_POWER_ON();
				delay_ms(500);
				NB_PowerKey();
			}
		break;
		
		case 2:
			//发送进程进入空闲态，检测状态
			if(NB_ConnectStatus == NotConnected)
			{
				if(s_LastStatus != NB_ConnectStatus)
				{
					s_LastStatus = NB_ConnectStatus;
					s_NBStatus = 15;
				}
				
			}
			else if(NB_ConnectStatus == Connecting)
			{
				if(s_LastStatus != NB_ConnectStatus)
				{
					s_LastStatus = NB_ConnectStatus;
				}
				
				if(CalculateTime(GetSystem10msCount(), s_WaitingTime) >= 600) 
				{
					s_WaitCount++;
					s_WaitingTime = GetSystem10msCount();
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
					u1_printf("\r\n [%0.2d:%0.2d:%0.2d]", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
					u3_printf("AT+CSQ\r\n");		
					delay_ms(50);					
					u3_printf("AT*ENGINFO=0\r\n");		
				}
				else if(s_WaitCount >= 15)
				{
					s_NBStatus = 1;
					s_WaitCount = 0;
					s_WaitingTime = GetSystem10msCount();
					u1_printf(" 等待IP超时，重启模块\r\n");
				}
			}
			else if(NB_ConnectStatus == Connected)
			{
				if(s_LastStatus != NB_ConnectStatus)
				{
					s_LastStatus = NB_ConnectStatus;
					
					s_NBStatus = 30;
				}
			}
			else if(NB_ConnectStatus == Unattach)
			{
				if(CalculateTime(GetSystem10msCount(), s_WaitingTime) >= 600) 
				{
					s_WaitCount++;
					s_WaitingTime = GetSystem10msCount();
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
					u1_printf("\r\n [%0.2d:%0.2d:%0.2d]", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);	
					u3_printf("AT+CSQ\r\n");		
					delay_ms(50);					
					u3_printf("AT*ENGINFO=0\r\n");				
					
				}
				else if(s_WaitCount >= 15)
				{
					s_NBStatus = 1;
					s_WaitCount = 0;
					s_WaitingTime = GetSystem10msCount();
					u1_printf(" 等待IP超时，重启模块\r\n");
				}
				
				if(s_LastStatus != NB_ConnectStatus)
				{
					s_LastStatus = NB_ConnectStatus;
					s_NBStatus = 3;
				}
			}
			else if(NB_ConnectStatus == Attach)
			{
				if(s_LastStatus != NB_ConnectStatus)
				{
					s_LastStatus = NB_ConnectStatus;
					s_NBStatus = 15;
					s_WaitCount = 0;
				}
			}
		break;
		
		case 3:		//测试模块AT应答
			err = NB_SendCmd("AT\r\n", "OK", 100, 3, 50);
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" AT无响应\r");
			}
		break;
		
		case 4:		//开启或关闭回显
			err = NB_SendCmd("ATE1\r\n", "OK", 100, 3,30);
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" ATE无响应\r");
			}
		break;
			
		case 5:		//设置波特率
			err = NB_SendCmd("AT+IPR=115200\r\n", "OK", 100, 3,30);
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" WAKETIME无响应\r");
			}
		break;


		case 6:		//设置WAKEUP
			err = NB_SendCmd("AT+CMSYSCTRL=1,0\r\n", "OK", 100, 3,30);
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" CMSYSCTRL Err\r\n");
			}
		break;
			
		case 7:		//设置STATUS
			err = NB_SendCmd("AT+CMSYSCTRL=0,0\r\n", "OK", 100, 3,30);
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" CMSYSCTRL Err\r\n");
			}
		break;
		
		case 8:		//设置接收数据格式
			err = NB_SendCmd("AT+IPRCFG=1,0,1\r\n", "OK", 100, 3,30);
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" 设置接收格式失败\r\n");
			}
		break;
		
		case 9:		//设置PSM
			err = NB_SendCmd("AT+CPSMS=1,,,\"00101111\",\"00001000\"\r\n", "OK", 100, 3,30);//	3214	3324
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" 设置接收格式失败\r\n");
			}
		break;
			
		case 10:		//设置eDRX
			err = NB_SendCmd("AT*EDRXCFG=1,5,\"0010\",\"0001\"\r\n", "+CME ERROR", 100, 3,30);	//eDRX   PTW	
//			err = NB_SendCmd("AT*EDRXCFG=0\r\n", "+CME ERROR", 100, 3,30);	//eDRX   PTW	
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_MESSAGE)
			{
				s_NBStatus++;
				u1_printf(" 设置eDRX失败，参数错误\r\n");
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" 设置eDRX失败\r\n");
			}
		break;
			
		case 11:
			err = NB_SendCmd("AT+CEREG?\r\n", "OK", 100, 3,30);
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" 设置AT+CEREG失败\r\n");
			}
		break;
			
		case 12:
			err = NB_SendCmd("AT+CSQ\r\n", "OK", 100, 3,30);
			if(err == NB_OK)
			{
				u1_printf("\r\n 等待IP...\r\n");
				s_WaitingTime = GetSystem10msCount();
				s_NBStatus = 2;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" AT+CSQ失败\r\n");
			}
		break;
			
		case 15:		//检测Attach状态
			err = NB_SendCmd("AT+CEREG=4\r\n", "+CEREG:", 100, 3,30);
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_MESSAGE)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus++;
				u1_printf(" CMD OverTime15\r\n");
			}

		break;
		
		case 16:		//检测信号
			err = NB_SendCmd("AT+CSQ\r\n", "+CSQ:", 100, 3,50);
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_MESSAGE)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				u1_printf(" CMD OverTime16\r\n");
				s_NBStatus++;
			}
		break;
		
		case 17:
			p_sys = GetSystemConfig();	
			u1_printf("[NB-IOT]建立TCP链接\r\n");
			sprintf(str,"AT+IPSTART=0,\"TCP\",\"%d.%d.%d.%d\",%d\r\n",p_sys->Gprs_ServerIP[0],p_sys->Gprs_ServerIP[1],p_sys->Gprs_ServerIP[2],p_sys->Gprs_ServerIP[3],p_sys->Gprs_Port);	
			WakeUpNBModules();
			s_NBStatus++;
		break;
			
		case 18:	//建立TCP链接
			u3_printf("%s", str);
			s_NBStatus++;
			s_LastTCPTime = GetSystem10msCount();
		break;
		
		case 19:		
			if((strstr((const char *)g_USART3_RX_BUF,"OK")) != NULL )
			{
				s_TCPCount = 0;
				s_WaitCount = 0;
				s_WaitingTime = GetSystem10msCount();
				NB_ConnectStatus = Connecting;

				s_NBStatus++;
				s_LastTCPTime = GetSystem10msCount();
			}
			else if((strstr((const char *)g_USART3_RX_BUF,"operation not allowed")) != NULL )
			{
				s_TCPCount = 0;
				s_WaitCount = 0;
				s_WaitingTime = GetSystem10msCount();
				NB_ConnectStatus = Connecting;
				u1_printf(" TCP Connecting\r\n");
				s_NBStatus++;
				s_LastTCPTime = GetSystem10msCount();
			}
			else if((strstr((const char *)g_USART3_RX_BUF,"CONNECT FAIL!")) != NULL )
			{
//				s_TCPCount++;
//				s_NBStatus = 17;
//				s_LastTCPTime = GetSystem10msCount();
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTCPTime) >= 100) 
			{
				s_LastTCPTime = GetSystem10msCount();
				s_TCPCount++;
				s_NBStatus = 15;
			}
			else if(s_TCPCount >= 3)
			{
				s_TCPCount = 0;
				s_NBStatus = 1;
				NB_ConnectStatus = Unattach;
				u1_printf(" TCP OverTime\r\n");
				s_LastTCPTime = GetSystem10msCount();
			}
			
		break;
		
		case 20:
			
			//检测信噪比
			err = NB_SendCmd("AT*ENGINFO=0\r\n", "*ENGINFOSC:", 100, 3,50);
			if(err == NB_OK)
			{
				s_NBStatus = 2;
				u1_printf(" 等待连接...\r\n");
			}
			else if(err == NB_MESSAGE)
			{
				s_NBStatus = 2;
				u1_printf(" 等待连接...\r\n");
			}
			else if(err == NB_OVERTIME)
			{
				u1_printf(" CMD OverTime16\r\n");
				s_NBStatus = 2;
			}
		break;
		case 30:	//发送心跳包
			if(NB_ConnectStatus == Connected)
			{				
				if(NB_RecStatus.SendAliveFlag)
				{
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
					u1_printf(" [%0.2d:%0.2d:%0.2d]Alive\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
					Mod_Send_Alive(NB_COM);
					s_LastTimeAlive = GetRTCSecond();
					s_NBStatus++;
				}
				else
				{
					s_NBStatus = 32;				
				}
			}
			else
			{
				s_NBStatus = 2;
			}
		break;
		
		case 31:	
			if((strstr((const char *)g_USART3_RX_BUF,"+IPSEND:")) != NULL )
			{
				s_NBStatus++;
				s_AliveCount = 0;
				s_LastTimeAlive = GetRTCSecond();
			}
			else if((strstr((const char *)g_USART3_RX_BUF,"OK")) != NULL )
			{
				s_NBStatus++;
				s_AliveCount = 0;
				s_LastTimeAlive = GetRTCSecond();
			}
			else if(DifferenceOfRTCTime(GetRTCSecond(), s_LastTimeAlive) >= 5)	
			{
				s_LastTimeAlive = GetRTCSecond();
				s_AliveCount++;
				s_NBStatus--;
			}
			else if(s_AliveCount >= 2)
			{
				s_NBStatus++;
				s_AliveCount = 0;
				s_LastTimeAlive = GetRTCSecond();
			}
		break;

		case 32:	//发送数据包
			if(NB_ConnectStatus == Connected)
			{				
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
				u1_printf("\r\n [%0.2d:%0.2d:%0.2d]", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
				Mod_Report_LowPower_Data(NB_COM);
				s_LastTimeData = GetRTCSecond();
				s_NBStatus++;
			}
			else
			{
				s_NBStatus = 2;
			}
		break;
		
		case 33:
			if((strstr((const char *)g_USART3_RX_BUF,"+IPSEND:")) != NULL )
			{
				s_NBStatus++;
				s_LastTimeData = GetRTCSecond();	
				s_OverTime = GetRTCSecond();
				s_DataCount = 0;
			}
			else if((strstr((const char *)g_USART3_RX_BUF,"OK")) != NULL )
			{
				s_NBStatus++;
				s_LastTimeData = GetRTCSecond();	
				s_OverTime = GetRTCSecond();
				s_DataCount = 0;
			}
			else if(DifferenceOfRTCTime(GetRTCSecond(), s_LastTimeData) >= 5)	
			{
				s_LastTimeData = GetRTCSecond();
				s_DataCount++;
				s_NBStatus--;
			}
			else if(s_DataCount >= 2)
			{
				s_NBStatus = 37;
				s_LastTimeData = GetRTCSecond();	
				s_DataCount = 0;
			}
		break;
			
		case 34:
			if(NB_RecStatus.DataAckFlag == TRUE)
			{		
				s_NBStatus++;	
				s_OverTime = GetRTCSecond();				
			}
			else if(DifferenceOfRTCTime(GetRTCSecond(), s_OverTime) >= 20)	
			{
				u1_printf(" 等待应答包超时\r\n");
				s_OverTime = GetRTCSecond();
				s_NBStatus = 37;
			}
		break;
		
		case 35:
			if(NB_RecStatus.ServerConfigCmdFlag == FALSE)
			{
				if(DifferenceOfRTCTime(GetRTCSecond(), s_OverTime) >= 3)
				{
					u1_printf(" 超时未接收0X63\r\n");
					NB_RecStatus.DataAckFlag = FALSE;
					NB_RecStatus.ServerConfigCmdFlag = FALSE;
					s_NBStatus = 37;
					s_LastTimeData = GetRTCSecond();	
				}
				
			}
			else if(NB_RecStatus.ServerConfigCmdFlag == TRUE)
			{
				NB_RecStatus.DataAckFlag = FALSE;
				NB_RecStatus.ServerConfigCmdFlag = FALSE;
				s_NBStatus = 37;
				s_LastTimeData = GetRTCSecond();	
				
			}					
		break;
			
		case 36:
			p_sys = GetSystemConfig();
		
			RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);				
			if(DifferenceOfRTCTime(GetRTCSecond(), s_LastTimeAlive) >= p_sys->Heart_interval)	
			{
				if(DifferenceOfRTCTime(GetRTCSecond(), s_LastTimeData) >= p_sys->Data_interval)	
				{				
					s_OverTime = GetRTCSecond();
					u1_printf(" Time To Alive\r\n");
					s_LastTimeAlive = GetRTCSecond();
					NB_RecStatus.SendAliveFlag = TRUE;
				}
			}
			
			if(NB_RecStatus.AliveAckFlag == TRUE)
			{
				NB_RecStatus.AliveAckFlag = FALSE;
				NB_RecStatus.SendAliveFlag = FALSE;
				s_LastTimeAlive = GetRTCSecond();
			}
				
			if(DifferenceOfRTCTime(GetRTCSecond(), s_LastTimeData) >= p_sys->Data_interval)	
			{
				s_LastTimeData = GetRTCSecond();	
				s_OverTime = GetRTCSecond();
				NB_RecStatus.STM32StopFlag = FALSE;
				if(NB_ConnectStatus == NotConnected)
				{	
					s_NBStatus = 15;
					WakeUpNBModules();
				}
				else
				{
					s_NBStatus = 2;
				}
			}
			else
			{
				if(NB_RecStatus.STM32StopFlag)
				{
					if(g_PowerDetectFlag && g_SHTGetFlag && g_MAXGetFlag && g_BMPGetFlag)
					{
						SetStopModeTime(TRUE, STOP_TIME);
					}
				}
			}		
		break;
					
		case 37:	//关闭TCP链接
			err = NB_SendCmd("AT+IPCLOSE=0\r\n", "+CME ERROR", 100, 3, 50);
			if(err == NB_OK)
			{
				s_NBStatus++;
				NB_ConnectStatus = NotConnected;
			}
			else if(err == NB_MESSAGE)
			{
				u1_printf(" TCP已关闭.\r\n");
				s_NBStatus++;		
				NB_ConnectStatus = NotConnected;
				
			}
			else if(err == NB_OVERTIME)
			{
				u1_printf(" CMD OverTime35\r\n");
				s_NBStatus++;
				NB_ConnectStatus = NotConnected;
				
			}
		break;
		
		case 38:
			err = NB_SendCmd("AT*ENTERSLEEP\r\n", "OK", 100, 3, 50);
			if(err == NB_OK)
			{
				s_NBStatus = 36;
				NB_RecStatus.STM32StopFlag = TRUE;
			}
			else if(err == NB_OVERTIME)
			{
				u1_printf(" CMD OverTime36\r\n");
				s_NBStatus = 36;
				NB_RecStatus.STM32StopFlag = TRUE;
				
			}	
		break;
		
	}
}









