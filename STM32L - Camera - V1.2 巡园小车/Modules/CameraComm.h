#ifndef __CAMERACOMM_H__
#define __CAMERACOMM_H__

#include "mytype.h"

//应用层用户数据区缓冲大小
#define COMM_DATA_SIZE    64
#define COMM_PHYDATA_SIZE (1024+8)
#define COMM_PHY_ID1      0x7E	//物理层帧ID标识符1
#define COMM_PHY_ID2      0x21	//物理层帧ID标识符2
//使用发送环形FIFO机制
#define ENABLE_TX_FIFO       1      //使用发送环形FIFO机制
#define TXFIFO_USER_SIZE     32     //环形FIFO用户区数据最大长度
#define TXFIFO_WAIT_MSG_TIME 11000//发送FIFO等待报文超时重试时间:单位:任务周期
#define TXFIFO_RETRY_TIME    5      //发送FIFO等待超时重试次数

 
//协议栈对象索引
typedef enum
{
	CAMERA_COMM0 = 0,
	CAMERA_COMM1,
	COMM_SIZE,	
}CAMERA_COMM_NUMBER;

//应用帧结构
typedef struct
{
	unsigned short  Protocol;
	unsigned int  DeviceID;
	unsigned char  Dir;
	unsigned short Seq;
	unsigned short Length;	
	unsigned char  OPType;
	unsigned char  UserBuf[COMM_DATA_SIZE];	
}CAMERA_COMM_MSG; 

#if ENABLE_TX_FIFO
//环形发送FIFO应用帧结构
typedef struct
{
	unsigned short  Protocol;
	unsigned int  DeviceID;
	unsigned char  Dir;
	unsigned short Seq;
	unsigned short Length;	
	unsigned char  OPType;
	unsigned char  UserBuf[COMM_DATA_SIZE];	
}TXFIFO_COMM_MSG; 
#endif

//接收观察者原型
typedef void (*CAMERACOMMRXOBSER)(CAMERA_COMM_MSG *pMsg);

typedef struct
{
	//注册接收报文观察者
	void (*AddRxObser)(HANDLE hComBus,CAMERACOMMRXOBSER hRxObser);
	//发送报文
	void (*SendMsg)(CAMERA_COMM_MSG *pMsg);
	//注销资源
    void (*Remove)(void);
//	//发送报文[m_TxFrame成员]
//	void (*SendMessage)(void);
#if ENABLE_TX_FIFO
	//注册一个发送FIFO空事件观察者
	void (*AddTxFIFONULLObser)(BASEOBSER hObser);
    //使用环形FIFO发送报文
    BOOL (*SendMsgbyFIFO)(CAMERA_COMM_MSG *pMsg);
	//报文等待超时重试时间
	UINT m_dwReTime;
#endif
	//报文成员结构
	CAMERA_COMM_MSG m_TxFrame;
}CameraComm;

//获取协议栈实例句柄 
CameraComm *CameraCommGet(void);


#endif

