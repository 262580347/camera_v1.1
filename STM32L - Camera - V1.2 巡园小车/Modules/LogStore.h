#ifndef		_LOGSTORE_H_
#define		_LOGSTORE_H_

#define		LOG_CODE_CLEAR				0XFF	//清错误码
#define		LOG_CODE_NOMODULE			0		//模块不存在
#define		LOG_CODE_WEAKSIGNAL			1		//信号弱
#define		LOG_CODE_CONSOLEON			2		//云台启动标记
#define		LOG_CODE_TIMEOUT			3		//连接超时
#define		LOG_CODE_SUCCESS			4		//上报数据成功
#define		LOG_CODE_POSTFAIL			5		//上一次通信失败
#define		LOG_CODE_PPPFAIL			6		//拨号失败
#define		LOG_CODE_NO_CARD			7		//没有SIM卡


#define		LOG_CODE_PHOTOFAIL			8		//拍照失败
#define		LOG_CODE_NOHI3519			9		//Hi3519不存在
#define		LOG_CODE_NOWORK				10		//不在拍照间隔
#define		LOG_CODE_SENSORERR			11		//传感器数据错误
#define		LOG_CODE_GPS_OK				12		//定位成功
#define		LOG_CODE_GPS_FAIL			13		//定位失败
#define		LOG_CODE_HARDERR			14		//硬件错误
#define		LOG_CODE_START				15		//开始运行标记



void LogStorePointerInit(void);

void SetLogErrCode(unsigned short ErrCode);

void ClearLogErrCode(unsigned short ErrCode);

void StoreOperationalData(void);

void ShowLogContent(void);






#endif

