#include "main.h"
#include "ConsoleDrive.h"


#ifdef		CAMERA_VERSION_V1_1

#define		MOTOR_POWER_PIN		GPIO_Pin_6
#define		MOTOR_POWER_TYPE	GPIOA

#define		RS485_POWER_PIN		GPIO_Pin_1
#define		RS485_POWER_TYPE	GPIOB

#endif

#ifdef		CAMERA_VERSION_V1_2

#define		MOTOR_POWER_PIN		GPIO_Pin_4
#define		MOTOR_POWER_TYPE	GPIOA

#define		RS485_POWER_PIN		GPIO_Pin_1
#define		RS485_POWER_TYPE	GPIOB

#endif




#define		PRESETGOTOTIME		16000	//预置点的旋转等待时间

#define		AUTOPOINTTIME		20000	//自检等待时间

#define		CMD_MOTOR_CONTROL		0x06
#define		CMD_MOTOR_INQUIRE		0x03


static ANGLESTORE_CFG	AngleStoreConfig;

static ANGLESTORE_CFG	*pthis = NULL;

static unsigned char s_BufData[sizeof(ANGLESTORE_CFG)] ;

static u16 s_NowHorAngle = 0, s_NowVerAngle = 0;
	
static u8 s_ConsolePosition = 0;

static u8 s_ConsolePowerFlag = FALSE;	//云台电源开启标志

static unsigned int OverTimerID = 0;		//超时定时器ID

static u8 s_NeedToDetMyself = FALSE;

static void TaskForMotorCommunication(void);

unsigned short GetHorAngle(void)
{
	return s_NowHorAngle;
}

unsigned short GetVerAngle(void)
{
	return s_NowVerAngle;
}

unsigned char GetConsolePosition(void)
{
	return s_ConsolePosition;
}

void SetConsolePosition(unsigned char Point)
{
	s_ConsolePosition = Point;
}

unsigned char GetConsolePowerFlag(void)
{
	return s_ConsolePowerFlag;
}

ANGLESTORE_CFG *GetPhotoConfig(void)
{
	if(pthis != NULL)
	{
		return pthis;		
	}
	else
	{
		return NULL;
	}
}

void SetPhotoConfig(ANGLESTORE_CFG *p_Config)
{
	if(p_Config == NULL)
	{
		return;
	}
	
	pthis->Magic 	= VAILD;
	pthis->Length 	= sizeof(ANGLESTORE_CFG) - 4;

	pthis->PhotoPointNum = p_Config->PhotoPointNum;
	pthis->PhotoInterval = p_Config->PhotoInterval;
	
	if(p_Config->EndPhotoTime[0] >= 24)
	{
		p_Config->EndPhotoTime[0] = 8;
	}
	if(p_Config->EndPhotoTime[1] >= 60)
	{
		p_Config->EndPhotoTime[1] = 0;
	}
	if(p_Config->EndPhotoTime[2] >= 60)
	{
		p_Config->EndPhotoTime[2] = 0;
	}
	
	if(p_Config->StartPhotoTime[0] >= 24)
	{
		p_Config->StartPhotoTime[0] = 18;
	}
	if(p_Config->StartPhotoTime[1] >= 60)
	{
		p_Config->StartPhotoTime[1] = 0;
	}
	if(p_Config->StartPhotoTime[2] >= 60)
	{
		p_Config->StartPhotoTime[2] = 0;
	}
	
	pthis->StartPhotoTime[0] = p_Config->StartPhotoTime[0];
	pthis->StartPhotoTime[1] = p_Config->StartPhotoTime[1];
	pthis->StartPhotoTime[2] = p_Config->StartPhotoTime[2];
	
	pthis->EndPhotoTime[0] = p_Config->EndPhotoTime[0];
	pthis->EndPhotoTime[1] = p_Config->EndPhotoTime[1];
	pthis->EndPhotoTime[2] = p_Config->EndPhotoTime[2];
	
	pthis->Chksum = Calc_Checksum((unsigned char *)pthis->HorizontalAngle, sizeof(ANGLESTORE_CFG) - 4);
	
	EEPROM_EraseWords(PHOTO_BLOCK1);
	EEPROM_EraseWords(PHOTO_BLOCK2);
	EEPROM_WriteBytes(PRESET_ANGLE_1_10, (unsigned char *)pthis, sizeof(ANGLESTORE_CFG));
	
	EEPROM_ReadBytes(PRESET_ANGLE_1_10, s_BufData, sizeof(ANGLESTORE_CFG));
	
	pthis = (ANGLESTORE_CFG *)s_BufData;
	
	AngleStoreConfig = *pthis;
}

unsigned char ConsoleAngleInit(void)
{
	u8 i;
	
	if (Check_Area_Valid(PRESET_ANGLE_1_10))
	{
		EEPROM_ReadBytes(PRESET_ANGLE_1_10, s_BufData, sizeof(ANGLESTORE_CFG));
		
		pthis = (ANGLESTORE_CFG *)s_BufData;
		
		AngleStoreConfig = *pthis;
		
		u1_printf("\r\n [Photo]Point:%d\r\n", pthis->PhotoPointNum);
		u1_printf("\r\n [Photo]Interval:%d\r\n", pthis->PhotoInterval);
		u1_printf("\r\n [Photo]Start Time: %02d:%02d:%02d\r\n", pthis->StartPhotoTime[0], pthis->StartPhotoTime[1], pthis->StartPhotoTime[2]);
		u1_printf("\r\n [Photo]End Time:   %02d:%02d:%02d\r\n\r\n ", pthis->EndPhotoTime[0], pthis->EndPhotoTime[1], pthis->EndPhotoTime[2]);
		for(i=0; i<20; i++)
		{
			u1_printf("%d:(%d,%d) ", i+1, AngleStoreConfig.HorizontalAngle[i], AngleStoreConfig.VerticalAngle[i]);
		}
		u1_printf("\r\n");
		
		return TRUE;
	}
	else	//第一次上电初始化,写入默认值
	{
		AngleStoreConfig.Magic 				= VAILD;
		AngleStoreConfig.Length 			= sizeof(ANGLESTORE_CFG) - 4;
		AngleStoreConfig.HorizontalAngle[0] = 27;
		AngleStoreConfig.VerticalAngle[0]   = 30;
		AngleStoreConfig.HorizontalAngle[1] = 77;
		AngleStoreConfig.VerticalAngle[1]   = 20;
		AngleStoreConfig.HorizontalAngle[2] = 127;
		AngleStoreConfig.VerticalAngle[2]   = 10;
		AngleStoreConfig.HorizontalAngle[3] = 177;
		AngleStoreConfig.VerticalAngle[3]   = 20;
		AngleStoreConfig.HorizontalAngle[4] = 227;
		AngleStoreConfig.VerticalAngle[4]   = 30;
		
		AngleStoreConfig.PhotoPointNum = 5;
		AngleStoreConfig.PhotoInterval = 30;
		AngleStoreConfig.StartPhotoTime[0] = 8;	
		AngleStoreConfig.EndPhotoTime[0] = 18;
		
		AngleStoreConfig.Chksum = Calc_Checksum((unsigned char *)&AngleStoreConfig.HorizontalAngle, sizeof(ANGLESTORE_CFG) - 4);
		EEPROM_WriteBytes(PRESET_ANGLE_1_10, (unsigned char *)&AngleStoreConfig, sizeof(ANGLESTORE_CFG));
		u1_printf(" The first power on Angle reset\r\n");
		
		EEPROM_ReadBytes(PRESET_ANGLE_1_10, s_BufData, sizeof(ANGLESTORE_CFG));
		
		pthis = (ANGLESTORE_CFG *)s_BufData;
		
		AngleStoreConfig = *pthis;
		
		return FALSE;
	}	
}

void ReadStoreAngle(void)
{
	u8 i;
	
	if (Check_Area_Valid(PRESET_ANGLE_1_10))
	{
		EEPROM_ReadBytes(PRESET_ANGLE_1_10, s_BufData, sizeof(ANGLESTORE_CFG));
		if(pthis != NULL)
		{
			pthis = (ANGLESTORE_CFG *)s_BufData;
			AngleStoreConfig = *pthis;
			
			u1_printf(" Store Angle:\r\n");
			for(i=0; i<20; i++)
			{
				u1_printf(" %d,%d ", AngleStoreConfig.HorizontalAngle[i], AngleStoreConfig.VerticalAngle[i]);
			}
			u1_printf("\r\n");
		}
	}
	else
	{
		u1_printf(" Read Angle ErrorStatus\r\n");
	}
}

void WriteNowAngle(unsigned int HorizontalAngle, unsigned int VerticalAngle, unsigned char Num)
{
	
	if(Num >= 20 && Num == 0)
	{
		return;
	}
	if(pthis != NULL)
	{
		pthis->HorizontalAngle[Num-1] = HorizontalAngle;
		pthis->VerticalAngle[Num-1] = VerticalAngle;
		pthis->Chksum = Calc_Checksum((unsigned char *)&pthis->HorizontalAngle, sizeof(ANGLESTORE_CFG) - 4);
		EEPROM_WriteBytes(PRESET_ANGLE_1_10, (unsigned char *)pthis, sizeof(ANGLESTORE_CFG));
	}
}

void SendConsoleCMD(unsigned char ConsoleCMD, unsigned short HorAngle, unsigned short VerAngle)
{
	u8 RS485Buf[10];
	u16 CRCVal = 0;
		
	RS485Buf[0] = 0;
	RS485Buf[1] = CMD_MOTOR_CONTROL;
	RS485Buf[2] = 0;
	RS485Buf[3] = ConsoleCMD;
	RS485Buf[4] = HorAngle >> 8;
	RS485Buf[5] = HorAngle&0xff;
	RS485Buf[6] = VerAngle >> 8;
	RS485Buf[7] = VerAngle&0xff;
	CRCVal = Calculate_CRC16(RS485Buf, 8);
	RS485Buf[8] = CRCVal&0xff;
	RS485Buf[9] = CRCVal >> 8;	
	USART3_DMA_Send(RS485Buf, 10);
}

void ConsoleStop(void)
{
	SendConsoleCMD(MOTOR_STOP, 0, 0);
}

void ConsoleUp(void)
{
	SendConsoleCMD(MOTOR_RUN_UP, 0, 0);
}

void ConsoleDown(void)
{
	SendConsoleCMD(MOTOR_RUN_DOWN, 0, 0);
}

void ConsoleLeft(void)
{
	SendConsoleCMD(MOTOR_RUN_LEFT, 0, 0);
}

void ConsoleRight(void)
{
	SendConsoleCMD(MOTOR_RUN_RIGHT, 0, 0);
}

void ConsoleRecStopAck(void)
{
	SendConsoleCMD(MOTOR_STOP_ACK, 0, 0);
}

void ConsoleRunModeisNormal(void)
{
	SendConsoleCMD(MOTOR_RUN_MODE, 0, 0);
}

void ConsoleRunModeisTest(void)
{
	SendConsoleCMD(MOTOR_RUN_MODE, 0x100, 0);
}

void GotoPresetPoint(unsigned char Num)
{
	if (Check_Area_Valid(PRESET_ANGLE_1_10))
	{
		EEPROM_ReadBytes(PRESET_ANGLE_1_10, s_BufData, sizeof(ANGLESTORE_CFG));
		if(pthis != NULL)
		{
			pthis = (ANGLESTORE_CFG *)s_BufData;
			AngleStoreConfig = *pthis;
			
			u1_printf(" Go to Preset(%d) Angle:%d,%d\r\n", Num, AngleStoreConfig.HorizontalAngle[Num-1], AngleStoreConfig.VerticalAngle[Num-1]);
			SendConsoleCMD(MOTOR_ANGLE, AngleStoreConfig.HorizontalAngle[Num-1], AngleStoreConfig.VerticalAngle[Num-1]);
		}
	}
	else
	{
		u1_printf(" Read Angle ErrorStatus\r\n");
	}
}

void MotorPowerControl(unsigned char IsTrue)
{
	GPIO_InitTypeDef GPIO_InitStructure;
			
	GPIO_InitStructure.GPIO_Pin = MOTOR_POWER_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(MOTOR_POWER_TYPE, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = RS485_POWER_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(RS485_POWER_TYPE, &GPIO_InitStructure);
	
	
	
	#ifdef		CAMERA_VERSION_V1_1

	if(IsTrue)
	{
		GPIO_SetBits(MOTOR_POWER_TYPE,MOTOR_POWER_PIN);
		GPIO_SetBits(RS485_POWER_TYPE,RS485_POWER_PIN);
	}
	else
	{
		GPIO_ResetBits(MOTOR_POWER_TYPE,MOTOR_POWER_PIN);
		GPIO_ResetBits(RS485_POWER_TYPE,RS485_POWER_PIN);
	}

	#endif

	#ifdef		CAMERA_VERSION_V1_2

	if(IsTrue)
	{
		GPIO_ResetBits(MOTOR_POWER_TYPE,MOTOR_POWER_PIN);
		GPIO_SetBits(RS485_POWER_TYPE,RS485_POWER_PIN);
	}
	else
	{
		GPIO_SetBits(MOTOR_POWER_TYPE,MOTOR_POWER_PIN);
		GPIO_ResetBits(RS485_POWER_TYPE,RS485_POWER_PIN);
	}

	#endif
}

static void TimerForPowerOff(void)
{	
	if(g_ComTestFlag == FALSE)	//调试模式不关闭电源
	{
		if(s_NeedToDetMyself)		//自检结束	转至第一个预置点 此分支不关闭定时器
		{
			s_NeedToDetMyself = FALSE;
			
			Task_Create(TaskForMotorCommunication, 15000);
			
			OverTimerID = SetTimer(PRESETGOTOTIME, TimerForPowerOff);	
			
			return;
		}
		else			
		{	
			u1_printf(" Motor Power Off\r\n");
			while (DMA_GetCurrDataCounter(DMA1_Channel4));
			MotorPowerOff();		
		}
	}
	
	KillTimer(OverTimerID);
	
	OverTimerID = 0;
}

static void TaskForMotorCommunication(void)
{
	static u8 s_First = FALSE;
	static u8 s_NextNeedToDet = FALSE;
	static u8 s_RunCount = 0;
	ANGLESTORE_CFG *p_Config;
	
		
	if(s_NextNeedToDet)
	{
		SendConsoleCMD(MOTOR_AUTO, 0, 0);
		
		u1_printf("\r\n Automatic cruise on\r\n");	
		
		delay_ms(100);
		
		s_NeedToDetMyself = TRUE;
		
		s_NextNeedToDet= FALSE;
			
		OverTimerID = SetTimer(AUTOPOINTTIME, TimerForPowerOff);
		
		Task_Kill(TaskForMotorCommunication);
		
		return;
	}
	
	if(!s_First)
	{
		s_First = TRUE;
		
		OverTimerID = SetTimer(AUTOPOINTTIME, TimerForPowerOff);		//40s后关闭云台
			
		SendConsoleCMD(MOTOR_AUTO, 0, 0);
		
		s_NeedToDetMyself = TRUE; 	//为了自检完后转至第一个预置点
			
		u1_printf(" First power on, start automatic cruise\r\n");
	}
	else
	{		
		p_Config = GetPhotoConfig();	
		
		s_ConsolePosition++;
				
		if((s_ConsolePosition > p_Config->PhotoPointNum))
		{
			s_ConsolePosition = 1;
			s_RunCount++;
			u1_printf(" Count:%d\r\n", s_RunCount);
		}
		
		if(s_RunCount >= 10)
		{			
			s_RunCount = 0;
			
			s_NextNeedToDet = TRUE;	//开启一次自检
		}
							
		GotoPresetPoint(s_ConsolePosition);
		
		OverTimerID = SetTimer(PRESETGOTOTIME, TimerForPowerOff);
	}	
	
	Task_Kill(TaskForMotorCommunication);
}

static void ExecutionInstruction(unsigned char *RS485Data)
{
	u8 Cmd = 0, i;
	unsigned int HorAngle = 0, VerAngle = 0;
	
	Cmd = RS485Data[1];
	
	
	u1_printf(" Now Motor Status:");
	switch(RS485Data[3])
	{
		case MOTOR_RUN_UP:
			u1_printf(" Turning Up");
		break;
		
		case MOTOR_RUN_DOWN:
			u1_printf(" Turning Down");
		break;

		case MOTOR_RUN_LEFT:
			u1_printf(" Turning Left");
		break;

		case MOTOR_RUN_RIGHT:
			u1_printf(" Turning Right");
		break;

		case MOTOR_STOP:
			u1_printf(" Stop");
		
//			if(s_NeedToDetMyself)		//自检结束	转至第一个预置点 此分支不关闭定时器
//			{
//				s_NeedToDetMyself = FALSE;			
//				Task_Create(TaskForMotorCommunication, 15000);			
//				OverTimerID = SetTimer(PRESETGOTOTIME, TimerForPowerOff);	
//			}
//			else
//			{
//				OverTimerID = SetTimer(200, TimerForPowerOff);	//500ms后关闭
//				
//			}
			ConsoleRecStopAck();
			STM32ToHi3519GettoPoint();
		break;
		
		case MOTOR_AUTO:
			u1_printf(" Auto Running");
		break;
		
		case MOTOR_ANGLE:
			u1_printf(" Go to Angle");
		break;
		
	}
	
	if(Cmd == CMD_MOTOR_CONTROL)
	{
		u1_printf("\r\n");
	}
	else if(Cmd == CMD_MOTOR_INQUIRE)
	{
		HorAngle = (RS485Data[4] << 8) + RS485Data[5];
		VerAngle = (RS485Data[6] << 8) + RS485Data[7];
		
		s_NowHorAngle = HorAngle;
		s_NowVerAngle = VerAngle;
		u1_printf("    Now Angle: %d,%d\r\n", HorAngle, VerAngle);
	}
	else
	{
		u1_printf("\r\n 485 error: ");

		for(i=0; i<10; i++)
		{
			u1_printf(" %02X", RS485Data[i]);
		}
		u1_printf("\r\n");	
	}
}

void TaskForRecRS485Pack(void)
{
	static u8 DataBuf[20];
	unsigned short CRCVal = 0;
	u8 i;
	
	if(g_Uart3RxFlag)
	{		
		memcpy(DataBuf, g_USART3_RX_BUF, 10);	//只处理一次中断到来的数据
				
		CRCVal = Calculate_CRC16(DataBuf, 8);
		if(CRCVal == ((DataBuf[9] << 8) + DataBuf[8]))
		{
			ExecutionInstruction(DataBuf);		//执行对应操作
		}
		else
		{
			if(g_USART3_RX_CNT > 5)
			{
				u1_printf("\r\n CRC error: ");

				for(i=0; i<g_USART3_RX_CNT; i++)
				{
					u1_printf(" %02X", g_USART3_RX_BUF[i]);
				}
				u1_printf("\r\n");	
			}			
		}
												
		Clear_Uart3Buff();
	}
}

void MotorTestPowerOn(void)
{
	Uart3ChannelSet(CHANNEL_MOTOR);	//Uart3与GPS共用，使用模拟开关选择
	
	u1_printf(" Motor Power On\r\n");
		
	s_ConsolePowerFlag = TRUE;
	
	MotorPowerControl(TRUE);
	
	USART3_Config(115200);	//云台波特率为115200
	
	Task_Create(TaskForRecRS485Pack, 1);	
	
	SetConsolePosition(1);
}

void MotorPowerOn_NoRun(void)
{
	s_ConsolePowerFlag = TRUE;
	
	MotorPowerControl(TRUE);
}

void MotorPowerOn(void)
{
	Clear_Uart3Buff();
	
	USART3_Config(115200);	//云台波特率为115200
		
	Uart3ChannelSet(CHANNEL_MOTOR);	//Uart3与GPS共用，使用模拟开关选择
	
	u1_printf(" Motor Power On\r\n");
	
	while (DMA_GetCurrDataCounter(DMA1_Channel4));
	
	s_ConsolePowerFlag = TRUE;
	
	MotorPowerControl(TRUE);
		
	Task_Create(TaskForMotorCommunication, 10000);
	
	OverTimerID = SetTimer(AUTOPOINTTIME, TimerForPowerOff);		//20s后关闭云台	
	
	Task_Create(TaskForRecRS485Pack, 1);
}

void MotorGotoAnglePowerOn(unsigned short HorAngle, unsigned short VerAngle)
{
//	USART3_Config(115200);	//云台波特率为115200
//		
//	Uart3ChannelSet(CHANNEL_MOTOR);	//Uart3与GPS共用，使用模拟开关选择
//	
//	u1_printf(" Motor Power On\r\n");
//		
//	while (DMA_GetCurrDataCounter(DMA1_Channel4));
//	
//	s_ConsolePowerFlag = TRUE;
//	
//	MotorPowerControl(TRUE);	
	
	Task_Create(TaskForRecRS485Pack, 1);
	
	delay_ms(10);
	
	SendConsoleCMD(MOTOR_ANGLE, HorAngle, VerAngle);
}

void MotorReturnInitPosition(void)
{
	Task_Create(TaskForRecRS485Pack, 1);
	
	delay_ms(10);
	
	SendConsoleCMD(MOTOR_RETURN, 0, 0);
}

void MotorPowerOff(void)
{
	MotorPowerControl(FALSE);
	
	s_ConsolePowerFlag = FALSE;
	
	Task_Kill(TaskForMotorCommunication);
	
	Task_Kill(TaskForRecRS485Pack);
	
	Clear_Uart3Buff();
	

}











