/**********************************
说明:慧云配置工具串口通信程序
	  
作者:关宇晟
版本:V2017.4.3
***********************************/
#include "Comm_Debug.h"
#include "main.h"

#define		MAX_BUFF	256
#define		COM_DATA_SIZE	256


static void Debug_Send_Packet(u8 *packet, u8 len)
{
	u8 send_buf[MAX_BUFF];
	u8 send_len;
	
	send_len = PackMsg(packet, len, send_buf, MAX_BUFF);			//数据包转义处理					
	USART1_DMA_Send(send_buf, send_len);	
}

static void Debug_Get_Time(u8 *data, u8 lenth)
{
	u8 send_buf[64];
	u32 send_len;
	
	CLOUD_HDR *hdr;
	u8 *p_data;
	RTC_DateTypeDef RTC_DateStruct;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(6);
	p_data = &send_buf[sizeof(CLOUD_HDR)];
	
	*(p_data++) = RTC_TimeStructure.RTC_Seconds;
	*(p_data++) = RTC_TimeStructure.RTC_Minutes; 
	*(p_data++) = RTC_TimeStructure.RTC_Hours;

	*(p_data++) = RTC_DateStruct.RTC_Date;
	*(p_data++) = RTC_DateStruct.RTC_Month;
	*(p_data++) = (u8)(RTC_DateStruct.RTC_Year);
	
	send_len += 6;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;

	Debug_Send_Packet(send_buf, send_len);
	
}	

static void Debug_Updata_Time(u8 *data, u8 lenth)
{
	u8 *p_data;
	u8 send_buf[64];
	u32 send_len;
	CLOUD_HDR *hdr;
	
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStruct;
		
	p_data = (u8 *)&data[sizeof(CLOUD_HDR)];
	
	RTC_TimeStructure.RTC_Seconds	 = *(p_data++);
	RTC_TimeStructure.RTC_Minutes 	 = *(p_data++); 
	RTC_TimeStructure.RTC_Hours	 	 = *(p_data++);
	RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure);

	RTC_DateStruct.RTC_Date  	= *(p_data++);
	RTC_DateStruct.RTC_Month  	= *(p_data++);
	RTC_DateStruct.RTC_Year  	= *(p_data++);	
	RTC_DateStruct.RTC_WeekDay  = 1;
	RTC_SetDate(RTC_Format_BIN, &RTC_DateStruct);
	
	RTC_WaitForSynchro();//等待RTC寄存器同步   
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = 0;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	Debug_Send_Packet(send_buf, send_len);
	
	RTC_TimeShow();
}

static void Debug_Read_SystemInfo(u8 *data, u8 lenth)
{
	u8 send_buf[128];
	u32 send_len;
	
	SYSTEM_INFO 	*sys_info;
	
	CLOUD_HDR *hdr;
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(SYSTEM_INFO));
	sys_info = Get_SystemInfo();
	memcpy(&send_buf[sizeof(CLOUD_HDR)], sys_info, sizeof(SYSTEM_INFO));
	send_len += sizeof(SYSTEM_INFO);
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
}


static void Debug_Read_SystemConfig(u8 *data, u8 lenth)
{
	u8 send_buf[128];
	u32 send_len;
	
	SYSTEMCONFIG 	*sys_cfg;
	
	CLOUD_HDR *hdr;
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG));
	sys_cfg = GetSystemConfig();
	memcpy(&send_buf[sizeof(CLOUD_HDR)], sys_cfg, sizeof(SYSTEMCONFIG));
	send_len += sizeof(SYSTEMCONFIG);
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
}

static unsigned char Debug_Set_SystemConfig(u8 *data, u8 lenth)
{
	u8 send_buf[64], ReturnVal = 0;
	u32 send_len;
	SYSTEMCONFIG 	*sys_cfg;
	
	CLOUD_HDR *hdr;
	
	sys_cfg = (SYSTEMCONFIG *)&data[sizeof(CLOUD_HDR)];
	ReturnVal = Set_System_Config(sys_cfg);
	if(ReturnVal == FALSE)
	{
		return FALSE;
	}
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = 0;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	

	return TRUE;
}


static void Debug_Enter_Test_Mode(u8 *data, u32 len)
{
	CLOUD_HDR *hdr;
	u8 send_buf[64];
	u32 send_len;
	
	if(g_ComTestFlag == FALSE)
	{
		g_ComTestFlag = TRUE;	
		TestModeApp_Run();
	}
	else
	{
		g_ComTestFlag = FALSE;
		App_Run();
	}
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(u32));
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);			
}

//读取Http服务器
static void Get_HttpServer(u8 *data, u32 len)
{
	u8 send_buf[128], Http_Length, HttpBuf[128];
	u8 send_len;
	
	CLOUD_HDR *hdr;
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	
	Http_Length = Check_Area_Valid(HTTP_SERVER_ADDR);
	if (Http_Length)
	{
		EEPROM_ReadBytes(HTTP_SERVER_ADDR, (u8 *)HttpBuf, sizeof(SYS_TAG) +  Http_Length);
		hdr->payload_len = swap_word(Http_Length);
		memcpy(&send_buf[sizeof(CLOUD_HDR)], &HttpBuf[sizeof(SYS_TAG)], Http_Length);
		send_len += Http_Length;
		send_buf[send_len] = Calc_Checksum(send_buf, send_len);
		send_len++;
		Debug_Send_Packet(send_buf, send_len);	
		HttpBuf[sizeof(SYS_TAG) + Http_Length] = 0;
//		u1_printf("(Size:%d)%s\r\n", Http_Length, &HttpBuf[sizeof(SYS_TAG)]);
	}
		
	
}
//设置Http服务器
static void Set_HttpServer(u8 *data, u32 len)
{
	SYS_TAG tag;
	u8 send_buf[128], HttpBuf[128], send_len = 0, Http_Length = 0;
	
	CLOUD_HDR *hdr;
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	Http_Length = hdr->payload_len;
	hdr->payload_len = swap_word(hdr->payload_len);
		
	memcpy(&send_buf[sizeof(CLOUD_HDR)], &data[sizeof(CLOUD_HDR)], Http_Length);
	send_len += Http_Length;
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
	
	tag.magic = VAILD;
	tag.length = Http_Length;
	tag.chksum = Calc_Checksum((unsigned char *)&data[sizeof(CLOUD_HDR)], Http_Length);
	
	memcpy(HttpBuf, &tag, sizeof(SYS_TAG));
	memcpy(&HttpBuf[sizeof(SYS_TAG)], &data[sizeof(CLOUD_HDR)], Http_Length);
	
	EEPROM_EraseWords(HTTP_BLOCK1);
	EEPROM_EraseWords(HTTP_BLOCK2);
	EEPROM_WriteBytes(HTTP_SERVER_ADDR, (unsigned char *)HttpBuf, sizeof(SYS_TAG) + Http_Length);

	HttpBuf[sizeof(SYS_TAG) + Http_Length] = 0;
//	u1_printf("(Size:%d)%s\r\n", Http_Length, &HttpBuf[sizeof(SYS_TAG)]);

}

static void DebugGetPhotoConfig(u8 *data, u8 lenth)
{
	u8 send_buf[200];
	u32 send_len;
	
	ANGLESTORE_CFG *p_Config;
	
	CLOUD_HDR *hdr;
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(ANGLESTORE_CFG) - 4);
	p_Config = GetPhotoConfig();
	if(p_Config == NULL)
	{
		return;
	}
	memcpy(&send_buf[sizeof(CLOUD_HDR)], (void *)p_Config->HorizontalAngle, sizeof(ANGLESTORE_CFG) - 4);
	send_len += (sizeof(ANGLESTORE_CFG) - 4);
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	Debug_Send_Packet(send_buf, send_len);		
}

static unsigned char DebugSetPhotoConfig(u8 *data, u8 lenth)
{
	u8 send_buf[64];
	u32 send_len;
	
	CLOUD_HDR *hdr;
		
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = 0;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	

	return TRUE;
}


void OnDebug(u8 *data, u8 lenth)
{
	u8 ReturnVal = 0, Num = 0;
	CLOUD_HDR *hdr;
	u32 Hor = 0, Ver = 0;
	ANGLESTORE_CFG Config;
	
	hdr = (CLOUD_HDR *)data;
	switch(hdr->cmd)
	{
		case CMD_GET_DATETIME:	//获取设备时间	--0xF1
			Debug_Get_Time(data, lenth);
			break;
		case CMD_SET_DATETIME:	 //设置设备时间 --0xF2
			Debug_Updata_Time(data, lenth);
			break;
		
		case CMD_RD_SYS_INFO:	//读取系统信息 --0xF3
			Debug_Read_SystemInfo(data, lenth);
			break;
	
		case CMD_RD_SYS_CFG:	//读取系统配置 --0xF5
			Debug_Read_SystemConfig(data, lenth);
			break;
		case CMD_WR_SYS_CFG:	//设置系统配置 --0xF6
			ReturnVal = Debug_Set_SystemConfig(data, lenth);
			if(ReturnVal == FALSE)
			{
				u1_printf(" Config Err Data\r\n");
				return;
			}
			delay_ms(20);
//			u1_printf("\r\n 等待重启...\r\n");
//			while (DMA_GetCurrDataCounter(DMA1_Channel4));
//			Sys_Soft_Reset();
			break;
//		
//		//单机配置输入通道================================================================
		case CMD_INPUT_COUNT:	   //获取设备额定单元输入参量个数 --0x10

		break;
		case CMD_GET_INPUT:		  //读取一个单元输入参量配置信息 --0x11

		break;
		case CMD_SET_INPUT:		  //设置一个单元输入参量配置信息 --0x12

		break;
		case CMD_DEL_INPUT:	   //删除一个输入配置 --0xF7

		break;
		//单机配置输出通道=================================================================
		case CMD_OUTPUT_COUNT:	 //获取设备额定单元输出通道个数	--0x13

			break;
		case CMD_GET_OUTPUT:	 //读取一个单元输出通道关联管脚配置信息--0x14

			break;
		case CMD_SET_OUTPUT:	//设置一个单元输出通道关联管脚配置信息--0x15

			break;
		case CMD_DEL_OUTPUT:   //删除一个输出配置  --0xF8

			break;	

		case CMD_TEST_MODE:
			Debug_Enter_Test_Mode(data, lenth);		//进入低功耗设备配置模式	关闭通信，不进入停止模式
			break;
	//本地云台操作指令	
		
		case CMD_CONTROL_STOP://停
			SendConsoleCMD(MOTOR_STOP, 0, 0);
		break;
		
		case CMD_CONTROL_UP://上
			SendConsoleCMD(MOTOR_RUN_UP, 0, 0);
		break;
		
		case CMD_CONTROL_DOWN://下
			SendConsoleCMD(MOTOR_RUN_DOWN, 0, 0);
		break;
		
		case CMD_CONTROL_LEFT://左
			SendConsoleCMD(MOTOR_RUN_LEFT, 0, 0);
		break;
		
		case CMD_CONTROL_RIGHT://右
			SendConsoleCMD(MOTOR_RUN_RIGHT, 0, 0);
		break;
		
		case CMD_CONTROL_GO_TO_PRESET:		//Num范围为1~20
			
			Num = (data[sizeof(CLOUD_HDR)]) ;
			GotoPresetPoint(Num);
		
		break;
		
		case CMD_CONTROL_SET_PRESET:		//Num范围为1~20
			
			Num = (data[sizeof(CLOUD_HDR)]) ;
			Hor = (data[sizeof(CLOUD_HDR)+1]) + (data[sizeof(CLOUD_HDR)+2] << 8);
			Ver = (data[sizeof(CLOUD_HDR)+3]) + (data[sizeof(CLOUD_HDR)+4] << 8);
		
			u1_printf("Set Preset(%d):%d,%d\r\n", Num, Hor, Ver);
			WriteNowAngle(Hor, Ver, Num);
		
		break;
		
		case CMD_CONTROL_CLEAR_PRESET:		//Num范围为1~20
			
			Num = (data[sizeof(CLOUD_HDR)]) ;
			u1_printf("Clear Preset(%d)\r\n", Num);
			WriteNowAngle(0, 0, Num);
		
		break;
		
		case CMD_CONTROL_GO_TO_ANGLE:	//转至指定角度
		
			Hor = (data[sizeof(CLOUD_HDR)]) + (data[sizeof(CLOUD_HDR)+1] << 8);
			Ver = data[sizeof(CLOUD_HDR)+2] + (data[sizeof(CLOUD_HDR)+3] << 8);
				
			u1_printf(" Angle: %d %d\r\n", Hor, Ver);
				
			if(Hor > MAX_HOR_ANGLE)
			{
				Hor = MAX_HOR_ANGLE;
				u1_printf(" Hor Angle Over 254\r\n");
			}
			
			if(Ver > MAX_VER_ANGLE)
			{
				Ver = MAX_VER_ANGLE;
				u1_printf(" Ver Angle Over 60\r\n");
			}
			
			SendConsoleCMD(MOTOR_ANGLE, Hor, Ver);
			
		break;
		
		case CMD_CONTROL_GETPHOTOCONFIG:	//获取相机参数		
			DebugGetPhotoConfig(data, lenth);
		break;
		
		case CMD_CONTROL_SETPHOTOCONFIG:	//设置相机参数
			
			DebugSetPhotoConfig(data, lenth);
		
			Config.PhotoPointNum = data[sizeof(CLOUD_HDR)];
			Config.PhotoInterval = (data[sizeof(CLOUD_HDR)+1]) + (data[sizeof(CLOUD_HDR)+2] << 8);
			
			Config.StartPhotoTime[0] = data[sizeof(CLOUD_HDR)+3];
			Config.StartPhotoTime[1] = data[sizeof(CLOUD_HDR)+4];
			Config.StartPhotoTime[2] = data[sizeof(CLOUD_HDR)+5];
			
			Config.EndPhotoTime[0] = data[sizeof(CLOUD_HDR)+6];
			Config.EndPhotoTime[1] = data[sizeof(CLOUD_HDR)+7];
			Config.EndPhotoTime[2] = data[sizeof(CLOUD_HDR)+8];
		
			SetPhotoConfig(&Config);
		
			
		break;
		
		case CMD_CONTROL_AUTO://自检操作
			SendConsoleCMD(MOTOR_AUTO, 0, 0);
		break;
			
		case CMD_GET_HTTPSERVER://读取HTTP服务器
			Get_HttpServer(data, lenth);		
		break;
		
		case CMD_SET_HTTPSERVER://设置HTTP服务器
			Set_HttpServer(data, lenth);
		break;
		
		case CMD_RESET:
			u1_printf("\r\n Reset Cmd...\r\n");
			while (DMA_GetCurrDataCounter(DMA1_Channel4));
			Sys_Soft_Reset();
			break;
	}
}
