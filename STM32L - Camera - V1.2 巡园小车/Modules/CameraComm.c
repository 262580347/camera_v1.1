#include "main.h"
#include "CameraComm.h"

#define		MSG_ACK_STATUS	1

//协议栈0的实例
static CameraComm m_Instance;
static CameraComm *pthis = NULL;
//协议栈0的通讯总线句柄
//static AfcUart *m_hComBus = NULL;
//协议栈0应用报文接收观察者
static CAMERACOMMRXOBSER m_hRxObser = NULL; 


//发送环形FIFO机制
#if ENABLE_TX_FIFO
#define TXFIFO_DEEP  5//发送环形FIFO深度(缓冲报文总数)
static TXFIFO_COMM_MSG m_TxFIFOMsg1;
static TXFIFO_COMM_MSG m_TxFIFOMsg2;  
static TXFIFO_COMM_MSG m_TxFIFOMsg3;  
static TXFIFO_COMM_MSG m_TxFIFOMsg4; 
static TXFIFO_COMM_MSG m_TxFIFOMsg5; 
static TXFIFO_COMM_MSG *m_hTxMsgFIFO[TXFIFO_DEEP];
static UCHAR m_TxFIFORPinter = 0;
static UCHAR m_TxFIFOWPinter = 0;
static UINT  m_TxFIFOtaskID = NULL;
static UINT  m_TxFIFORetryCnt = 0;
static BASEOBSER m_hTxFIFONull = NULL;
static void  taskForTxFIFO(void);
static void  OnRxMatchFIFOMsg(CAMERA_COMM_MSG *pMsg);
#endif

////计数报文长度校验
//static UINT16 GetCheckLen(UINT16 uLen)
//{
//	UINT reLen = 0;

//    uLen = uLen&0x1FFF;
//	reLen = (~uLen)&0x07;
//	reLen = (reLen<<13)|uLen;
//	return reLen;
//}

void OnRecCOMMsg(CAMERA_COMM_MSG *m_RxFrame)
{
	u8 i;
	
	u1_printf(" On Msg:");
	u1_printf("Pro:%04X ", m_RxFrame->Protocol); 
	u1_printf("ID:%02X ", m_RxFrame->DeviceID );
	u1_printf("Dir:%01X ", m_RxFrame->Dir);  
	u1_printf("Seq:%02X ", m_RxFrame->Seq);    
	u1_printf("Len:%02X ", m_RxFrame->Length);     
	u1_printf("CMD:%01X Data:", m_RxFrame->OPType); 
	for(i=0; i<m_RxFrame->Length; i++)
	{
		u1_printf("%02X ", m_RxFrame->UserBuf[i]);
	}	
	u1_printf("\r\n");
}


//计数报文的校验和
static UCHAR GetCheckValue(CAMERA_COMM_MSG *pMsg)
{
    UCHAR CheckB = 0;
	UINT  i;
	CheckB += (UCHAR)(pMsg->Protocol>>8);
	CheckB += (UCHAR)(pMsg->Protocol);
	CheckB += (UCHAR)(pMsg->DeviceID>>24);
	CheckB += (UCHAR)(pMsg->DeviceID>>16);
	CheckB += (UCHAR)(pMsg->DeviceID>>8);
	CheckB += (UCHAR)(pMsg->DeviceID);
	CheckB += pMsg->Dir;
	CheckB += (UCHAR)(pMsg->Seq>>8);
	CheckB += (UCHAR)(pMsg->Seq);
	CheckB += (UCHAR)(pMsg->Length>>8);
	CheckB += (UCHAR)(pMsg->Length);
	CheckB += (UCHAR)(pMsg->OPType);
	for(i=0;i<pMsg->Length;i++)
	{
		CheckB += pMsg->UserBuf[i];
	}
	return CheckB;
}
static void DecodeAppMsg(UCHAR *pBuf,u16 uLen,CAMERA_COMM_MSG *pMsg)
{
    u32 uPoint = 0;
	
    pMsg->Protocol = pBuf[uPoint++];
    pMsg->Protocol = (pMsg->Protocol<<8)|pBuf[uPoint++];
	
	pMsg->DeviceID = pBuf[uPoint++];
    pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	
	pMsg->Dir   = pBuf[uPoint++];
	
	pMsg->Seq = pBuf[uPoint++];
    pMsg->Seq = (pMsg->Seq<<8)|pBuf[uPoint++];
	
    pMsg->Length = pBuf[uPoint++];
    pMsg->Length = (pMsg->Length<<8)|pBuf[uPoint++];
	
    pMsg->OPType   = pBuf[uPoint++];
	
    if(pMsg->Length > COMM_DATA_SIZE)
    {
	    pMsg->Length = COMM_DATA_SIZE;
    }
	memcpy((void *)pMsg->UserBuf,(void *)&pBuf[uPoint],pMsg->Length);
}

static BOOL COM3Protocol(CAMERA_COMM_MSG *m_RxFrame)
{
	unsigned char PackBuff[100];
	u16   PackLengthgth = 0, i;
	
	UnPackMsg(g_USART3_RX_BUF+1, g_USART3_RX_CNT-2, PackBuff, &PackLengthgth);	//解包

	if (PackBuff[(PackLengthgth)-1] == Calc_Checksum(PackBuff, (PackLengthgth)-1))
	{		
		DecodeAppMsg(PackBuff, PackLengthgth-1, m_RxFrame);
		return TRUE;
	}
	else
	{
		u1_printf("COM3:");
		for(i=0; i<PackLengthgth; i++)
		{
			u1_printf("%02X ", PackBuff[i]);
		}
		u1_printf("\r\n");
		u1_printf(" CRC Error :%02X\r\n", Calc_Checksum(PackBuff, (PackLengthgth)-1));
		return FALSE;
	}	
	
}

//协议栈0 应用报文接收观察者处理任务
static void TaskForRxObser(void)
{
	static CAMERA_COMM_MSG m_RxFrame;
	u8 i;

	if(g_Uart3RxFlag == TRUE)
	{
		g_Uart3RxFlag = FALSE;
		if(g_USART3_RX_BUF[0] == BOF_VAL && g_USART3_RX_BUF[g_USART3_RX_CNT-1] == EOF_VAL && g_USART3_RX_CNT >= 15)
		{
			if(COM3Protocol(&m_RxFrame))
			{
				if(m_hRxObser!=NULL)
				{
					m_hRxObser(&m_RxFrame);
				}
			}
			 #if ENABLE_TX_FIFO
				 OnRxMatchFIFOMsg(&m_RxFrame);
			 #endif
		}
		else
		{
			for(i=0; i<g_USART3_RX_CNT; i++)
			{
				u1_printf("%02X ", g_USART3_RX_BUF[i]);
			}
			u1_printf("\r\n");
		}

		Clear_Uart3Buff();
	}
	
}

//注册接收报文观察者
static void  AddRxObser(HANDLE hComBus,CAMERACOMMRXOBSER hRxObser)
{
//	if(hComBus!=NULL)
//	{
//		m_hComBus = hComBus;
//		m_hComBus->AddRxObser(OnComm);
//	}
	if(hRxObser!=NULL)
	{
		m_hRxObser = hRxObser;
		Task_Create(TaskForRxObser,1);					
	}
	Task_Create(TaskForRxObser,1);				
}
static void Remove(void)
{
	Task_Kill(TaskForRxObser);
	m_hRxObser = NULL;
}
//发送报文
static void SendMsg(CAMERA_COMM_MSG *pMsg)
{
	UCHAR CheckVale;
	u16 nMsgLen, len;
	u8 Packet[100], send_buf[100], i;
	
	if(pMsg->Length <= COMM_DATA_SIZE)
	{
		CheckVale = GetCheckValue(pMsg);
		
		len = 0;
		send_buf[len++] = pMsg->Protocol >> 8;
		send_buf[len++] = pMsg->Protocol;
		
		send_buf[len++] = pMsg->DeviceID >> 24;
		send_buf[len++] = pMsg->DeviceID >> 16;	
		send_buf[len++] = pMsg->DeviceID >> 8;
		send_buf[len++] = pMsg->DeviceID;	
		
		send_buf[len++]  = 0;
		
		send_buf[len++]  = pMsg->Seq >> 8;
		send_buf[len++]  = pMsg->Seq ;
		
		send_buf[len++]  = pMsg->Length >> 8;
		send_buf[len++]  = pMsg->Length;

		send_buf[len++] = pMsg->OPType;			
				
		memcpy(&send_buf[len], pMsg->UserBuf, pMsg->Length);
		
		len += pMsg->Length;
		
		send_buf[len++] = CheckVale;
				
		nMsgLen = PackMsg( send_buf,len,Packet,100);
		USART3_DMA_Send(Packet, nMsgLen);
		for(i=0; i<nMsgLen; i++)
		{
			u1_printf("%02X ", Packet[i]);
		}
		u1_printf("\r\n");
	}
}

////发送报文[m_TxFrame成员]
//static void SendMessage(void)
//{
//	UCHAR uData, CheckVale;
//	u16 nMsgLen, len, i;
//	u8 Packet[100], send_buf[100];

//	if(m_Instance.m_TxFrame.Length<=COMM_DATA_SIZE)
//	{
//		CheckVale = GetCheckValue(&m_Instance.m_TxFrame);
//		
//		len = 0;
//		send_buf[len++] = m_Instance.m_TxFrame.Protocol >> 8;
//		send_buf[len++] = m_Instance.m_TxFrame.Protocol;
//		
//		send_buf[len++] = m_Instance.m_TxFrame.DeviceID >> 24;
//		send_buf[len++] = m_Instance.m_TxFrame.DeviceID >> 16;	
//		send_buf[len++] = m_Instance.m_TxFrame.DeviceID >> 8;
//		send_buf[len++] = m_Instance.m_TxFrame.DeviceID;	
//		
//		send_buf[len++]  = m_Instance.m_TxFrame.Dir;
//		
//		send_buf[len++]  = m_Instance.m_TxFrame.Seq >> 8;
//		send_buf[len++]  = m_Instance.m_TxFrame.Seq ;
//		
//		send_buf[len++]  = m_Instance.m_TxFrame.Length >> 8;
//		send_buf[len++]  = m_Instance.m_TxFrame.Length;

//		send_buf[len++] = m_Instance.m_TxFrame.OPType;			
//		
//		memcpy(&send_buf[len], m_Instance.m_TxFrame.UserBuf, m_Instance.m_TxFrame.Length);
//		
//		len += m_Instance.m_TxFrame.Length;
//		
//		send_buf[len++] = CheckVale;
//				
//		nMsgLen = PackMsg( send_buf,len,Packet,100);
//		USART3_DMA_Send(Packet, nMsgLen);
//		
//		for(i=0; i<nMsgLen; i++)
//		{
//			u1_printf("%02X ", Packet[i]);
//		}
//		u1_printf("\r\n");
//	}
//}

#if ENABLE_TX_FIFO
static void taskForTxFIFO(void)
{
	m_TxFIFORetryCnt++;
	if(m_TxFIFORetryCnt>=TXFIFO_RETRY_TIME)
	{
		m_TxFIFORetryCnt = 0;
		m_TxFIFORPinter++;
		if(m_TxFIFORPinter>=TXFIFO_DEEP)
		{
			m_TxFIFORPinter = 0;
		}
		if(m_TxFIFOWPinter == m_TxFIFORPinter)
		{
			Task_Kill(taskForTxFIFO);
			m_TxFIFOtaskID = NULL;
			#if ENABLE_TX_FIFO
			if(m_hTxFIFONull!=NULL)
			{
			    m_hTxFIFONull();
			}
			#endif
			return;
		}
	}
	pthis->m_TxFrame.Protocol   = m_hTxMsgFIFO[m_TxFIFORPinter]->Protocol;
	pthis->m_TxFrame.DeviceID = m_hTxMsgFIFO[m_TxFIFORPinter]->DeviceID;
	pthis->m_TxFrame.Dir   = m_hTxMsgFIFO[m_TxFIFORPinter]->Dir;
	pthis->m_TxFrame.Seq      = m_hTxMsgFIFO[m_TxFIFORPinter]->Seq;
	pthis->m_TxFrame.Length      = m_hTxMsgFIFO[m_TxFIFORPinter]->Length;
	pthis->m_TxFrame.OPType      = m_hTxMsgFIFO[m_TxFIFORPinter]->OPType;
	memcpy((void *)pthis->m_TxFrame.UserBuf,(void *)m_hTxMsgFIFO[m_TxFIFORPinter]->UserBuf,m_hTxMsgFIFO[m_TxFIFORPinter]->Length);
	pthis->SendMsg(&pthis->m_TxFrame);	
}
#endif

#if ENABLE_TX_FIFO			 
//使用发送FIFO发送报文
static BOOL SendMsgbyFIFO(CAMERA_COMM_MSG *pMsg)
{
	UCHAR CurPos;

	if(pMsg->Length<=TXFIFO_USER_SIZE)
	{
		m_hTxMsgFIFO[m_TxFIFOWPinter]->Protocol   	= pMsg->Protocol;
		m_hTxMsgFIFO[m_TxFIFOWPinter]->DeviceID   	= pMsg->DeviceID;
		m_hTxMsgFIFO[m_TxFIFOWPinter]->Dir     	 	= pMsg->Dir;
		m_hTxMsgFIFO[m_TxFIFOWPinter]->Seq      	= pMsg->Seq;
		m_hTxMsgFIFO[m_TxFIFOWPinter]->Length      	= pMsg->Length;
		m_hTxMsgFIFO[m_TxFIFOWPinter]->OPType      	= pMsg->OPType;
	    memcpy((void *)m_hTxMsgFIFO[m_TxFIFOWPinter]->UserBuf,(void *)pMsg->UserBuf,pMsg->Length);
		CurPos = m_TxFIFOWPinter;
		m_TxFIFOWPinter++;
		if(m_TxFIFOWPinter>=TXFIFO_DEEP)
		{
			m_TxFIFOWPinter = 0;	
		}
		if(m_TxFIFOWPinter == m_TxFIFORPinter)
		{
			m_TxFIFORPinter++;
			if(m_TxFIFORPinter>=TXFIFO_DEEP)
			{
				m_TxFIFORPinter = 0;
			}
		}
		if(m_TxFIFOtaskID == NULL)
		{
			m_TxFIFOtaskID = Task_Create(taskForTxFIFO,pthis->m_dwReTime);
			pthis->m_TxFrame.Protocol   	= m_hTxMsgFIFO[CurPos]->Protocol;
			pthis->m_TxFrame.DeviceID 		= m_hTxMsgFIFO[CurPos]->DeviceID;
			pthis->m_TxFrame.Dir   			= m_hTxMsgFIFO[CurPos]->Dir;
			pthis->m_TxFrame.Seq      		= m_hTxMsgFIFO[CurPos]->Seq;
			pthis->m_TxFrame.Length      	= m_hTxMsgFIFO[CurPos]->Length;
			pthis->m_TxFrame.OPType      	= m_hTxMsgFIFO[CurPos]->OPType;
			memcpy((void *)pthis->m_TxFrame.UserBuf,(void *)m_hTxMsgFIFO[CurPos]->UserBuf,m_hTxMsgFIFO[CurPos]->Length);
			pthis->SendMsg(&pthis->m_TxFrame);
			m_TxFIFORetryCnt = 0;
		}
		return TRUE;
	}
	return FALSE;		
}
#endif

#if ENABLE_TX_FIFO
//仲裁当前接收报文与发送FIFO报文响应的匹配
static void OnRxMatchFIFOMsg(CAMERA_COMM_MSG *pMsg)
{
	if((m_TxFIFOWPinter==m_TxFIFORPinter)\
	 &&(m_TxFIFOtaskID==NULL))
	{
		return;
	}
	if((pMsg->DeviceID == m_hTxMsgFIFO[m_TxFIFORPinter]->DeviceID)\
 	 &&(pMsg->OPType == m_hTxMsgFIFO[m_TxFIFORPinter]->OPType)\
	 &&(pMsg->Dir == MSG_ACK_STATUS)\
	)
	{
		m_TxFIFORetryCnt = 0;
		m_TxFIFORPinter++;
		if(m_TxFIFORPinter>=TXFIFO_DEEP)
		{
			m_TxFIFORPinter = 0;	
		}
		if(m_TxFIFOWPinter==m_TxFIFORPinter)
		{
			Task_Kill(taskForTxFIFO);
			m_TxFIFOtaskID = NULL;	
		}
		else
		{
			pthis->m_TxFrame.Protocol   	= m_hTxMsgFIFO[m_TxFIFORPinter]->Protocol;
			pthis->m_TxFrame.DeviceID 		= m_hTxMsgFIFO[m_TxFIFORPinter]->DeviceID;
			pthis->m_TxFrame.Dir   			= m_hTxMsgFIFO[m_TxFIFORPinter]->Dir;
			pthis->m_TxFrame.Seq      		= m_hTxMsgFIFO[m_TxFIFORPinter]->Seq;
			pthis->m_TxFrame.Length      	= m_hTxMsgFIFO[m_TxFIFORPinter]->Length;
			pthis->m_TxFrame.OPType      	= m_hTxMsgFIFO[m_TxFIFORPinter]->OPType;
			memcpy((void *)pthis->m_TxFrame.UserBuf,(void *)m_hTxMsgFIFO[m_TxFIFORPinter]->UserBuf,m_hTxMsgFIFO[m_TxFIFORPinter]->Length);
			pthis->SendMsg(&pthis->m_TxFrame);				
		}
	} 
}
#endif

#if ENABLE_TX_FIFO
//注册一个发送FIFO空事件观察者
static void AddTxFIFONULLObser(BASEOBSER hObser)
{
     m_hTxFIFONull = hObser;
}
#endif
//创建协议栈0
static void New(void)
{
	pthis = &m_Instance;
	pthis->AddRxObser = AddRxObser;
	pthis->SendMsg = SendMsg;
	pthis->Remove = Remove;
//	pthis->SendMessage = SendMessage;	
#if ENABLE_TX_FIFO
	pthis->SendMsgbyFIFO = SendMsgbyFIFO;
	pthis->AddTxFIFONULLObser = AddTxFIFONULLObser;
	m_hTxMsgFIFO[0] = &m_TxFIFOMsg1;
	m_hTxMsgFIFO[1] = &m_TxFIFOMsg2;
	m_hTxMsgFIFO[2] = &m_TxFIFOMsg3;
	m_hTxMsgFIFO[3] = &m_TxFIFOMsg4;
	m_hTxMsgFIFO[4] = &m_TxFIFOMsg5;
	m_TxFIFORPinter = 0;
    m_TxFIFOWPinter = 0;
	m_TxFIFOtaskID  = NULL;
    m_TxFIFORetryCnt = 0;
	m_hTxFIFONull  = NULL;
	pthis->m_dwReTime = TXFIFO_WAIT_MSG_TIME;
#endif
}

//获取协议栈实例句柄 
CameraComm *CameraCommGet(void)
{
	if(pthis == NULL)
	{
		New();
	}
	
	return pthis;	
}
