#ifndef __AFCTASK_H__
#define __AFCTASK_H__

#define 	TASK_SIZE  20
#define  TASK_TIME_1S	44590 //近似1s的任务周期
//任务函数原型
typedef void (*TASKFUN)(void);

//创建一个新的任务
unsigned char Task_Create(TASKFUN htack,unsigned int time);
//设置任务的运行周期
void Task_SetTime(TASKFUN htack,unsigned int time);
//注销一个指定任务
void Task_Kill(TASKFUN htack);
//注销所有任务
void Task_KillAll(void);
//任务执行
void Task_Execute(void);


#endif


