#ifndef __LED_H
#define __LED_H	 

#include "compileconfig.h"

#ifdef		CAMERA_VERSION_V1_1

#define		RUN_LED_GPIO_PIN		GPIO_Pin_6
#define		RUN_LED_GPIO_TYPE		GPIOB

#define		NET_LED_GPIO_PIN		GPIO_Pin_7
#define		NET_LED_GPIO_TYPE		GPIOB

#define		SENSOR_POWER_PIN		GPIO_Pin_8
#define		SENSOR_POWER_TYPE		GPIOB

#define		SDA_PIN		GPIO_Pin_0
#define		SDA_TYPE	GPIOA
#define		SCK_PIN		GPIO_Pin_1
#define		SCK_TYPE	GPIOA

#endif

#ifdef		CAMERA_VERSION_V1_2

#define		RUN_LED_GPIO_PIN		GPIO_Pin_3
#define		RUN_LED_GPIO_TYPE		GPIOB

#define		NET_LED_GPIO_PIN		GPIO_Pin_4
#define		NET_LED_GPIO_TYPE		GPIOB

#define		BLUE_LED_GPIO_PIN		GPIO_Pin_15
#define		BLUE_LED_GPIO_TYPE		GPIOA

#define		SENSOR_POWER_PIN		GPIO_Pin_7
#define		SENSOR_POWER_TYPE		GPIOB

#define		SDA_PIN		GPIO_Pin_6
#define		SDA_TYPE	GPIOB
#define		SCK_PIN		GPIO_Pin_5
#define		SCK_TYPE	GPIOB

#endif


#define		LED_RUN_ON()	GPIO_SetBits(RUN_LED_GPIO_TYPE,RUN_LED_GPIO_PIN)
#define		LED_NET_ON()	GPIO_SetBits(NET_LED_GPIO_TYPE,NET_LED_GPIO_PIN)

#define		LED_RUN_OFF()	GPIO_ResetBits(RUN_LED_GPIO_TYPE,RUN_LED_GPIO_PIN)
#define		LED_NET_OFF()	GPIO_ResetBits(NET_LED_GPIO_TYPE,NET_LED_GPIO_PIN)

void LEDNoWork(void);

void LEDResuseWork(void);

void STM32_GPIO_Init(void);

void RunLED(void);

void WakeUp_GPIO_Init(void);

void Stop_GPIO_Init(void);

void TaskForLEDBlinkRun(void);

void TaskForLEDBlinkEnd(void);

void TaskForRunLED(void);

#endif
