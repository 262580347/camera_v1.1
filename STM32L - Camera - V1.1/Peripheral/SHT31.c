/**********************************
说明:SHT31温湿度传感器驱动程序
	  注意ADDR接高电平，CMD_WRITE为0x88
	  ADDR接低电平，CMD_WRITE为0x8A
	  芯片自带CRC检验，检验多项式为P(x) = x^8 + x^5 + x^4 + 1 = 100110001
	  若读数错误则返回ERROR_VALUE
	  若接口更改则需修改宏定义及IIC_Init中的GPIO
	  建议1S读一次温湿度值，最快至少需间隔0.5S
	  初始化时已强制失能自动加热功能
	  低功耗设备中使用单次测量模式降低功耗
作者:关宇晟
版本:V2018.4.3
***********************************/

#include "main.h"
#include "SHT31.h"

#define  CMD_WRITE           0x88	//ADDR Connect GND else Write :0x8A Read :0x8B
#define  CMD_READ            0x89

/*CRC*/
#define POLYNOMIAL             0x131           // P(x) = x^8 + x^5 + x^4 + 1 = 100110001

#define	BUF_COUNT	10

SHT31_STATUS Sht31Status;	//传感器状态

unsigned char s_SHTGetFlag = FALSE;

static float s_Temp_Val = 0;
static float s_Humi_Val = 0;

static float TempValBuf[BUF_COUNT];
static float HumiValBuf[BUF_COUNT];

static unsigned int TimerID = 0;

void StartSHTSensor(void)
{
	if(Sht31Status != SHT_ERROR)
	{
		s_SHTGetFlag = TRUE;
		
		Sht31Status = SHT_WORKING;
	}
	
}
/******************************************************/
static u8 SHT3X_WriteCMD(uint16 cmd)
{
	u8 err = 0;
	IIC_Start();
	IIC_WriteByte(CMD_WRITE);
	err = IIC_WaitACK();
	if(err)
		return 1;
	IIC_WriteByte(cmd>>8);
	err = IIC_WaitACK();
	if(err)
		return 1;
	IIC_WriteByte(cmd);
	err = IIC_WaitACK();
	if(err)
		return 1;
	IIC_Stop();
	
	return 0;
}

/********************************************************/
static u8 SHT3X_SetPeriodicMeasurement()
{
	u8 err = 0;
    err = SHT3X_WriteCMD(CMD_MEAS_POLLING_H);
	return err;
}


static u8 SHX3X_ReadResults(uint16 cmd,  uint8 *data)
{
	u8 err;
	IIC_Start();
	IIC_WriteByte(CMD_WRITE);
	err = IIC_WaitACK();
	if(err)
		return 1;
	IIC_WriteByte(cmd>>8);
	err = IIC_WaitACK();
	if(err)
		return 1;
	IIC_WriteByte(cmd);
	err = IIC_WaitACK();
	if(err)
		return 1;

    IIC_Start();
	IIC_WriteByte(CMD_READ);
	err = IIC_WaitACK();
	if(err)
		return 1;

	data[0] = IIC_ReadByte(TRUE);

	data[1] = IIC_ReadByte(TRUE);

	data[2] = IIC_ReadByte(TRUE);

	data[3] = IIC_ReadByte(TRUE);

	data[4] = IIC_ReadByte(TRUE);

	data[5] = IIC_ReadByte(FALSE);

	IIC_Stop();
	
	return 0;
}

/********************************************************************************/
/********************************************************************************/




static uint8 SHT3X_CalcCrc(uint8 *data, uint8 nbrOfBytes)
{
	uint8 bit;        // bit mask
    uint8 crc = 0xFF; // calculated checksum
    uint8 byteCtr;    // byte counter

    // calculates 8-Bit checksum with given polynomial
    for(byteCtr = 0; byteCtr < nbrOfBytes; byteCtr++) {
        crc ^= (data[byteCtr]);
        for(bit = 8; bit > 0; --bit) {
            if(crc & 0x80) {
                crc = (crc << 1) ^ POLYNOMIAL;
            }  else {
                crc = (crc << 1);
            }
        }
    }
	return crc;
}

static uint8 SHT3X_CheckCrc(uint8 *pdata, uint8 nbrOfBytes, uint8 checksum)
{
    uint8 crc;
	crc = SHT3X_CalcCrc(pdata, nbrOfBytes);// calculates 8-Bit checksum
    if(crc != checksum) 
    {   
        return 1;           
    }
    return 0;              
}

static float SHT3X_CalcTemperature(uint16 rawValue)
{
    // calculate temperature 
    float temp;
    temp = (175 * (float)rawValue / 65535 - 45) ; // T = -45 + 175 * rawValue / (2^16-1)
    return temp;
}

static float SHT3X_CalcRH(uint16 rawValue)
{
    // calculate relative humidity [%RH]
    float temp1 ;
	temp1 = (100 * (float)rawValue / 65535) ;  // RH = rawValue / (2^16-1) * 10
    return temp1;
}

static void SHT_GetValue(float *f_Temp_Value, float *f_RH_Value)
{
    uint8 temp = 0, err;
    uint16 dat;
    uint8 data[3];
    static uint8 buffer[6];
	
    err = SHX3X_ReadResults(CMD_FETCH_DATA, buffer);
	if(err)
	{
		*f_Temp_Value = ERROR_VALUE;//异常值
		*f_RH_Value = ERROR_VALUE;
		return;
	}
    /* check tem */
    data[0] = buffer[0];
    data[1] = buffer[1];
    data[2] = buffer[2];
    temp = SHT3X_CheckCrc(data,2,data[2]);
    if( !temp ) /* value is ture */
    {
        dat = ((uint16)buffer[0] << 8) | buffer[1];
        *f_Temp_Value = SHT3X_CalcTemperature( dat );    
    }
	else
	{
		u1_printf("CheckCrc1\r\n");
		*f_Temp_Value = ERROR_VALUE;//异常值
	}
    /* check humidity */
    data[0] = buffer[3];
    data[1] = buffer[4];
    data[2] = buffer[5];
    temp = SHT3X_CheckCrc(data,2,data[2]);
    if( !temp )
    {
        dat = ((uint16)data[0] << 8) | data[1];
        *f_RH_Value = SHT3X_CalcRH( dat ); 
    }
	else
	{
		u1_printf("CheckCrc2\r\n");
		*f_RH_Value = ERROR_VALUE;//异常值
	}
}

float Get_Temp_Value(void)
{
	return s_Temp_Val;
}

float Get_Humi_Value(void)
{
	return s_Humi_Val;
}

static void GetSHT31Data(void)
{
	float f_Temp_Value, f_RH_Value;
	static u8  s_TempDetCount = 0, s_TempDetNum = 0, s_HumiDetCount = 0, s_HumiDetNum = 0;
	uint32 error_mark = 0xEEEEEEEE;	
		
	SHT_GetValue(&f_Temp_Value, &f_RH_Value);
	if(f_Temp_Value == ERROR_VALUE)
	{
		s_Temp_Val = ERROR_VALUE;
		Sht31Status = SHT_ERROR;
		memcpy((uint8 *)&s_Temp_Val, (uint8 *)&error_mark, sizeof(uint32));
//		u1_printf("\r\n Error Temp Data.\r\n");
	}
	else
	{
		if(s_TempDetCount < 3)
		{
			TempValBuf[s_TempDetCount] = f_Temp_Value;
			s_Temp_Val = f_Temp_Value;
		}
		else
		{
			TempValBuf[s_TempDetNum] = f_Temp_Value;
			s_Temp_Val = Mid_Filter(TempValBuf, s_TempDetCount);
		}
		
		s_TempDetCount++;
		s_TempDetNum++;
		
		if(s_TempDetCount >= BUF_COUNT)
		{
			s_TempDetCount = BUF_COUNT;
		}
		
		if(s_TempDetNum >= BUF_COUNT)
		{
			s_TempDetNum = 0;
		}
		
//		u1_printf("\r\n Temp:%1.2f℃ Avg:%1.2f℃ ", f_Temp_Value, s_Temp_Val);
	}
	if(f_RH_Value == ERROR_VALUE)
	{
		s_Humi_Val = ERROR_VALUE;
		Sht31Status = SHT_ERROR;
		memcpy((uint8 *)&s_Humi_Val, (uint8 *)&error_mark, sizeof(uint32));
//		u1_printf("\r\n Error Humi Data.\r\n");
	}
	else
	{
		if(s_HumiDetCount < 3)
		{
			HumiValBuf[s_HumiDetCount] = f_RH_Value;
			s_Humi_Val = f_RH_Value;
		}
		else
		{
			HumiValBuf[s_HumiDetNum] = f_RH_Value;
			s_Humi_Val = Mid_Filter(HumiValBuf, s_HumiDetCount);
		}
		
		s_HumiDetCount++;
		s_HumiDetNum++;
		
		if(s_HumiDetCount >= BUF_COUNT)
		{
			s_HumiDetCount = BUF_COUNT;
		}
		
		if(s_HumiDetNum >= BUF_COUNT)
		{
			s_HumiDetNum = 0;
		}
		
//		u1_printf(" Humi:%1.2f%% Avg:%1.2f%%\r\n", f_RH_Value, s_Humi_Val);
	}
	
	Sht31Status = SHT_IDLE;
	
	KillTimer(TimerID);
}

static void TaskForSHT31(void)
{
	if(s_SHTGetFlag == TRUE)
	{						
		SHT3X_SetPeriodicMeasurement();	//	SHT31 开启读取一次温湿度数据
		
		TimerID = SetTimer(100, GetSHT31Data);
		
		Sht31Status = SHT_WORKING;
		
		s_SHTGetFlag = FALSE;
	}
}

unsigned char SHT_Init(void)
{
	u8 err;
	float Temp, Humi;
	uint32 error_mark = 0xEEEEEEEE;
	
	Sht31Status = SHT_ERROR;
	IIC_Init();
	delay_ms(10); 
    err = SHT3X_SetPeriodicMeasurement();
	delay_ms(10);
	if(err)
	{
		u1_printf("\r\n [SHT31]Init Fail\r\n");
		Sht31Status = SHT_ERROR;
		memcpy((uint8 *)&s_Temp_Val, (uint8 *)&error_mark, sizeof(uint32));
		memcpy((uint8 *)&s_Humi_Val, (uint8 *)&error_mark, sizeof(uint32));
	}
	else
	{
		SHT_GetValue(&Temp, &Humi);
		s_Temp_Val = Temp;
		s_Humi_Val = Humi;
		if(Temp == ERROR_VALUE || Humi == ERROR_VALUE)
		{
			memcpy((uint8 *)&s_Temp_Val, (uint8 *)&error_mark, sizeof(uint32));
			memcpy((uint8 *)&s_Humi_Val, (uint8 *)&error_mark, sizeof(uint32));
			u1_printf("\r\n [SHT31]Init Fail, Data Err\r\n");
			Sht31Status = SHT_ERROR;
			return 1;
		}
		else
		{		
			u1_printf("\r\n [SHT31]Temp: %2.2f℃  Humi: %2.2f%%\r\n", Temp, Humi);
			Sht31Status = SHT_IDLE;
			Task_Create(TaskForSHT31, 40);
		}
	}
	
	return err;
}
