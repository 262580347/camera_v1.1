/**********************************
说明:BMP280大气压力传感器驱动
	  注意SDO接高电平，IIC地址为0x76
	  SDO接低电平，IIC地址为0x77
	  若接口更改则需修改宏定义及IIC_Init中的GPIO
	  IIC_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, 0x3C);	//睡眠模式  用于低功耗设备
	  IIC_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, 0x3F);	//正常模式  用于传感器
作者:关宇晟
版本:V2017.12.26
***********************************/

#include "main.h"
#include "BMP280.h"
#include <math.h>

#define __STAND_TEMP      15.0f    // 15 C
#define __STAND_PRES      1013.25f // 101.325kPa = 1013.25mbar

#define  GPIO_I2C_DIR_SEND         ((unsigned char)0x00)
#define  GPIO_I2C_DIR_RECV         ((unsigned char)0x01)

#define BMP280_I2C_ADDR                         0x76 // SDO 接地 0x76，SDO接VCC  0x77

#define BMP280_REG_TEMP_XLSB                    0xFC
#define BMP280_REG_TEMP_LSB                     0xFB
#define BMP280_REG_TEMP_MSB                     0xFA
#define BMP280_REG_PRESS_XLSB                   0xF9
#define BMP280_REG_PRESS_LSB                    0xF8
#define BMP280_REG_PRESS_MSB                    0xF7
#define BMP280_REG_CONFIG                       0xF5
#define BMP280_REG_CTRL_MEAS                    0xF4
#define BMP280_REG_STATUS                       0xF3
#define BMP280_REG_RESET                        0xE0
#define BMP280_REG_ID                           0xD0

#define BMP280_REG_DIG_T1                       0x88
#define BMP280_REG_DIG_T2                       0x8A
#define BMP280_REG_DIG_T3                       0x8C

#define BMP280_REG_DIG_P1                       0x8E
#define BMP280_REG_DIG_P2                       0x90
#define BMP280_REG_DIG_P3                       0x92
#define BMP280_REG_DIG_P4                       0x94
#define BMP280_REG_DIG_P5                       0x96
#define BMP280_REG_DIG_P6                       0x98
#define BMP280_REG_DIG_P7                       0x9A
#define BMP280_REG_DIG_P8                       0x9C
#define BMP280_REG_DIG_P9                       0x9E
      
#define BMP280_ID                               0x58	//0x56 0x57 0x58?BMP280
#define BMP280_RESET                            0xB6


#define 	BMP_DETECT_INTERVAL		10

#define		BMP_FORCE_MODE		0x25
#define		BMP_WORK_MODE		0x27
#define		BMP_SLEEP_MODE		0x24

uint16 dig_T1;
int16  dig_T2;
int16  dig_T3;

uint16 dig_P1;
int16  dig_P2;
int16  dig_P3;
int16  dig_P4;
int16  dig_P5;
int16  dig_P6;
int16  dig_P7;
int16  dig_P8;
int16  dig_P9;

int32  t_fine;

#define		BUF_MAX		10

static float s_fBMPTemp = 0.0;
static float s_fBMPPress = 0.0;
static float s_fBMPAltitude = 0.0;

static unsigned int TimerID = 0;
static float s_PressBuf[BUF_MAX];
unsigned char s_BMPGetFlag = FALSE;

BMP_STATUS BmpStatus;	//传感器状态

float GetBMPTemp( void )
{
	return s_fBMPTemp;
}

float GetBMPPress( void )
{
	return s_fBMPPress;
}

float GetBMPAltitude( void )
{
	return s_fBMPAltitude;
}

void StartBMPSensor(void)
{
	if(BmpStatus != BMP_ERROR)
	{
		s_BMPGetFlag = TRUE;
		
		BmpStatus = BMP_WORKING;
	}
	
}


static u8  IIC_Read_Data (unsigned char addr, unsigned char reg, unsigned char *pbuf, int len)
{
    IIC_Start();
    IIC_WriteByte((addr << 1) | 0x00);
	IIC_WaitACK();
    
    IIC_WriteByte(reg);
    IIC_WaitACK();
	
    IIC_Start();
    IIC_WriteByte((addr << 1) | 0x01);
    IIC_WaitACK();
    while (len) 
	{ 
		if (len == 1) 
		{
			*pbuf = IIC_ReadByte(NOACK);
		} 
		else 
		{
			*pbuf = IIC_ReadByte(ACK);
		}
        pbuf++;
        len--;
    }
    IIC_Stop();
    
    return 0;
}

static u8  IIC_Write_Reg (unsigned char addr, unsigned char reg, unsigned char data)
{
	u8 err = 1;
	
    IIC_Start();
    IIC_WriteByte((addr << 1) | 0x00);
	err = IIC_WaitACK();   
	if(err)
		return 1;
    
    IIC_WriteByte(reg);
	err = IIC_WaitACK();   
	if(err)
		return 1;
	IIC_WriteByte(data);

	err = IIC_WaitACK();   
	if(err)
		return 1;
    IIC_Stop();
    
    return 0;
}

static u8  IIC_Read_Reg (unsigned char addr, unsigned char reg, /* @out */unsigned char *data)
{
	u8 err = 1;
	
    IIC_Start();
    IIC_WriteByte((addr << 1) | 0x00);
	err = IIC_WaitACK();   
	if(err)
		return 1;
    IIC_WriteByte(reg);
	err = IIC_WaitACK();   
	if(err)
		return 1;

    IIC_Start();
    IIC_WriteByte((addr << 1) | 0x01);
	err = IIC_WaitACK();   
	if(err)
		return 1;
	
    *data = IIC_ReadByte(NOACK);
    IIC_Stop();

    return 0;
}




static int BMP280Get_Temp_Raw (void)
{
    unsigned char temp[3] = {0};
    int     result = 0;

    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_TEMP_MSB, temp, 3);
    
    result = (temp[0] << 16) | (temp[1] << 8) | (temp[2]);
    
    result >>= 4;
    
    return result;
}

static int BMP280Get_Press_Raw (void)
{
    unsigned char press[3] = {0};
    int     result = 0;

    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_PRESS_MSB, press, 3);
    
    result = (press[0] << 16) | (press[1] << 8) | (press[2]);
    
    result >>= 4;
    
    return result;
}

// Returns temperature in DegC, resolution is 0.01 DegC. Output value of "5123" equals 51.23 DegC.
// t_fine carries fine temperature as global value

static int BMP280Get_Temp_x100 (void)
{
	
	//int32  t_fine;
    int raw_temp;   
    int adc_T;
    int32 var1, var2, T;
	
	raw_temp = BMP280Get_Temp_Raw();
	adc_T = raw_temp;
    
    var1 = ((((adc_T>>3) - ((int32)dig_T1<<1))) * ((int32)dig_T2)) >> 11;
    var2 = (((((adc_T>>4) - ((int32)dig_T1)) * ((adc_T>>4) - ((int32)dig_T1))) >> 12) * ((int32)dig_T3)) >> 14;
    
    t_fine = var1 + var2;
    
    T = (t_fine * 5 + 128) >> 8;
    
    return T;
}

static float BMP280Get_Temp (void)
{
    int tempc_x100 = BMP280Get_Temp_x100();
    return (float)(tempc_x100 / 100.0f);
}

// Returns pressure in Pa as unsigned 32 bit integer in Q24.8 format (24 integer bits and 8 fractional bits).
// Output value of "24674867" represents 24674867/256 = 96386.2 Pa = 963.862 hPa
static unsigned int BMP280Get_Press_Q24_8 (void)
{
    int raw_press = BMP280Get_Press_Raw();
    
    int adc_P = raw_press;
    
    long long var1, var2, p;
    
    var1 = ((long long)t_fine) - 128000;
    var2 = var1 * var1 * (long long)dig_P6;
    var2 = var2 + ((var1*(long long)dig_P5)<<17);
    var2 = var2 + (((long long)dig_P4)<<35);
    var1 = ((var1 * var1 * (long long)dig_P3)>>8) + ((var1 * (long long)dig_P2)<<12);
    var1 = (((((long long)1)<<47)+var1))*((long long)dig_P1)>>33;
    
    if (var1 == 0) {
        return 0; // avoid exception caused by division by zero
    }
    
    p = 1048576- adc_P;
    p = (((p<<31)-var2)*3125)/var1;
    var1 = (((long long)dig_P9) * (p>>13) * (p>>13)) >> 25;
    var2 = (((long long)dig_P8) * p) >> 19;
    p = ((p + var1 + var2) >> 8) + (((long long)dig_P7)<<4);
    
    return (unsigned int)p;
}

static float BMP280Get_Press (void)
{
    float pa;
    unsigned int press_q24_8 = BMP280Get_Press_Q24_8();
    pa = (float)(press_q24_8 / 256.0f);
    
    return pa / 100.0f;
}

// h = Z2-Z1 = 18400(1+atm)LgP1/P2
static float BMP280Get_Altitude (float temperature, float pressure)
{
    return 18400.0f * (1.0f + temperature / 273.0f) * log10(__STAND_PRES / pressure);
}

static void GetBMPData(void)
{
	float fBMPPress = 0.0, fBMPTemp;
	u8 ack = 0;
	static u8  s_DetCount = 0, s_DetNum = 0;
	uint32 error_mark = 0xEEEEEEEE;

	fBMPTemp = BMP280Get_Temp();
	fBMPPress = BMP280Get_Press();
	BMP280Get_Altitude(fBMPTemp, fBMPPress);
	if(fBMPPress  > 1100 || fBMPPress < 300)
	{
		s_fBMPPress = ERROR_VALUE;
		memcpy((uint8 *)&s_fBMPPress, (uint8 *)&error_mark, sizeof(uint32));
		BmpStatus = BMP_ERROR;
		BMP280_Init();
	}
	else
	{
		if(s_DetCount < 3)
		{
			s_PressBuf[s_DetCount] = fBMPPress;
			s_fBMPPress = fBMPPress;
		}
		else
		{
			s_PressBuf[s_DetNum] = fBMPPress;
			s_fBMPPress = Mid_Filter(s_PressBuf, s_DetCount);
		}
		
		s_DetCount++;
		s_DetNum++;
		
		if(s_DetCount >= BUF_MAX)
		{
			s_DetCount = BUF_MAX;
		}
		
		if(s_DetNum >= BUF_MAX)
		{
			s_DetNum = 0;
		}
												
//		u1_printf("\r\n Pressure:%2.2fhPa Avg:%2.2fhPa\r\n", fBMPPress, s_fBMPPress);
	}

	if(fBMPPress >= 300 && fBMPPress <= 550 )	//错误的大气压力范围，重新初始化芯片
	{
		memcpy((uint8 *)&s_fBMPPress, (uint8 *)&error_mark, sizeof(uint32));
		BMP280_Init();
		BmpStatus = BMP_ERROR;
	}

	ack = IIC_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, BMP_SLEEP_MODE);
	if(ack == 0)
	{
//		u1_printf(" BMP OK\r\n");
	}
	else
	{
		u1_printf(" BMP Err\r\n");
	}
	BmpStatus = BMP_IDLE;
	KillTimer(TimerID);
}

void TaskForBMP(void)
{	
	if(s_BMPGetFlag == TRUE)
	{
		IIC_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, BMP_FORCE_MODE);
		
		s_BMPGetFlag = FALSE;
		
		BmpStatus = BMP_WORKING;
		
		TimerID = SetTimer(70, GetBMPData);		
	}
}

void BMP280_Init(void)
{
    unsigned char uc_ID = 0, err = 0;
	uint32 error_mark = 0xEEEEEEEE;
	
	BmpStatus = BMP_ERROR;
    IIC_Init();
        
    err = IIC_Read_Reg (BMP280_I2C_ADDR, BMP280_REG_ID, &uc_ID);
    
	if(err)
	{
		u1_printf("\r\n [BMP280]Init Fail\r\n");
		BmpStatus = BMP_ERROR;
		memcpy((uint8 *)&s_fBMPPress, (uint8 *)&error_mark, sizeof(uint32));
		return;
	}
	
    if (uc_ID != BMP280_ID) 
	{
        u1_printf("\r\n [BMP280] uc_ID = 0x%02x，not BMP280 ID!Init failed!\r\n", uc_ID);
		BmpStatus = BMP_ERROR;
		memcpy((uint8 *)&s_fBMPPress, (uint8 *)&error_mark, sizeof(uint32));
		return;
    }
   
    u1_printf("\r\n [BMP280] ID = 0x%02x, Init Succeed ...\n", uc_ID);
	
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_T1, (unsigned char *)&dig_T1, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_T2, (unsigned char *)&dig_T2, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_T3, (unsigned char *)&dig_T3, 2);

    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P1, (unsigned char *)&dig_P1, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P2, (unsigned char *)&dig_P2, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P3, (unsigned char *)&dig_P3, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P4, (unsigned char *)&dig_P4, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P5, (unsigned char *)&dig_P5, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P6, (unsigned char *)&dig_P6, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P7, (unsigned char *)&dig_P7, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P8, (unsigned char *)&dig_P8, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P9, (unsigned char *)&dig_P9, 2);
    
	Task_Create(TaskForBMP, 40);
	BmpStatus = BMP_IDLE;
	StartBMPSensor();
}
