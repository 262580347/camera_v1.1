#include "main.h"
#include "SIM.h"

#define COM_DATA_SIZE	256

static void SIMObser(unsigned short nMain10ms);

static u8 m_CommBuff[COM_DATA_SIZE];

static u16 m_CirWPinter = 0;

static u16 m_CirRPinter = 0;

static u8 s_DataPackFlag = FALSE;		//接收到数据包

static unsigned char s_Sim800CReadyFlag = 0;	//模块连接状态

static unsigned char s_ServerConfigCmdFlag = FALSE;	//服务端即将下发配置信息

unsigned char GetSim800cReadyFlag( void )
{
	return s_Sim800CReadyFlag;
}

void SetSim800cReadyFlag( unsigned char ucFlag )
{
	s_Sim800CReadyFlag = ucFlag;
}

static void Clear_COMBuff(void)
{
	u16 i;
	
	for(i=0; i<COM_DATA_SIZE; i++)
	{
		m_CommBuff[i] = 0;		
	}
		m_CirWPinter = 0;

		m_CirRPinter = 0;	
}

void OnSIMComm(u8 CommData)
{
	m_CommBuff[m_CirWPinter++] = CommData;
	
	if(m_CirWPinter >= COM_DATA_SIZE)
	{
		m_CirWPinter = 0;
	}
	if(m_CirWPinter == m_CirRPinter)
	{
		m_CirRPinter++;
		if(m_CirRPinter	>= COM_DATA_SIZE)
		{
		    m_CirRPinter = 0; 
		}
	}	
}

void SIM800CPortInit(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA , ENABLE);	 

	GPIO_InitStructure.GPIO_Pin = SIM800C_ENABLE_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(SIM800C_ENABLE_TYPE, &GPIO_InitStructure);	
			
}

void SIM800CProcess(unsigned short nMain10ms)
{
	u8 i;
	u16 nTemp = 0;
	static u16 s_LastTime = 0, s_OverCount = 0, s_ATAckCount = 0;
	static unsigned int s_RTCSumSecond = 0, s_AliveRTCCount = 0xfffe0000;
	static u8 s_APNFlag = 1, s_State = 1, s_Step = 1, RunOnce = FALSE, s_ErrCount = 0, s_RetryCount = 0, s_Reboot = 0;
	char * p1 = NULL, buf[10], str[100];
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	p_sys = GetSystemConfig();
	
	if(RunOnce == FALSE)
	{
		RunOnce = TRUE;
		s_LastTime = GetSystem10msCount();
	}
			
	SIMObser(GetSystem10msCount());
	
	switch(s_State)
	{	
		case 1:   //模块断电
			s_RetryCount++;
			SetSim800cReadyFlag(0);
			if(s_RetryCount >= 3)
			{
				u1_printf("超时未连上GPRS\r\n");
				s_Reboot++;
				if(s_Reboot >= 6)
				{
					s_Reboot = 0;
					u1_printf(" SIM长时间链接不上,设备重启\r\n");
					delay_ms(100);
					while (DMA_GetCurrDataCounter(DMA1_Channel4));
					Sys_Soft_Reset();	
				}
				s_RetryCount = 0;
				s_State = 52;
				break;
			}
			SIM800CPortInit();
			SIM_PWR_OFF();
			USART2_Config(SIM_BAND_RATE);
			s_LastTime = GetSystem10msCount();
			s_State++;
			u1_printf("[SIM800C]模块断电，延时3S.\r\n");
		break;
		 
		case 2:	//断电后延时5S上电，模块上电自动开机
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 300 )
			{
				u1_printf("[SIM800C]模块上电，延时14S.\r\n");
				SIM_PWR_ON();
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		break;
			
		case 3: 	//上电完成后，初始化端口
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 1400 )
			{
				u1_printf("[SIM800C]模块上电完成，检测模块波特率.\r\n");
				Uart_Send_Str(SIM_COM, "AT\r");
				delay_ms(50);
				Uart_Send_Str(SIM_COM, "AT\r");
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		break;
			
		case 4:  	//检测模块的波特率
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 5 )
			{
				Clear_Uart2Buff();
				Uart_Send_Str(SIM_COM, "AT+CIURC=0\r");	//发送"AT\r"，查询模块是否连接，返回"OK"
				u1_printf("[SIM800C]AT+CIURC=0\r\n");
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		break;
		
		case 5:	//延时100MS,检测响应数据
			if(g_Uart2RxFlag == TRUE)
			{
				g_Uart2RxFlag = FALSE;
				
				if(strstr((char *)g_USART2_RX_BUF, "OK"))
				{
					s_ErrCount = 0;
					s_State++;	
					u1_printf("Close Call Ready\r\n");
				}			
				Clear_Uart2Buff();
				
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 30 )
			{				
					u1_printf("[SIM800C]OK无响应\r\n");
					s_State--;   //回上一个状态
					s_ErrCount++;   //重试次数加1
					if(s_ErrCount >= 20 )
					{
						s_ErrCount = 0;
						s_State = 1;	//模块重新上电初始化
					}						
			}
	 	break;
		
		case 6:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 30) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			

				switch(s_Step)
				{
					case 1:
						Uart_Send_Str(SIM_COM, "ATE0\r");
						u1_printf("[SIM]->ATE0\r\n");
						s_LastTime = GetSystem10msCount();
					break;
					case 2:
						Uart_Send_Str(SIM_COM, "AT+CGMI\r");
						u1_printf("[SIM]->查询制造商名称\r\n");
						s_LastTime = GetSystem10msCount();
					break;
					case 3:
						Uart_Send_Str(SIM_COM, "AT+CGMM\r");
						u1_printf("[SIM]->查询模块名称\r\n");
						s_LastTime = GetSystem10msCount();
					break;
					case 4:
						Uart_Send_Str(SIM_COM, "AT+CGSN\r");
						u1_printf("[SIM]->查询模块序列号\r\n");
						s_LastTime = GetSystem10msCount();
					break;
					case 5:
						Uart_Send_Str(SIM_COM, "AT+CSQ\r");
						u1_printf("[SIM]->查询模块信号质量\r\n");
						s_LastTime = GetSystem10msCount();		
					break;
					case 6:
						Uart_Send_Str(SIM_COM, "AT+COPS?\r");
						u1_printf("[SIM]->查询网络运营商\r\n");
						s_LastTime = GetSystem10msCount();
					break;	
					case 7:
						Uart_Send_Str(SIM_COM, "AT+CPIN?\r");
						u1_printf("[SIM]->查询模块SIM状态\r\n");
						s_LastTime = GetSystem10msCount();
					break;
					case 8:
						Uart_Send_Str(SIM_COM, "AT+CGATT=1\r");
						u1_printf("[SIM]->设置模块附着GPRS状态\r\n");
						s_LastTime = GetSystem10msCount();				
					break;				
					case 9:
						Uart_Send_Str(SIM_COM, "AT+CGREG?\r");
						u1_printf("[SIM]->查询模块GPRS网络注册状态\r\n");
						s_LastTime = GetSystem10msCount();
					break;		
					case 10:
						Uart_Send_Str(SIM_COM, "AT+CREG?\r");
						u1_printf(" [SIM]->查询模块GSM网络注册状态\r\n");
						s_LastTime = GetSystem10msCount();
					break;					
					case 11:
						Uart_Send_Str(SIM_COM, "AT+CGATT?\r");
						u1_printf("[SIM]->查询模块GPRS附着状态\r\n");
						s_LastTime = GetSystem10msCount();
					break;				
					case 12:
						Uart_Send_Str(SIM_COM, "AT+CGATT=1\r");
						u1_printf("[SIM]->设置模块附着GPRS状态\r\n");
						s_LastTime = GetSystem10msCount();
					break;
				}
				s_State++;
			}	
					
		break;
		
		case 7:

			if(g_Uart2RxFlag == TRUE)
			{
				g_Uart2RxFlag = FALSE;
				
				for(i=0; i<g_USART2_RX_CNT; i++)
					u1_printf("%c", g_USART2_RX_BUF[i]);
											
				switch(s_Step)
				{
					case 1:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();							
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 2:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
						//	u1_printf("制造商：%s\r\n", g_USART2_RX_BUF);
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();							
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 3:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
						//	u1_printf("模块名称：%s\r\n", g_USART2_RX_BUF);
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();							
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 4:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
						//	u1_printf("序列号：%s\r\n", g_USART2_RX_BUF);
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();							
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 5:	
					//	u1_printf("检测信号质量： ");
						if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CSQ:")) != NULL )
						{							
							//u1_printf("%s\r\n", g_USART2_RX_BUF);							
							strncpy(buf,p1+6,2);

							nTemp = atoi( buf);
							if( nTemp < 10 )								
							{
								s_State--;	
								s_ErrCount++;
								u1_printf(" 信号差\r\n");
							}
							else
							{
								s_State--;	
								s_Step++;	
								s_ErrCount = 0;
								u1_printf(" 信号正常\r\n");
							}
						}
						else
						{
//							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}		
					break;		
						
					case 6:
						//u1_printf(" 运营商名称： ");
						if((p1 = strstr((const char *)g_USART2_RX_BUF,"+COPS:")) != NULL )
						{				
							if( (strstr((const char *)g_USART2_RX_BUF,"UNICOM")) != NULL )  
								s_APNFlag = 1;	
							else
								s_APNFlag = 0;	
	
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();		
						//	u1_printf("%s\r\n", g_USART2_RX_BUF);
						}
						else
						{
							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}
					break;	
						
					case 7:
					//	u1_printf("检测SIM卡： ");
						if((strstr((const char *)g_USART2_RX_BUF,"+CPIN: READY")) != NULL )
						{					
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();		
							u1_printf("....SIM卡正常\r\n");
						}
						else
						{
//							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}						
					break;	
						
					case 8:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							u1_printf(" 设置GPRS附着成功\r\n");
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();	
						}
						else
						{
							s_State--;	
							s_ErrCount++;
							if(s_ErrCount == 30)
							{		
								u1_printf(" 设置GPRS附着状态失败，检测SIM卡\r\n");
								s_ErrCount = 0;								
								s_Step++;
							}
						}
						
					break;
										
					case 9:
						//u1_printf(" 模块注册网络： ");
						if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CGREG:")) != NULL )
						{						
							//u1_printf("%s\r\n", g_USART2_RX_BUF);
							
							nTemp = atoi( (const char *) (p1+10));
							switch( nTemp )								
							{
								case 0:
								u1_printf("[SIM800C]模块注册--未注册\r\n");
								s_State--;
								s_ErrCount++;
								break;
								
								case 1:
								u1_printf("[SIM800C]模块注册网络--成功\r\n");
								s_State--;
								s_Step++;
								s_Step++;		//调过GSM查询
								s_ErrCount = 0;
								break;
								
								case 2:
								u1_printf("[SIM800C]模块注册--正在注册，查询次数=%d\r\n",s_ErrCount);
								s_State--;
								s_ErrCount++;

								break;
								
								case 3:
								u1_printf("\r\n [SIM800C]模块注册--注册被拒, 次数=%d\r\n",s_ErrCount);
								s_State--;
								s_ErrCount++;
								break;
								
								default:
								u1_printf("[SIM800C]模块注册--注册状态未识别\r\n");
								s_State--;
								s_ErrCount++;
								break;	
							}
							
							if(s_ErrCount == 50)
							{		
								u1_printf(" GPRS注册网络失败，查询GSM\r\n");
								s_ErrCount = 0;								
								s_Step++;
							}
							
						}
						else
						{
//							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}	
							
					break;
						
					case 10:
						//u1_printf(" 模块注册网络： ");
						if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CREG:")) != NULL )
						{						
							//u1_printf("%s\r\n", g_USART2_RX_BUF);
							
							nTemp = atoi( (const char *) (p1+9));
							switch( nTemp )								
							{
								case 0:
								u1_printf("[SIM800C]模块注册--未注册\r\n");
								s_State--;
								s_ErrCount++;
								break;
								
								case 1:
								u1_printf("[SIM800C]模块注册网络--成功\r\n");
								s_State--;
								s_Step++;
								s_ErrCount = 0;
								break;
								
								case 2:
								u1_printf("[SIM800C]模块注册--正在注册，查询次数=%d\r\n",s_ErrCount);
								s_State--;
								s_ErrCount++;

								break;
								
								case 3:
								u1_printf("\r\n [SIM800C]模块注册--注册被拒, 次数=%d\r\n",s_ErrCount);
								s_State--;
								s_ErrCount++;
								break;
								
								default:
								u1_printf("[SIM800C]模块注册--注册状态未识别\r\n");
								s_State--;
								s_ErrCount++;
								break;	
							}
							
							if(s_ErrCount == 50)
							{		
								u1_printf(" GSM注册网络失败，查询附着状态\r\n");
								s_ErrCount = 0;								
								s_Step++;
							}
						}
						else
						{
//							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}	
							
					break;
					
					case 11:	
						//u1_printf("GPRS附着状态： ");
						if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CGATT:")) != NULL )
						{					
							//u1_printf("%s\r\n", g_USART2_RX_BUF);
							
							nTemp = atoi( (const char *) (p1+8));
							if( nTemp == 0 )								
							{
								s_State--;	
								s_ErrCount++;
								if(s_ErrCount == 30)
								{									
									s_Step++;
									s_ErrCount++;
								}
							}
							else
							{
								s_State++;	
								s_ATAckCount = 0;
								s_Step = 1;	
								s_ErrCount = 0;
							}
						}
						else
						{
							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}		
					break;		
						
					case 12:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							s_Step--;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();	
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
						
					break;
						
				}
				
				if(s_ErrCount > 120 )
				{
					s_ErrCount = 0;
					s_State = 1;
					s_Step = 1;
				}
				Clear_Uart2Buff();
				s_LastTime = GetSystem10msCount();
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 500) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
	
				s_ATAckCount++;
				
				if(s_ATAckCount >= 5)
				{
					s_ATAckCount = 0;
					u1_printf(" AT无响应\r\n");
					s_State = 1;
					s_Step = 1;
				}
				else
				{
					s_State--;
					u1_printf(" AT指令超时\r\n");
				}
				s_LastTime = GetSystem10msCount();
				s_ErrCount++;
			}			
		break;
		
		case 8:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 30) 
			{
				switch(s_Step)
				{
						case 1:
							Uart_Send_Str(SIM_COM, "AT+CIPCLOSE=1\r");
							u1_printf("[SIM]->关闭当前链接\r\n");
							s_LastTime = GetSystem10msCount();
						break;
						case 2:
							Uart_Send_Str(SIM_COM, "AT+CIPSHUT\r");
							u1_printf("[SIM]->关闭所有链接\r\n");
							s_LastTime = GetSystem10msCount();
						break;
						case 3:
							Uart_Send_Str(SIM_COM, "AT+CIPMODE=1\r");
							u1_printf("[SIM]->设置为透传模式\r\n");
							s_LastTime = GetSystem10msCount();
						break;
						case 4:
							if(s_APNFlag)
								Uart_Send_Str(SIM_COM, "AT+CSTT=\"uninet\"\r");
							else
								Uart_Send_Str(SIM_COM, "AT+CSTT=\"cmnet\"\r");
							u1_printf("[SIM]->建立APN\r\n");
							s_LastTime = GetSystem10msCount();
						break;
						case 5:
							Uart_Send_Str(SIM_COM, "AT+CIICR\r");
							u1_printf("[SIM]->建立无线链路GPRS\r\n");
							s_LastTime = GetSystem10msCount();
						break;
						case 6:
							Uart_Send_Str(SIM_COM, "AT+CIFSR\r");
							u1_printf("[SIM]->获得本地IP地址\r\n\r\n");
							s_LastTime = GetSystem10msCount();
						break;
						
						case 7:
							s_LastTime = GetSystem10msCount();
						break;
						
						default:
							s_LastTime = GetSystem10msCount();
							s_State = 1;
							s_Step = 1;
						break;
				}
				s_State++;
			}
		
		break;
	
		case 9:
			if(g_Uart2RxFlag == TRUE)
			{
				g_Uart2RxFlag = FALSE;
				
				for(i=0; i<g_USART2_RX_CNT; i++)
					u1_printf("%c", g_USART2_RX_BUF[i]);
											
				switch(s_Step)
				{
					case 1:
						if((strstr((const char *)g_USART2_RX_BUF,"+CIPCLOSE")) != NULL )
						{
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
													
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"ERROR")) != NULL )
						{
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 2:
						if((strstr((const char *)g_USART2_RX_BUF,"SHUT OK")) != NULL )
						{
						//	u1_printf("关闭移动场景\r\n");
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
													
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 3:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
						//	u1_printf("设置为透传模式\r\n");
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
												
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 4:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
						//	u1_printf("APN OK\r\n");
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
													
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
					
					case 5:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
						//	u1_printf("激活移动场景 OK\r\n");
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
												
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
						
					break;
						
					case 6:
						//u1_printf("模块本地IP：");
						if((strstr((const char *)g_USART2_RX_BUF,".")) != NULL )
						{					
							s_Step = 1;	
							s_State++;		
							s_ATAckCount = 0;
							s_ErrCount = 0;
						
							//u1_printf("%s\r\n", g_USART2_RX_BUF);
						}
						else
						{
							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}						
					break;
					
					case 7:	
						u1_printf("模块收到数据包含IP头=%s\r\n", g_USART2_RX_BUF);
						s_State++;
						s_Step = 1;
					break;
						
				}
				
				if(s_ErrCount > 50 )
				{
					s_ErrCount = 0;
					s_State = 1;
					s_Step = 1;
				}
				s_LastTime = GetSystem10msCount();	
				
				Clear_Uart2Buff();

			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1000) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
	
				s_ATAckCount++;
				
				if(s_ATAckCount >= 5)
				{
					s_ATAckCount = 0;
					u1_printf(" AT无响应\r\n");
					s_State = 1;
					s_Step = 1;
				}
				else
				{
					s_State--;
					u1_printf(" AT指令超时\r\n");
				}
				s_LastTime = GetSystem10msCount();
				s_ErrCount++;
			}	
		break;		
		
		case 10:
			u1_printf("[SIM800C]建立TCP链接\r\n");
			sprintf(str,"AT+CIPSTART=\"TCP\",\"%d.%d.%d.%d\",%d\r"
			,p_sys->Gprs_ServerIP[0]
			,p_sys->Gprs_ServerIP[1]
			,p_sys->Gprs_ServerIP[2]
			,p_sys->Gprs_ServerIP[3]
			,p_sys->Gprs_Port	);

			Uart_Send_Str(SIM_COM, str);	
			Uart_Send_Str(USART1, str);	//串口输出AT命令
			u1_printf("\n");
			s_LastTime = GetSystem10msCount();
			s_State++;		
		//	s_ErrCount = 0;		
		break;
		
		case 11:
			if(g_Uart2RxFlag == TRUE)
			{
				g_Uart2RxFlag = FALSE;
				
				for(i=0; i<g_USART2_RX_CNT; i++)
					u1_printf("%c", g_USART2_RX_BUF[i]);
													
				if((strstr((const char *)g_USART2_RX_BUF,"+ALREADY CONNECT")) != NULL )
				{
					s_Step = 1;	
					s_State++;		
					s_ErrCount = 0;
					SetSim800cReadyFlag(1);		//连接成功========================
					u1_printf("GPRS链接已建立\r\n");				
				}
				else if((strstr((const char *)g_USART2_RX_BUF,"CONNECT OK")) != NULL )
				{
					s_Step = 1;	
					s_State++;		
					s_ErrCount = 0;
					SetSim800cReadyFlag(1);		//连接成功========================
					u1_printf("GPRS链接成功\r\n");				
				}
				else if((strstr((const char *)g_USART2_RX_BUF,"CONNECT FAIL")) != NULL )
				{		
					s_ErrCount++;
					s_State = 8;
					s_Step = 1;	
					u1_printf("GPRS链接失败\r\n");				
				}
				else if((strstr((const char *)g_USART2_RX_BUF,"CONNECT")) != NULL )
				{
					s_Step = 1;	
					s_State++;		
					s_ErrCount = 0;
					SetSim800cReadyFlag(1);		//连接成功========================
					u1_printf("GPRS链接成功\r\n");				
				}
				else if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
				{		
					u1_printf("\r\n 等待Connect...\r\n");	
					s_LastTime = GetSystem10msCount();				
				}
				else if((strstr((const char *)g_USART2_RX_BUF,"ERROR")) != NULL )
				{		
					s_ErrCount++;
					s_State--;		
				}
				else
				{
					s_State--;	
					s_ErrCount++;
					u1_printf("未识别指令\r\n");
				}
					
				
				if(s_ErrCount > 5 )
				{
					s_ErrCount = 0;
					s_State = 1;
					s_Step = 1;
				}
				s_LastTime = GetSystem10msCount();	
				
				Clear_Uart2Buff();

			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1000) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
	
				s_State = 1;
				s_Step = 1;
				u1_printf(" AT无响应\r\n");
				s_LastTime = GetSystem10msCount();
				s_ErrCount++;
			}			
		break;
		
		case 12:
			s_OverCount = 0;
			s_State = 50;
			s_LastTime = GetSystem10msCount();
		break;
		
		case 50:
			if(GetSim800cReadyFlag())		//SIM就绪
			{
				if(DifferenceOfRTCTime(GetRTCSecond(), s_AliveRTCCount) >= p_sys->Heart_interval)	
				{
					s_AliveRTCCount = GetRTCSecond();
					Mod_Send_Alive(SIM_COM);
					delay_ms(100);
				}
				
				if(g_PowerDetectFlag && g_SHTGetFlag && g_MAXGetFlag && g_BMPGetFlag)
				{
					Mod_Report_LowPower_Data(SIM_COM);	//发送数据包
					s_State++;
					s_LastTime = GetSystem10msCount();
				}
			}
			else
			{
				s_State = 1;
				s_Step = 1;
				s_ErrCount = 0;
				s_LastTime = GetSystem10msCount();
			}
		break;
		
		case 51:
			if(s_DataPackFlag)//非长连接设备取消心跳包  2019.2.12
			{
				s_State++;
				s_LastTime = GetSystem10msCount();
				s_DataPackFlag = FALSE;
				s_OverCount = 0;
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 1000 )
			{
				s_State--;
				s_LastTime = GetSystem10msCount();
				u1_printf("\r\n 超时未收到回复包  重发\r\n");
				Clear_Uart2Buff();
				s_OverCount++;
				if(s_OverCount >= 2)
				{
					s_OverCount = 0;
					s_State = 1;
					s_Step = 1;
					s_ErrCount = 0;
				}
			}		
		break;
					
		case 52:
			if(s_ServerConfigCmdFlag == FALSE)
			{
				if(CalculateTime(GetSystem10msCount(),s_LastTime) >= 100)		//延时1s等待GPRS下行数据
				{
					u1_printf("\r\n 等待服务器下行数据超时，休眠\r\n");
					s_RTCSumSecond = GetRTCSecond();
					s_State++;
					s_LastTime = GetSystem10msCount();
				}
			}
			else if(s_ServerConfigCmdFlag)		//收到关闭Socket指令，可以休眠
			{
				u1_printf("\r\n 收到应答包,休眠\r\n");
				s_RTCSumSecond = GetRTCSecond();
				s_State++;
				s_LastTime = GetSystem10msCount();
				s_ServerConfigCmdFlag = FALSE;
			}
		break;
		
		case 53:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= p_sys->Data_interval)		//达到上报数据时间点
			{
				s_RTCSumSecond = GetRTCSecond();	
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
						
				u1_printf("\r\n [%02d:%02d:%02d][SIM]重新上电\r\n", RTC_TimeStructure.RTC_Hours,  RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
				s_RetryCount = 0;
//				Clear_Flag();			//清传感器标志，开启传感器检测
				s_State++;
			}		
			else			//未到上报时间，继续休眠
			{
				if(g_PowerDetectFlag && g_SHTGetFlag && g_MAXGetFlag && g_BMPGetFlag)
				{
					s_LastTime = GetSystem10msCount();
					SetStopModeTime(TRUE, STOP_TIME);
				}					
			}
		break;
			
		case  54:
			if(g_PowerDetectFlag && g_SHTGetFlag && g_MAXGetFlag && g_BMPGetFlag)	//SIM重新上电初始化
			{
				s_State = 1;
				s_Step = 1;
				s_ErrCount = 0;
				s_LastTime = GetSystem10msCount();
				SetSim800cReadyFlag(0);
			}
		break;		
	}
	
}


void OnRecSimData(CLOUD_HDR *pMsg, u8 *data, u8 lenth)
{
	u8 i, sendbuf[100], sendlenth, SetTimeFlag = FALSE, ReturnVal = 0;
	CLOUD_HDR *hdr;
	u32 RTCTime = 0;
	RTC_TimeTypeDef RTC_TimeStructure;	
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
	//解析协议包
	u1_printf("\r\n [%02d:%02d:%02d][SIM]<-Version:%02d Device:(%d)%04d Dir:%X Seq:%d Len:%d CMD:%02X Data:", \
	RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, pMsg->protocol, ((pMsg->device_id)&0x10000000) >> 28, (pMsg->device_id)&0xfffffff, pMsg->dir, pMsg->seq_no, pMsg->payload_len, pMsg->cmd);
	for(i=0; i<lenth; i++)
		u1_printf("%02X ", data[i]);
	u1_printf("\r\n");
	
	if(pMsg->cmd == CMD_HEATBEAT)
	{
		RTC_TimeStructure.RTC_H12     = RTC_H12_AM;				
		RTC_TimeStructure.RTC_Hours   = data[2];
		RTC_TimeStructure.RTC_Minutes = data[1];
		RTC_TimeStructure.RTC_Seconds = data[0];;
		u1_printf(" 服务器时间:[%02d:%02d:%02d]\r\n", data[2], data[1], data[0]);
		RTCTime = data[2]*3600 + data[1]*60 + data[0];
		
		if(RTCTime > GetRTCSecond())		//与服务器误差超过120S则同步服务器时间
		{
			if(DifferenceOfRTCTime(RTCTime, GetRTCSecond()) > 20)					
			{
				SetTimeFlag = TRUE;
			}
			else 
			{
				SetTimeFlag = FALSE;
			}
		}
		else
		{
			if(DifferenceOfRTCTime(GetRTCSecond(), RTCTime) > 20)
			{
				
				SetTimeFlag = TRUE;
			}
			else 
			{
				SetTimeFlag = FALSE;
			}
		}
		
		if(SetTimeFlag)
		{
			if(RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure)  != ERROR)
			{
				u1_printf(" ———同步远程时间成功————");
				RTC_TimeShow();
			}
			else
			{
				u1_printf("   同步远程时间失败\r\n");
			}
		}
		else
		{
			u1_printf(" ———不同步服务器时间————");
		}
	}

	if(pMsg->cmd == CMD_CLOSE_SOCKET)	//关闭Socket指令
	{		
		u1_printf(" 收到(%d)服务器关闭Socket命令\r\n", ((int)pMsg->device_id)&0xfffffff);
		
		Mod_Send_Ack(SIM_COM, pMsg, data, lenth);
					
		delay_ms(500);		//等待发送完成
		
		s_ServerConfigCmdFlag = TRUE;	//收到关闭Socket指令，可以休眠
	}
	
	if(pMsg->cmd == CMD_REPORT_D)
	{
		u1_printf(" 收到服务器上报数据回复,上报数据成功\r\n");
		
		s_DataPackFlag = TRUE;
	}
	
	if(pMsg->cmd == CMD_GET_CONFIG)//获取设备配置
	{
		u1_printf("\r\n 收到服务端获取配置指令\r\n");		
		System_get_config(SIM_COM, pMsg, data, lenth);
		delay_ms(300);		//等待发送完成	
	}
	
	if(pMsg->cmd == CMD_SET_CONFIG) //设置设备配置
	{		
		u1_printf("\r\n 收到服务端修改配置指令\r\n");
			
		memcpy(sendbuf, pMsg, sizeof(CLOUD_HDR));
		sendlenth = sizeof(CLOUD_HDR);
		
		hdr = (CLOUD_HDR *)sendbuf;
		hdr->protocol = swap_word(hdr->protocol);	
		hdr->device_id = swap_dword((hdr->device_id) | 0x10000000);
		hdr->seq_no = swap_word(hdr->seq_no);	
		hdr->dir = 1;
		hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG)+4);
		memcpy(&sendbuf[sizeof(CLOUD_HDR)], data, lenth);
		ReturnVal = System_set_config(SIM_COM, sendbuf, lenth + sendlenth);
		if(ReturnVal == 0)
		{
			return;
		}
		u1_printf("\r\n 更新设备参数，等待设备重启\r\n");
		delay_ms(1500);
		delay_ms(1500);
		while (DMA_GetCurrDataCounter(DMA1_Channel4));
		Sys_Soft_Reset();
	}
}
	
static void SIMObser(unsigned short nMain10ms)
{
	static u8 FrameID = 0, special_flag = FALSE, s_HeadFlag = FALSE;
	static u16 data_lenth = 0, s_LastTime = 0, i = 0;
	static CLOUD_HDR m_RxFrame;
	static u8 RxData, databuff[COM_DATA_SIZE];
	u8 checksum;
	
	if(s_HeadFlag == TRUE)
	{
		if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 50)
		{
			if(g_USART2_RX_BUF[0] == 0x7e && g_USART2_RX_BUF[g_USART2_RX_CNT - 1] == 0x21)
			{
				OnRecSimData(&m_RxFrame, databuff, data_lenth);
				u1_printf("\r\n+++++++SIM\r\n");
			}
			else
			{
				u1_printf("\r\n SIM接收超时,收到%d个数据\r\n", g_USART2_RX_CNT);
			
				u1_printf("Rec:  ");
				for(i=0; i<g_USART2_RX_CNT; i++)
				{
					u1_printf("%02X ", g_USART2_RX_BUF[i]);
				}
				u1_printf("\r\n");
			}
			s_HeadFlag = FALSE;
			
			Clear_Uart2Buff();
			Clear_COMBuff();
			memset(databuff, 0, COM_DATA_SIZE);
			data_lenth = 0;
			i = 0;
			FrameID = 0;
			return ;
		}
	}
		
	if(m_CirRPinter != m_CirWPinter)
	{
		RxData = m_CommBuff[m_CirRPinter++];
		if(m_CirRPinter >= COM_DATA_SIZE)
		{
			m_CirRPinter = 0;
		}
		if(RxData == 0x7d && special_flag == FALSE)
		{
			special_flag = TRUE;
			return;
		}
		if(special_flag == TRUE)
		{
			special_flag = FALSE;
			if(RxData == 0x5d)
			{
				RxData = 0x7d;
			}
			else if(RxData == 0x5e)
			{
				RxData = 0x7e;
			}
			else if(RxData == 0x51)
			{
				RxData = 0x21;
			}
		}



		switch(FrameID)
		{
			case 0:
				if(RxData == 0X7e)
				{
					s_HeadFlag = TRUE;
					s_LastTime = GetSystem10msCount();
					FrameID++;
				}
				break;
			case 1:
				m_RxFrame.protocol = RxData;
				FrameID++;
			break;
			case 2:
				m_RxFrame.protocol = (m_RxFrame.protocol << 8) | RxData;
				FrameID++;
			break;
			case 3:
				m_RxFrame.device_id = RxData;
				FrameID++;
			break;
			case 4:
				m_RxFrame.device_id = (m_RxFrame.device_id << 8) | RxData;
				FrameID++;
			break;
			case 5:
				m_RxFrame.device_id = (m_RxFrame.device_id << 8) | RxData;
				FrameID++;
			break;
			case 6:
				m_RxFrame.device_id = (m_RxFrame.device_id << 8) | RxData;
				FrameID++;
			
			break;
			case 7:
				m_RxFrame.dir = RxData;
				FrameID++;
			break;
			case 8:
				m_RxFrame.seq_no = RxData;
				FrameID++;
			break;
			case 9:
				m_RxFrame.seq_no = (m_RxFrame.seq_no << 8) | RxData;
				FrameID++;
			break;
			case 10:
				m_RxFrame.payload_len = RxData;
				FrameID++;
			break;
			case 11:
				m_RxFrame.payload_len = (m_RxFrame.payload_len << 8) | RxData;
				data_lenth = m_RxFrame.payload_len;
				FrameID++;
			break;
			case 12:
				m_RxFrame.cmd = RxData;				
				i = 0;
			    memset(databuff, 0, 100);
				FrameID++;
				if(data_lenth == 0)
				{
					FrameID = 14;
					break;
				}	
				else if(data_lenth >= 100)
				{
					u1_printf("\r\n too long %d\r\n", data_lenth );
					Clear_Uart2Buff();
					memset(databuff, 0, 100);
					data_lenth = 0;
					i = 0;
					FrameID = 0;
				}
			break;
			case 13:				
				databuff[i++] = RxData;
				if(i >= data_lenth)
				{
					FrameID++;
				}
			break;		
			case 14:
				checksum = RxData;
				if(CheckHdrSum(&m_RxFrame, databuff, data_lenth) != checksum)
				{
					u1_printf("[SIM]Check error...");
					u1_printf("Rec:%02X, CheckSum:%02X\r\n", checksum, CheckHdrSum(&m_RxFrame, databuff, data_lenth));
					FrameID = 0;	
					s_HeadFlag = FALSE;					
					Clear_Uart2Buff();	
					Clear_COMBuff();
					s_HeadFlag = FALSE;		
					memset(databuff, 0, 100);
					data_lenth = 0;
					i = 0;
				}else
				FrameID++;
			break;
			case 15:
				if(RxData == 0x21)	
				{								
					OnRecSimData(&m_RxFrame, databuff, data_lenth);
				}
				else
				{				
					u1_printf("end error!\r\n");
				}
				s_HeadFlag = FALSE;		
				Clear_Uart2Buff();
				memset(databuff, 0, 100);
				data_lenth = 0;
				i = 0;
			    FrameID = 0;				
			break;
			default:
				s_HeadFlag = FALSE;		
				Clear_Uart2Buff();
				memset(databuff, 0, 100);
				data_lenth = 0;
				i = 0;
			    FrameID = 0;	 
			break;	
		}
	}	
}

