#ifndef		_WIFI_H_
#define		_WIFI_H_

#define 	WIFI_AP_ADDR		(STM32_FLASH_BASE + 60*1024L) 

#define		WIFI_ENABLE_TYPE		GPIOA		//δʹ��
#define		WIFI_ENABLE_PIN			GPIO_Pin_4

#define		WIFI_PWR_ON				GPIO_SetBits(WIFI_ENABLE_TYPE, WIFI_ENABLE_PIN)
#define		WIFI_PWR_OFF			GPIO_ResetBits(WIFI_ENABLE_TYPE, WIFI_ENABLE_PIN)


void WiFiPortInit(void);

void WiFiProcess(unsigned short nMain10ms);

void WiFi_Send_Alive(void);

unsigned int WiFi_Report_Data(void);

void OnWiFiComm(unsigned char CommData);

unsigned char Set_WiFi_AP(unsigned char *WiFiPassword, unsigned char lenth);





#endif












