#include "main.h"
#include "SIM800C.h"

#define COM_DATA_SIZE	256

static void SIMObser(unsigned short nMain10ms);

static u8 m_CommBuff[COM_DATA_SIZE];

static u16 m_CirWPinter = 0;

static u16 m_CirRPinter = 0;

static u8 s_HeartPackFlag = FALSE;

static u8 s_DataPackFlag = FALSE;

static u8 s_StartInitFlag = TRUE;

static u8 s_SleepFlag = FALSE, s_RebootCount = 0;

static unsigned char g_ucSim800cReadyFlag = 0;	//模块连接状态
static unsigned char g_ucSim800cRstFlag = 0;		//模块重启标识
static unsigned char g_ucSim800cRelinkFlag = 0;	//重新连接标识

unsigned char GetSim800cResetFlag( void )
{
	return g_ucSim800cRstFlag;
}

void SetSim800cResetFlag( unsigned char ucData )
{
	g_ucSim800cRstFlag = ucData;
	
}

unsigned char GetSim800cRelinkFlag( void )
{
	return g_ucSim800cRelinkFlag;
}

void SetSim800cRelinkFlag( unsigned char ucData )
{
	g_ucSim800cRelinkFlag = ucData;
	
}

unsigned char GetSim800cReadyFlag( void )
{
	return g_ucSim800cReadyFlag;
}

void SetSim800cReadyFlag( unsigned char ucFlag )
{
	g_ucSim800cReadyFlag = ucFlag;
}

static void Clear_COMBuff(void)
{
	u16 i;
	
	for(i=0; i<COM_DATA_SIZE; i++)
	{
		m_CommBuff[i] = 0;		
	}
		m_CirWPinter = 0;

		m_CirRPinter = 0;	
}

void OnSIMComm(u8 CommData)
{
	m_CommBuff[m_CirWPinter++] = CommData;
	
	if(m_CirWPinter >= COM_DATA_SIZE)
	{
		m_CirWPinter = 0;
	}
	if(m_CirWPinter == m_CirRPinter)
	{
		m_CirRPinter++;
		if(m_CirRPinter	>= COM_DATA_SIZE)
		{
		    m_CirRPinter = 0; 
		}
	}	
}

void SIM800CPortInit(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA , ENABLE);	 

	GPIO_InitStructure.GPIO_Pin = SIM800C_ENABLE_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(SIM800C_ENABLE_TYPE, &GPIO_InitStructure);	
			
	GPIO_InitStructure.GPIO_Pin = LORA_M0_PIN | LORA_M1_PIN;//LORA M0 M1
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(LORA_GPIO_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = LORA_PWR_PIN;   //LORA  PWR
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(LORA_PWR_TYPE, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = LORA_TX_PIN | LORA_RX_PIN; //LORA TX RX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(LORA_UART_TYPE, &GPIO_InitStructure);	
}



unsigned char SIM800C_Init( unsigned short nMain10ms )
{
	static unsigned char s_ucState = 1;
	static uint16 s_nLast=0;
	static unsigned char s_ucErrCount = 0;

	switch( s_ucState )
	{
	 case 1:   //模块断电
		SIM800CPortInit();
		SIM_PWR_OFF;
		USART2_Config(SIM_BAND_RATE);
		s_nLast = nMain10ms;
		s_ucState++;
		printf("[SIM800C]模块断电，延时0.5S.\r\n");
	 	break;
	 
	 case 2:	//断电后延时5S上电，模块上电自动开机
	 	if( CalculateTime(nMain10ms,s_nLast) >= 50 )
		{
			printf("[SIM800C]模块上电，延时8S.\r\n");
			SIM_PWR_ON;
			s_nLast = nMain10ms;
			s_ucState++;
		}
	 	break;
	 case 3: 	//上电完成后，初始化端口
	 	if( CalculateTime(nMain10ms,s_nLast) >= 800 )
		{
			printf("[SIM800C]模块上电完成，检测模块波特率.\r\n");
			
			s_nLast = nMain10ms;
			s_ucState++;
		}
	 	break;
	 case 4:  	//检测模块的波特率
		Uart_Send_Data(SIM_COM, (unsigned char *)"AT\r\n", strlen("AT\r\n"));		//发送"AT\r"，查询模块是否连接，返回"OK"
		printf("[SIM800C][SEND]:AT\r\n");
		s_nLast = nMain10ms;
		s_ucState++;
	 	break;
	 case 5:	//延时100MS,检测响应数据
		if(g_Uart2RxFlag == TRUE)
		{
			g_Uart2RxFlag = FALSE;
			
			if(strstr((char *)g_USART2_RX_BUF, "OK"))
			{
				s_ucErrCount = 0;
				s_ucState++;	
				printf("SIM800C OK\r\n");
			}			
			Clear_Uart2Buff();
			
		}
		else if( CalculateTime(nMain10ms,s_nLast) >= 30 )
		{				
				printf("[SIM800C]模块超时无响应\r\n");
				s_ucState--;   //回上一个状态
				s_ucErrCount++;   //重试次数加1
				if(s_ucErrCount >= 3 )
				{
					s_ucErrCount = 0;
					s_ucState = 1;	//模块重新上电初始化
				}						
		}
	 	break;
	 case 6:	//检测到模块，模块响应正常
		 
		s_ucState = 1;
		return 1;

		 //break;
	 default:
		 s_ucState = 1;
		 break;
	}
	return 0;
}




//初始化SIM800C模块,包括上电,硬件是否正常自检,同步寄存器配置等
uint32 InitSim800c( uint16 nMain10ms )
{
	static unsigned char s_ucState = 1;
	static unsigned char s_ucStep = 1;
	static uint16 s_nLast;
	static unsigned char s_ucErrCount = 0;
	static unsigned char s_ucAPNFlg = 0;
	unsigned char ucRecv[100];
	char ucBuf[10];
//	uint16 nRecvLen = 0;
	uint16 nTemp = 0;
	char str[100];
	u8 i;
	char * p1 = NULL;

	SYSTEMCONFIG *sys_cfg;
	
	sys_cfg = GetSystemConfig();		//读取系统参数
		
	memset(ucRecv,'\0',sizeof(ucRecv));

	if(GetSim800cResetFlag() == 1)	//重启模块
	{
		printf("[SIM800C]复位\r\n");
		SetSim800cResetFlag(0);
		SetSim800cReadyFlag(0);
		SetOnline( 0 );
		s_ucState = 1;
		s_ucStep = 1;
		return 0;
	}
		
	switch( s_ucState )
	{
	 case 1:   //模块断电	 
		if(s_RebootCount >= 2)
		{
			printf(" 连续2次连接GPRS网络失败，9分钟后再试...\r\n");
			s_SleepFlag = TRUE;
			return 0;	
		}
		SetSim800cReadyFlag(0);
		if( SIM800C_Init( nMain10ms) == 1 )	//硬件初始化，检测SIM800C模块
		{
			s_ucState++;
			s_ucErrCount = 0;
		}
		s_nLast = nMain10ms;
	 	break;
		
	 case 2: 	//查询模块信息及SIM卡	
	 	switch( s_ucStep )
		{
			//查询 GPRS 参数
			case 1: printf("[SIM800C]关闭回显\r\n");
					sprintf(str,"ATE0\r"); 
					break;
			case 2: printf("[SIM800C]查询制造商名称\r\n");
					sprintf(str,"AT+CGMI\r"); 
					break;
			case 3:	printf("[SIM800C]查询模块名称\r\n");
					sprintf(str,"AT+CGMM\r");
					break;
			case 4: printf("[SIM800C]查询模块序列号\r\n");
					sprintf(str,"AT+CGSN\r");
					break;
			case 5: printf("[SIM800C]设置模块附着GPRS状态\r\n");
					sprintf(str,"AT+CGATT=1\r"); 
					s_ucStep++;
					s_nLast = nMain10ms;
					s_ucErrCount = 0;
					return 0;
//					break;
				
			//查询与卡相关
			case 6: printf("[SIM800C]查询模块SIM状态\r\n");
					sprintf(str,"AT+CPIN?\r"); 
					break;
			case 7: printf("[SIM800C]查询模块信号质量\r\n");
					sprintf(str,"AT+CSQ\r"); 
					break;
			case 8: printf("[SIM800C]查询模块网络注册状态\r\n");
					sprintf(str,"AT+CREG?\r"); 
					break;
			case 9: printf("[SIM800C]查询网络运营商\r\n");
					sprintf(str,"AT+COPS?\r"); 
					break;
			case 10: printf("[SIM800C]查询模块GPRS附着状态\r\n");
					sprintf(str,"AT+CGATT?\r"); 
					break;
			default:
				s_ucStep = 1;
				return 0;
				//break;
		}
		s_nLast = nMain10ms;
		Uart_Send_Str(SIM_COM, str);
		Uart_Send_Str(USART1, str);
		s_ucState++;	
	 	break;
	 case 3: 	//检测响应数据
		if(g_Uart2RxFlag == TRUE)
		{
			g_Uart2RxFlag = FALSE;
			
			for(i=0; i<g_USART2_RX_CNT; i++)
				ucRecv[i] = g_USART2_RX_BUF[i];
			
			Clear_Uart2Buff();
			
			switch( s_ucStep )
			{
			case 1:	//ATE0	关闭回显示
				sprintf(str,"%s\r\n",ucRecv);
				Uart_Send_Str(USART1, str);
				if( (p1=strstr((const char *)ucRecv,"OK")) != NULL )
				{
					s_ucState--;
					s_ucStep++;
					s_ucErrCount = 0;
				}
				else
				{
					s_ucState--;	//重新发送关闭回显命令
					s_ucErrCount++;
					if(s_ucErrCount > 5 )
					{
						s_ucErrCount = 0;
						s_ucState = 1;
						s_ucStep = 1;
						s_RebootCount++;
					}
				}
					
				break;
			
			case 2:	//AT+CGMI	制造商名称
				sprintf(str,"[SIM800C]制造商名称=%s\r\n",ucRecv);
				Uart_Send_Str(USART1, str);
				if( (p1=strstr((const char *)ucRecv,"OK")) != NULL )
				{
					s_ucState--;
					s_ucStep++;
					s_ucErrCount = 0;
				}
				else
				{
					s_ucState--;	//重新发送获取制造商名称
					s_ucErrCount++;
					if(s_ucErrCount > 5 )
					{
						s_ucErrCount = 0;
						s_ucState = 1;
						s_ucStep = 1;
						s_RebootCount++;
					}
				}
				break;
			
			case 3:	//AT+CGMM	模块名称
				sprintf(str,"[SIM800C]模块名称=%s\r\n",ucRecv);
				Uart_Send_Str(USART1, str);
				if( (p1=strstr((const char *)ucRecv,"OK")) != NULL )
				{
					s_ucState--;
					s_ucStep++;
					s_ucErrCount = 0;
				}
				else
				{
					s_ucState--;	//重新发送获取模块名称命令
					s_ucErrCount++;
					if(s_ucErrCount > 5 )
					{
						s_ucErrCount = 0;
						s_ucState = 1;
						s_ucStep = 1;
					}
				}
				break;
			
			case 4:	 //AT+CGSN	序列号
				sprintf(str,"[SIM800C]模块序列号=%s\r\n",ucRecv);
				Uart_Send_Str(USART1, str);
				if( (p1=strstr((const char *)ucRecv,"OK")) != NULL )
				{
					s_ucState--;
					s_ucStep++;
					s_ucErrCount = 0;
				}
				else
				{
					s_ucState--;	//重新发送获取模块序列号命令
					s_ucErrCount++;
					if(s_ucErrCount > 5 )
					{
						s_ucErrCount = 0;
						s_ucState = 1;
						s_ucStep = 1;
						s_RebootCount++;
					}
				}
				break;
				
//			case 5:	//AT+CGATT=1	附着GPRS
//				sprintf(str,"[SIM800C]模块附着GPRS=%s\r\n",ucRecv);
//				Uart_Send_Str(USART1, str);
//				s_ucState--;
//				s_ucStep++;
//				break;
				
			
			//SIM卡状态===================================================
			case 6:	 //AT+CPIN 查询是否有SIM卡
				
				sprintf(str,"[SIM800C]检查模块SIM卡%s\r\n",ucRecv);
				Uart_Send_Str(USART1, str);
				if( (p1=strstr((const char *)ucRecv,"+CPIN: READY")) != NULL )
				{
					s_ucState--;
					s_ucStep++;
					s_ucErrCount = 0;
				}
				else
				{
					s_ucState--;	//重新发送检查SIM卡命令
					s_ucErrCount++;
					if(s_ucErrCount > 10 )
					{
						s_ucErrCount = 0;
						s_ucState = 1;
						s_ucStep = 1;
						s_RebootCount++;
					}
					sprintf(str,"[SIM800C]检查模块SIM卡错误次数 %d\r\n",s_ucErrCount);
					Uart_Send_Str(USART1, str);
				}
				break;
			
			case 7:	//AT+CSQ	查询信号质量,+CSQ: 20,0	注意，返回数据中+CSQ：后面有一个空格
				if( (p1=strstr((const char *)ucRecv,"+CSQ:")) != NULL )
				{
					strncpy(ucBuf,p1+6,2);
					nTemp = atoi( ucBuf);
					if( nTemp < 10 )
					{
						
						sprintf(str,"[SIM800C]模块信号质量小于10，检测次数=%d,重新检测信号质量。%s\r\n",s_ucErrCount,ucRecv);
						Uart_Send_Str(USART1, str);
						s_ucState--;	//重新发送命令
						
						s_ucErrCount++;
						if(s_ucErrCount > 12 )	//超过次数检测到的信号还是弱，重启模块
						{
							sprintf(str,"[SIM800C]模块信号质量弱，重新启动模块。%s\r\n",ucRecv);
							Uart_Send_Str(USART1, str);
							s_ucErrCount = 0;
							s_ucState = 1;
							s_ucStep = 1;
							s_RebootCount++;
							return 0;
						}
					}
					else	//信号正常，下一步
					{
						sprintf(str,"[SIM800C]模块信号质量=%s\r\n",ucRecv);
						Uart_Send_Str(USART1, str);
						s_ucStep++;
						s_ucState--;
						s_ucErrCount = 0;
					}
					
				}
				else	//接收到不是检测信号质量的其他返回值，则重新检测信号质量，连续5次收到格式错误数据，则重启模块
				{
					s_ucState--;	//重新发送检查信号强度
					sprintf(str,"[SIM800C]检查信号质量返回数据错误，重新检查信号质量，%s\r\n",ucRecv);
					Uart_Send_Str(USART1, str);
					s_ucErrCount++;
					if( s_ucErrCount > 5 )
					{
						s_ucErrCount = 0;
						s_ucState = 1;
						s_ucStep = 1;
						s_RebootCount++;
					}
				}
				break;
			
			case 8:	//AT+CREG?	查询模块是否注册网络,+CREG: 0,1
				sprintf(str,"[SIM800C]模块注册网络=%s\r\n",ucRecv);
				Uart_Send_Str(USART1, str);
				if( (p1=strstr((const char *)ucRecv,"+CREG:")) != NULL )
				{
					s_ucErrCount = 0;
					nTemp = atoi( (const char *) (p1+9));
					switch( nTemp  )
					{
						case 0:
							printf("[SIM800C]模块注册--未注册\r\n");
							s_ucState--;
							//s_ucStep = 6;	//重新检查SIM卡和网络信号
							break;
						case 1:
							printf("[SIM800C]模块注册网络--成功\r\n");
							s_ucState--;
							s_ucStep++;
							s_ucErrCount = 0;
							break;
						case 2:
							sprintf(str,"[SIM800C]模块注册--正在注册，查询次数=%d\r\n",s_ucErrCount);
							Uart_Send_Str(USART1, str);
							s_ucState--;
							s_ucErrCount++;
							if( s_ucErrCount > 60 )	//60次超时约30秒
							{
								s_ucErrCount = 0;
								s_ucState = 1;
								s_ucStep = 1;
								s_RebootCount++;
									
							}
							//s_ucStep = 6;	//重新检查SIM卡和网络信号
							break;
						case 3:
							printf("\r\n [SIM800C]模块注册--注册被拒, 次数=%d\r\n",s_ucErrCount);
							s_ucState--;
							s_ucErrCount++;
							if( s_ucErrCount > 20 )	//60次超时约30秒
							{
								s_ucErrCount = 0;
								s_ucState = 1;
								s_ucStep = 1;
								s_RebootCount++;
									
							}
							break;
						default:
							printf("[SIM800C]模块注册--注册状态未识别\r\n");
							s_ucState--;
							s_ucStep = 6;	//重新检查SIM卡和网络信号
							break;										
					}
				}
				else		//响应数据格式错误，重新发送检测命令，连续5次错误则重启模块
				{
					printf("[SIM800C]响应数据不符合格式，重新检测注册状态\r\n");
					s_ucState--;
					//s_ucStep--;	//重新检查网络信号
					s_ucErrCount++;
					if( s_ucErrCount > 5 )
					{
						s_ucErrCount = 0;
						s_ucState = 1;
						s_ucStep = 1;
						s_RebootCount++;
					}
				}
				break;
				
			case 9:	//AT+COPS	运营商名称
				//nTemp = atoi( (const char *)ucRecv );
				sprintf(str,"[SIM800C]模块网络运营商名称=%s\r\n",ucRecv);
				Uart_Send_Str(USART1, str);
				if( (p1=strstr((const char *)ucRecv,"+COPS:")) != NULL )
				{
					if( (p1=strstr((const char *)ucRecv,"UNICOM")) != NULL )  s_ucAPNFlg = 1;	//联通
					else s_ucAPNFlg = 0;					//移动或其它
					s_ucState--;
					s_ucStep++;
					s_ucErrCount = 0;
				}
				else	//响应数据格式错误，重新发送检测命令，连续5次错误则重启模块
				{
					s_ucState--;	//重新查询
					s_ucErrCount++;
					if(s_ucErrCount > 5 )
					{
						s_ucErrCount = 0;
						s_ucState = 1;
						s_ucStep = 1;
						s_RebootCount++;
					}
				}	
				break;
			
			case 10: //AT+CGATT?	模块是否GPRS +CGATT: 1
				
				sprintf(str,"[SIM900A]模块GPRS附着状态=%s\r\n",ucRecv);
				Uart_Send_Str(USART1, str);
				if( (p1=strstr((const char *)ucRecv,"+CGATT:")) != NULL )
				{
					
					nTemp = atoi( (const char *) (p1+8));
					if( nTemp == 0)		//没有附着GPRS
					{
						s_ucState--;
						//s_ucStep = 5;
						s_ucErrCount++;
						if( s_ucErrCount > 250 )
						{
							s_ucErrCount = 0;
							s_ucState = 1;
							s_ucStep = 1;	
							s_RebootCount++;
						}
					}
					else	//已经附着GPRS网络
					{
						s_ucErrCount = 0;
						s_ucState++;	//完成后，转入下一步
						s_ucStep=1;						
					}
				}
				else	//响应数据格式错误，重新发送检测命令，连续5次错误则重启模块
				{
					s_ucState--;	//重新查询GPRS附着状态
					//s_ucStep--;	
					s_ucErrCount++;
					if( s_ucErrCount > 5 )
					{
						s_ucErrCount = 0;
						s_ucState = 1;
						s_ucStep = 1;
						s_RebootCount++;
					}
				}
				break;

			default:	//程序异常，直接重启模块
				s_nLast = nMain10ms; 
				s_ucState = 1;
				s_ucStep = 1;
				s_RebootCount++;
				break;
			}
		}
		
		else	//超时500MS无响应，重新发送命令，5次命令无响应重新初始化模块
		{	
			if( CalculateTime(nMain10ms,s_nLast) >= 100 )
			{
				printf("[SIM800C]模块超时500MS无响应数据\r\n");
				s_ucErrCount++;
				s_nLast = nMain10ms;
				if( s_ucErrCount >= 5 )
				{
					s_ucErrCount = 0;
					s_ucState = 1;
					s_ucStep = 1;
					s_RebootCount++;
					return 0;				
				}
				s_ucState--;	//重新发送命令
			}
		}
	 	break;
		
	case 4:	//初始化网络连接
		switch( s_ucStep )
		{
		//GPRS 参数
		case 1: 
			printf("[SIM800C]关闭当前链接\r\n");
			sprintf(str,"AT+CIPCLOSE=1\r"); 
			break;
		case 2: 
			printf("[SIM800C]关闭所有链接\r\n");
			sprintf(str,"AT+CIPSHUT\r"); 
			break;
		case 3: 
			printf("[SIM800C]设置为透传模式\r\n");
			sprintf(str,"AT+CIPMODE=1\r"); 
			break;	//设置为透传模式
		case 4:
			printf("[SIM800C]开始任务，建立APN\r\n");	
			if(	s_ucAPNFlg ) sprintf(str,"AT+CSTT=\"uninet\"\r");
			else 			 sprintf(str,"AT+CSTT=\"cmnet\"\r");
			break;	//APN,移动：cmnet,联通：uninet
		case 5: 
			printf("[SIM800C]建立无线链路GPRS\r\n");
			sprintf(str,"AT+CIICR\r");
			break;	// 激活移动场景
		case 6: 
			printf("[SIM800C]获得本地IP地址\r\n");
			sprintf(str,"AT+CIFSR\r");
			break;	//获取本地IP
		//case 7: sprintf(str,"AT+CIPHEAD=1\r");break;
		default:
			s_nLast = nMain10ms; 
			s_ucState = 1;
			s_ucStep = 1;
			break;
		}
		//UartSendStr(SIM800C_UART,str);
		Uart_Send_Str(SIM_COM, str);
		Uart_Send_Str(USART1, str);	//串口输出AT命令
		printf("\n");
		s_nLast = nMain10ms;
		s_ucState++;	
		break;
		
	case 5:	//初始化网络连接,响应检测
		if(g_Uart2RxFlag == TRUE)
		{
			g_Uart2RxFlag = FALSE;
			
			for(i=0; i<g_USART2_RX_CNT; i++)
				ucRecv[i] = g_USART2_RX_BUF[i];
			
			Clear_Uart2Buff();
			
			switch(s_ucStep)
			{
			//GPRS设置=====================================================
			case 1: //AT+CIPCLOSE	关闭TCP连接
				sprintf(str,"[SIM800C]模块关闭TCP连接=%s\r\n",ucRecv);
				Uart_Send_Str(USART1, str);
			
				if( (p1=strstr((const char *)ucRecv,"+CIPCLOSE")) != NULL )
				{
					s_ucState--;
					s_ucStep++;	
					s_ucErrCount = 0;
				}			
				else if( (p1=strstr((const char *)ucRecv,"ERROR")) != NULL )//刚上电还没有建立过连接，这个地方会返回ERROR
				{
					s_ucState--;
					s_ucStep++;	
					s_ucErrCount = 0;
				}
				else
				{
					s_ucState--;	//重新发送命令
					s_ucErrCount++;
					if( s_ucErrCount > 5 )
					{
						s_ucErrCount = 0;
						s_ucState = 1;
						s_ucStep = 1;
						s_RebootCount++;
					}	
				}		
				break;
				
			case 2:  //AT+CIPSHUT	关闭移动场景
				sprintf(str,"[SIM800C]模块关闭移动场景=%s\r\n",ucRecv);
				Uart_Send_Str(USART1, str);
				if( (p1=strstr((const char *)ucRecv,"SHUT OK")) != NULL )
				{
					s_ucState--;
					s_ucStep++;	
					s_ucErrCount = 0;
				}
				else
				{
					s_ucState--;	//重新发送命令
					s_ucErrCount++;
					if( s_ucErrCount > 5 )
					{
						s_ucErrCount = 0;
						s_ucState = 1;
						s_ucStep = 1;
						s_RebootCount++;
					}	
				}			
				break;
				
			case 3:	//AT+CIPMODE = 1 //设置为透传模式
				sprintf(str,"[SIM800C]设置为透传模式=%s\r\n",ucRecv);
				Uart_Send_Str(USART1, str);
//				if( (p1=strstr((const char *)ucRecv,"+CIPMODE")) != NULL 
				if( (p1=strstr((const char *)ucRecv,"OK")) != NULL )
				{
					s_ucState--;
					s_ucStep++;	
					s_ucErrCount = 0;
				}
				else
				{
					s_ucState--;	//重新发送命令
					s_ucErrCount++;
					if( s_ucErrCount > 5 )
					{
						s_ucErrCount = 0;
						s_ucState = 1;
						s_ucStep = 1;
						s_RebootCount++;
					}	
				}
				break;
			
			case 4:	//AT+CSTT	设置APN
				sprintf(str,"[SIM800C]模块APN=%s\r\n",ucRecv);
				Uart_Send_Str(USART1, str);
				if( (p1=strstr((const char *)ucRecv,"OK")) != NULL )
				{
					s_ucState--;
					s_ucStep++;	
					s_ucErrCount = 0;
				}
				else
				{
					s_ucState--;	//重新发送命令
					s_ucErrCount++;
					if( s_ucErrCount > 5 )
					{
						s_ucErrCount = 0;
						s_ucState = 1;
						s_ucStep = 1;
						s_RebootCount++;
					}	
				}
				break;
			case 5:	//AT+CIICR	激活移动场景
				sprintf(str,"[SIM800C]模块激活移动场景=%s\r\n",ucRecv);
				Uart_Send_Str(USART1, str);
				if( (p1=strstr((const char *)ucRecv,"OK")) != NULL )
				{
					s_ucState--;
					s_ucStep++;	
					s_ucErrCount = 0;
				}
				else
				{
					s_ucState--;	//重新发送命令
					s_ucErrCount++;
					if( s_ucErrCount > 5 )
					{
						s_ucErrCount = 0;
						s_ucState = 1;
						s_ucStep = 1;
						s_RebootCount++;
					}	
				}
				break;
			case 6:	//AT+CISFR	本地IP
				sprintf(str,"[SIM800C]模块本地IP=%s\r\n",ucRecv);
				Uart_Send_Str(USART1, str);
			if( (p1=strstr((const char *)ucRecv,".")) != NULL )
				{
					s_ucState++;	//下一步
					s_ucStep=1;	
					s_ucErrCount = 0;
				}
				else
				{
					s_ucState--;	//重新发送命令
					s_ucErrCount++;
					if( s_ucErrCount > 5 )
					{
						s_ucErrCount = 0;
						s_ucState = 1;
						s_ucStep = 1;
						s_RebootCount++;
					}	
				}
				s_nLast = nMain10ms;
				break;
			case 7:	//AT+CIPHEAD	收到数据包含IP头
				sprintf(str,"[SIM800C]模块收到数据包含IP头=%s\r\n",ucRecv);
				Uart_Send_Str(USART1, str);
				s_ucState++;		//初始化完成
				s_ucStep = 1;
				break;
			default:
				s_ucState = 1;
				s_ucStep = 1;
				break;	
			}
		}
		else	//超时未收到数据处理方式
		{
			if( CalculateTime(nMain10ms,s_nLast) > 500 )
			{
				s_ucState--;	//重新发送命令
				printf("[SIM800C]模块超时无响应数据\r\n");
				s_ucErrCount++;
				if( s_ucErrCount >= 5 )
				{
					s_ucErrCount = 0;
					s_ucState = 1;
					s_ucStep = 1;
					s_RebootCount++;
					return 0;
				}
				
			}			
		}
		break;

//	 case 51://获取IP后等待一段时间，防止在拨号过程中收到SMS Ready或Call Ready的信息
//		
//		if( CalculateTime(nMain100ms,s_nLast) < 5 )  //500MS
//		{
//			return 0;
//		}
//		s_nLast = nMain100ms;
//		
//		if( (sim800c_recv_data( ucRecv )) > 0 )
//		{
//			sprintf(str,"[SIM800C]模块收到数据=%s\r\n",ucRecv);
//			Uart_Send_Str(USART1, str);
//			
//		}
//		s_ucErrCount++;
//		
//		if( s_ucErrCount < 30 )	//15S
//		{
//			return 0;
//		}
//		
//		s_ucErrCount = 0;
//		
//		s_ucState=6;
//		s_ucStep=1;
//		s_nLast = nMain100ms;
//		break;
		
	 case 6:	//发起TCP连接 AT+CIPSTART="TCP","42.159.233.88",6070	建立TCP连接
		printf("[SIM800C]建立TCP链接\r\n");
		sprintf(str,"AT+CIPSTART=\"TCP\",\"%d.%d.%d.%d\",%d\r"
		,sys_cfg->Gprs_ServerIP[0]
		,sys_cfg->Gprs_ServerIP[1]
		,sys_cfg->Gprs_ServerIP[2]
		,sys_cfg->Gprs_ServerIP[3]
		,sys_cfg->Gprs_Port	);

	//	UartSendStr(SIM800C_UART,str);
		Uart_Send_Str(SIM_COM, str);	
		Uart_Send_Str(USART1, str);	//串口输出AT命令
		printf("\n");
		s_nLast = nMain10ms;
		s_ucState++;		
	 	break;
		
	case 7:		//TCP连接响应处理
		if(g_Uart2RxFlag == TRUE)
		{
			g_Uart2RxFlag = FALSE;
			
			for(i=0; i<g_USART2_RX_CNT; i++)
				ucRecv[i] = g_USART2_RX_BUF[i];
			
	
			
			sprintf(str,"[SIM800C]模块建立GPRS连接响应=%s\r\n",ucRecv);
			Uart_Send_Str(USART1, str);

			if( (p1 = strstr((const char *)ucRecv,"ALREADY CONNECT")) != NULL)
			{
				s_ucErrCount = 0;
				printf("[SIM800C]模块已建立GPRS连接\r\n");
				s_ucErrCount=0;
				s_ucState++;
				s_ucStep=1;
				SetSim800cReadyFlag(1);		//连接成功========================
				SetSim800cRelinkFlag(0);
			}
			
			else if( (p1 = strstr((const char *)ucRecv,"CONNECT OK")) != NULL)
			{
				s_RebootCount = 0;
				s_ucErrCount = 0;
				printf("[SIM800C]模块建立GPRS连接成功\r\n");
				s_ucErrCount=0;
				s_ucState++;
				s_ucStep=1;
				SetSim800cReadyFlag(1);		//连接成功========================
				SetSim800cRelinkFlag(0);
			}
			
			else if( (p1 = strstr((const char *)ucRecv,"CONNECT FAIL")) != NULL)	
			{
				s_ucErrCount++;
				sprintf( str,"[SIM800C]模块建立GPRS连接失败,重新连接 = %d\r\n",s_ucErrCount);
				Uart_Send_Str(USART1, str);
		
				s_ucState--;		//重新发送建立连接命令
	
				if( s_ucErrCount > 5 )
				{
					s_ucErrCount = 0;
					s_ucState = 1;
					s_ucStep=1;
				}
			}
			else if( (p1 = strstr((const char *)ucRecv,"CONNECT")) != NULL)
			{
				s_ucErrCount = 0;
				printf("[SIM800C]模块建立GPRS连接成功\r\n");
				s_ucErrCount=0;
				s_ucState++;
				s_ucStep=1;
				SetSim800cReadyFlag(1);		//连接成功========================
				SetSim800cRelinkFlag(0);
				return 1;
				
			}			
			else if( (p1 = strstr((const char *)ucRecv,"OK")) != NULL)
			{
			//	s_ucErrCount=0;	//在当前步骤等待CONNECT======================	
				s_nLast = nMain10ms;
				printf("TCP OK\r\n");
			}					
			else if( (p1 = strstr((const char *)ucRecv,"ERROR")) != NULL)
			{
				s_ucState--;
				s_ucErrCount++;
				if(s_ucErrCount>10)
				{
					s_ucErrCount=0;
					s_ucState=1;	//重启模块
					s_ucStep=1;
					s_RebootCount++;
				}
			}	
			else
			{
				s_ucState--;	//重新发送建立连接命令
				
				printf("[SIM800C]建立GPRS连接的响应状态未识别，重新建立连接\r\n");
				s_ucErrCount++;
				if( s_ucErrCount > 5 )
				{
					s_ucErrCount=0;
					s_ucState=1;	//重启模块
					s_ucStep=1;
					s_RebootCount++;
				}					
			}
			
			Clear_Uart2Buff();
		}
		else	//命令超时无响应
		{	
			if( CalculateTime(nMain10ms,s_nLast) > 2500 )
			{
				s_ucState--;	//重新发送命令
				s_nLast = nMain10ms;
				
				printf("[SIM800C]模块超时无响应数据\r\n");
				s_ucErrCount++;
				if( s_ucErrCount > 8 )
				{
					printf("[SIM800C]模块连接服务器失败，重新初始化模块\r\n");
					s_ucErrCount = 0;
					s_ucState = 1;
					s_ucStep = 1;
					s_RebootCount++;
					return 0;
				}
//				
//				if( s_ucStep == 1 )
//				{
//					s_ucState--;
//				}	
			}
		}	
		break;
		
	case 8:	//初始化完成，可以进行发送数据和接收数据处理
		
//		printf("[SIM800C]模块在状态8。。。\r\n");
		//需要重启模块
		if(GetSim800cResetFlag() == 1)	//重启模块
		{
			printf("[SIM800C]重启模块\r\n");
			SetSim800cResetFlag(0);
			SetSim800cReadyFlag(0);
			SetOnline( 0 );
			s_ucState = 1;
			s_ucStep = 1;
			return 0;
		}
		
		if( GetSim800cRelinkFlag() == 1)	//重新连接
		{
			printf("[SIM800C]重新连接网络。。。\r\n");
			g_ucSim800cRelinkFlag = 0;
			g_ucSim800cReadyFlag = 0;
			SetOnline( 0 );
			s_ucState = 6;
			s_ucStep = 1;
			return 0;
		}
		//模块已经连接网络
		if( GetSim800cReadyFlag() == 1)
		{
			SetOnline( 1 );
			s_RebootCount = 0;
//			printf("[SIM800C]模块已经连接。。。\r\n");
			return 1;
		}
		else
		{
			printf("[SIM800C]模块未连接网络，重新启动模块\r\n");
			SetSim800cResetFlag(0);
			SetSim800cReadyFlag(0);
			SetOnline( 0 );
			s_ucState = 1;
			s_ucStep = 1;
		}
		break;

	 default:
	 	s_ucState = 1;
		s_ucStep = 1;
		s_nLast = nMain10ms;
		break;
	} 
	return 0;
}

void SIM800CProcess(unsigned short nMain10ms)
{
	static u16 s_nLast = 0, s_LastTime = 0, s_OverCount = 0;
	static unsigned int s_RTCSumSecond = 0, s_RTCTime = 0;
	static u8 s_State = 1, err, RunOnce = FALSE, s_State_err = 1;
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	p_sys = GetSystemConfig();
	
	if( (CalculateTime(nMain10ms,s_nLast) >= 20) && s_StartInitFlag == TRUE)
	{
		s_nLast = nMain10ms;
		err = InitSim800c(nMain10ms);
	}
	
	if(err == 0)
	{
		if(RunOnce == FALSE)
		{
			RunOnce = TRUE;
			s_RTCTime = GetRTCSecond();
			s_LastTime = nMain10ms;
		}
		
		switch(s_State_err)
		{
			case 1:

				s_State_err++;
			break;
			
			case 2:
				if(GetSim800cReadyFlag() == 0)
				{
					if(s_SleepFlag)
					{					
						s_State_err ++;
						s_RTCTime = GetRTCSecond();
						s_StartInitFlag = FALSE;
					}
				}
			break;
				
			case 3:
				if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCTime) >= p_sys->Heart_interval)
				{					
					s_SleepFlag = FALSE;
					s_RTCTime = GetRTCSecond();
					s_RebootCount = 0;
					printf(" \r\n 断线重连\r\n");
					Clear_Flag();
					s_State_err++;
					s_StartInitFlag = TRUE;
					SetSim800cResetFlag(1);
					SetSim800cReadyFlag(0);
				}
				else
				{
//					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
//					printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
					EnterStopMode_SIM(24);			
				}
			break;
				
			case 4:
				s_State_err = 1;
			break;
		}
	}
	
	SIMObser(nMain10ms);
	
	if(err)
	{
		switch(s_State)
		{
			case 1:
				if(GetSim800cReadyFlag())
				{
					if(g_PowerDetectFlag)
					{
						Mod_Send_Alive(SIM_COM);
						s_State++;
						s_LastTime = nMain10ms;
					}
				}			
			break;
			
			case 2:
				if(s_HeartPackFlag && (CalculateTime(nMain10ms,s_LastTime) >= 2))
				{
					Mod_Report_LowPower_Data(SIM_COM);
					s_State++;
					s_LastTime = nMain10ms;
				}
				else
				{
					if( CalculateTime(nMain10ms,s_LastTime) >= 800 )
					{
						s_State--;
						s_LastTime = nMain10ms;
						printf("\r\n 超时未收到心跳回复包  重发\r\n");
						s_OverCount++;
						if(s_OverCount >= 3)
						{
							s_OverCount = 0;
							SetSim800cResetFlag(1);
							SetSim800cReadyFlag(0);
						}
					}
				}
			break;
			
			case 3:
				if(s_DataPackFlag)
				{
					s_State++;
					s_LastTime = nMain10ms;
					s_DataPackFlag = FALSE;
					s_HeartPackFlag = FALSE;
				}
				else
				{
					if( CalculateTime(nMain10ms,s_LastTime) >= 800 )
					{
						s_State--;
						s_LastTime = nMain10ms;
						printf("\r\n超时未收到数据回复包  重发\r\n");
						s_OverCount++;
						if(s_OverCount >= 5)
						{
							s_OverCount = 0;
							SetSim800cResetFlag(1);
							SetSim800cReadyFlag(0);
						}
					}
				}			
			break;
				
			case 4:
				s_RTCSumSecond = GetRTCSecond();
				s_State++;
				s_LastTime = nMain10ms;
			break;
			
			case 5:
				if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= p_sys->Heart_interval)
				{
					s_RTCSumSecond = GetRTCSecond();
					
					printf(" \r\n 重新注册\r\n");
					Clear_Flag();
					s_State++;
				}		
				else
				{
					s_LastTime = nMain10ms;
//					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
//					printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
					EnterStopMode_SIM(24);			
				}
			break;
				
			case  6:
				if(g_PowerDetectFlag)
				{
					s_State = 1;
					SetSim800cResetFlag(1);
					SetSim800cReadyFlag(0);
				}
			break;		
		}
	}
}

void OnRecSimData(CLOUD_HDR *pMsg, u8 *data, u8 lenth)
{
	u8 i;
//	CLOUD_HDR *hdr;
//	static u8 s_RegistPackCount = 0;
	RTC_TimeTypeDef RTC_TimeStructure;	
	RTC_DateTypeDef RTC_DateStruct;
	
//	SYSTEMCONFIG *p_sys;

//	p_sys = GetSystemConfig();
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
	//解析协议包
	printf("\r\n [%02d:%02d:%02d][SIM] <- 协议号：%02d 设备号：%04d 方向：%X 包序号：%04X 包长度：%X 命令字：%02X 数据:", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, pMsg->protocol, pMsg->device_id, pMsg->dir, pMsg->seq_no, pMsg->payload_len, pMsg->cmd);
	for(i=0; i<lenth; i++)
		printf("%02X ", data[i]);
	printf("\r\n");

	if(pMsg->cmd == CMD_HEATBEAT)
	{
		printf("收到服务器心跳包回复,同步服务器时间\r\n");
			
		RTC_TimeStructure.RTC_H12     = RTC_H12_AM;
				
		RTC_TimeStructure.RTC_Hours   = data[2];
		RTC_TimeStructure.RTC_Minutes = data[1];
		RTC_TimeStructure.RTC_Seconds = data[0];

		RTC_DateStruct.RTC_Date  	= data[3];
		RTC_DateStruct.RTC_Month  	= data[4];
		RTC_DateStruct.RTC_Year  	= data[5];	
		RTC_SetDate(RTC_Format_BIN, &RTC_DateStruct);
		
		if(RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure) != ERROR)
		{
			printf("\r\n 同步远程时间成功\r\n");
			RTC_TimeShow();
		}
		else
		{
			printf("\r\n 同步远程时间失败\r\n");
		}
		
		s_HeartPackFlag = TRUE;
	}
	
	if(pMsg->cmd == CMD_REPORT_D)
	{
		printf("收到服务器上报数据回复,上报数据成功\r\n");
		
		s_DataPackFlag = TRUE;
	}
}
	
static void SIMObser(unsigned short nMain10ms)
{
	static u8 FrameID = 0, special_flag = FALSE, s_HeadFlag = FALSE;
	static u16 data_lenth = 0, s_LastTime = 0, i = 0;
	static CLOUD_HDR m_RxFrame;
	static u8 RxData, databuff[100];
	u8 checksum;
	
	if(s_HeadFlag == TRUE)
	{
		if(CalculateTime(nMain10ms, s_LastTime) >= 100)
		{
			s_HeadFlag = FALSE;
			printf("\r\n 接收超时\r\n");
			Clear_Uart2Buff();
			memset(databuff, 0, 100);
			data_lenth = 0;
			i = 0;
			FrameID = 0;
			return ;
		}
	}
		
	if(m_CirRPinter != m_CirWPinter)
	{
		RxData = m_CommBuff[m_CirRPinter++];
		if(m_CirRPinter >= COM_DATA_SIZE)
		{
			m_CirRPinter = 0;
		}
		if(RxData == 0x7d && special_flag == FALSE)
		{
			special_flag = TRUE;
			return;
		}
		if(special_flag == TRUE)
		{
			special_flag = FALSE;
			if(RxData == 0x5d)
			{
				RxData = 0x7d;
			}
			else if(RxData == 0x5e)
			{
				RxData = 0x7e;
			}
			else if(RxData == 0x51)
			{
				RxData = 0x21;
			}
		}



		switch(FrameID)
		{
			case 0:
				if(RxData == 0X7e)
				{
					s_HeadFlag = TRUE;
					s_LastTime = nMain10ms;
					FrameID++;
				}
				break;
			case 1:
				m_RxFrame.protocol = RxData;
				FrameID++;
			break;
			case 2:
				m_RxFrame.protocol = (m_RxFrame.protocol << 8) | RxData;
				FrameID++;
			break;
			case 3:
				m_RxFrame.device_id = RxData;
				FrameID++;
			break;
			case 4:
				m_RxFrame.device_id = (m_RxFrame.device_id << 8) | RxData;
				FrameID++;
			break;
			case 5:
				m_RxFrame.device_id = (m_RxFrame.device_id << 8) | RxData;
				FrameID++;
			break;
			case 6:
				m_RxFrame.device_id = (m_RxFrame.device_id << 8) | RxData;
				FrameID++;
			
			break;
			case 7:
				m_RxFrame.dir = RxData;
				FrameID++;
			break;
			case 8:
				m_RxFrame.seq_no = RxData;
				FrameID++;
			break;
			case 9:
				m_RxFrame.seq_no = (m_RxFrame.seq_no << 8) | RxData;
				FrameID++;
			break;
			case 10:
				m_RxFrame.payload_len = RxData;
				FrameID++;
			break;
			case 11:
				m_RxFrame.payload_len = (m_RxFrame.payload_len << 8) | RxData;
				data_lenth = m_RxFrame.payload_len;
				FrameID++;
			break;
			case 12:
				m_RxFrame.cmd = RxData;				
				i = 0;
			    memset(databuff, 0, 100);
				FrameID++;
				if(data_lenth == 0)
				{
					FrameID = 14;
					break;
				}	
				else if(data_lenth >= 100)
				{
					printf("\r\n too long %d\r\n", data_lenth );
					Clear_Uart2Buff();
					memset(databuff, 0, 100);
					data_lenth = 0;
					i = 0;
					FrameID = 0;
				}
			break;
			case 13:				
				databuff[i++] = RxData;
				if(i >= data_lenth)
				{
					FrameID++;
				}
			break;		
			case 14:
				checksum = RxData;
				if(CheckHdrSum(&m_RxFrame, databuff, data_lenth) != checksum)
				{
					printf("[SIM]Check error...");
					printf("Rec:%02X, CheckSum:%02X\r\n", checksum, CheckHdrSum(&m_RxFrame, databuff, data_lenth));
					FrameID = 0;	
					s_HeadFlag = FALSE;					
					Clear_Uart2Buff();	
					Clear_COMBuff();
					s_HeadFlag = FALSE;		
					memset(databuff, 0, 100);
					data_lenth = 0;
					i = 0;
				}else
				FrameID++;
			break;
			case 15:
				if(RxData == 0x21)	
				{								
					OnRecSimData(&m_RxFrame, databuff, data_lenth);
				}
				else
				{				
					printf("end error!\r\n");
				}
				s_HeadFlag = FALSE;		
				Clear_Uart2Buff();
				memset(databuff, 0, 100);
				data_lenth = 0;
				i = 0;
			    FrameID = 0;				
			break;
			default:
				s_HeadFlag = FALSE;		
				Clear_Uart2Buff();
				memset(databuff, 0, 100);
				data_lenth = 0;
				i = 0;
			    FrameID = 0;	 
			break;	
		}
	}	
}
