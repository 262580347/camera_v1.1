#include "main.h"
#include "App.h"

#define		STATUS_WAITING			0x01	//等待
#define		STATUS_INIT_SENSOR  	0x02	//第一次获取传感器数据
#define		STATUS_CONSOLE_START   	0x03	//控制云台
#define		STATUS_CONSOLE_WAIT   	0x04	//等待云台转至预置点
#define		STATUS_START_GPS   		0x05	//开启GPS数据
#define		STATUS_GET_GPS   		0x06	//获得经纬度数据
#define		STATUS_HI3519START		0x07	//海思工作状态
#define		STATUS_HI3519WAIT		0x08	//等待与海思上电
#define		STATUS_HI3519DETCOM		0x09	//检测与核心板通信状态
#define		STATUS_HI3519RUN		0x0A	//等待与海思进行数据交换
#define		STATUS_HI3519WAITPHOTO  0x0B	//等待拍照上传完成
#define		STATUS_HI3519GOTOPOS	0x0C	//前往下一拍照点
#define		STATUS_HI3519WAITMOTOR	0x0D	//前往下一拍照点
#define		STATUS_HI3519POWEROFF	0x0E	//开始关闭核心板电源
#define		STATUS_MOTORGOTOZERO	0X0F	//电机转回高点

#define		STATUS_NOINTERVALTIMEWAI 0x20	//等待一定时间关闭HI3519

#define		STATUS_SLEEP  			0x10	//休眠
#define		STATUS_RUN 				0x11	//运行
#define		STATUS_GET_SENSOR		0x12	//得到传感器数据
#define		STATUS_DONOTSLEEP		0x13	//相机不休眠模式

#define		ILL_PHOTO_PONIT			5		//开始拍照的光照度

#define		HI3519STARTWAITTIME		47		//等待核心板启动时间

#define		HI3519_PHOTO_WAIT		300		//拍照等待间隔
#define		HI3519WAITTIME			160		//等待核心板联网时间

#define		HI3519DETWAITIME		100		//检测核心板存在等待时间

#define		GPSGETDATAWAITTIME		70		//GPS获取经纬度数据等待时间


static u8 s_TestFlag = FALSE;

static unsigned char TimeIntheInterval(void)
{
	RTC_TimeTypeDef RTC_TimeStructure;
	ANGLESTORE_CFG *p_Config;
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
		
	p_Config = GetPhotoConfig();
				
	if(p_Config != NULL)
	{
		if((RTC_TimeStructure.RTC_Hours*60 + RTC_TimeStructure.RTC_Minutes) >= ((p_Config->StartPhotoTime[0]*60) + p_Config->StartPhotoTime[1]))	//开启拍照时间
		{					
			if((RTC_TimeStructure.RTC_Hours*60 + RTC_TimeStructure.RTC_Minutes) <= ((p_Config->EndPhotoTime[0]*60) + p_Config->EndPhotoTime[1]))	//结束拍照时间
			{
	//			u1_printf(" Photo interval!\r\n");
				return TRUE;
			}
			else
			{
//				u1_printf(" NowTime: %02d:%02d , End Time:%02d:%02d:%02d\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, p_Config->EndPhotoTime[0], p_Config->EndPhotoTime[1], p_Config->EndPhotoTime[2]);
//				u1_printf(" Not between photos interval2\r\n");
				return FALSE;
			}
		}
		else
		{
//			u1_printf(" NowTime: %02d:%02d , Start Time:%02d:%02d:%02d\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, p_Config->StartPhotoTime[0], p_Config->StartPhotoTime[1], p_Config->StartPhotoTime[2]);
//			u1_printf(" Not between photos interval1\r\n");
			return FALSE;
		}
	}
	
	return 0xff;
	
}
//刚开始达到拍照时间，开始拍照
unsigned char TimeToStart(void)
{
	RTC_TimeTypeDef RTC_TimeStructure;
	ANGLESTORE_CFG *p_Config;
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
		
	p_Config = GetPhotoConfig();
				
	if(p_Config != NULL)
	{
		if((RTC_TimeStructure.RTC_Hours == p_Config->StartPhotoTime[0]) && (RTC_TimeStructure.RTC_Minutes == p_Config->StartPhotoTime[1]) && (RTC_TimeStructure.RTC_Seconds <= 11))	//开启拍照时间
		{			
			u1_printf("\r\n 到达开始拍照时间，开始拍照!\r\n");
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	return FALSE;
}

static void TaskForAppRun(void)
{
	static u8 AppStatus = STATUS_WAITING, s_FirstPresetPointFlag = FALSE;
	static u8 s_First = FALSE, s_GPSDate = 0, s_GPSFlag = FALSE, s_GPSGetCount = 0, s_WaitOverTimeCount = 0, s_NowPosition = 0;
	static RTC_TimeTypeDef RTC_TimeStructure;
	static RTC_DateTypeDef RTC_DateStructure;
	static u32 s_RTCSum = 0, s_CameraRTCTime = 0, s_KeepAliveTime = 0, s_SensorInterval = 0;
	static u32 PhotoUseTime = 0, s_PhotoErrCount = 0, s_ErrRetryCount = 0;
	float Latitude = 0, Longitude = 0, f_Ill = 0;
	SYSTEMCONFIG *p_sys;
	ANGLESTORE_CFG *p_Config;
	
	if(!s_First)
	{
		s_First = TRUE;
		
		p_Config = GetPhotoConfig();
		if(p_Config->PhotoInterval <= 5)
		{
			s_SensorInterval = 30;
		}
		else
		{
			s_SensorInterval = 120;
		}
		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
		RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);	
//		s_GPSDate = RTC_DateStructure.RTC_Date;		//上电不开启GPS
		s_GPSDate = 32;					//上电开机GPS
		s_RTCSum = GetRTCSecond();
		s_CameraRTCTime = GetRTCSecond();
		s_KeepAliveTime = GetRTCSecond();
		u1_printf(" Time:20%02d-%02d-%02d  %02d:%02d:%02d\r\n", RTC_DateStructure.RTC_Year, RTC_DateStructure.RTC_Month, RTC_DateStructure.RTC_Date, RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
	}
	
	if(GetUpdataTimeFlag())
	{
		SetUpdataTimeFlag(FALSE);
		s_RTCSum = GetRTCSecond();
		s_CameraRTCTime = GetRTCSecond();
		s_KeepAliveTime = GetRTCSecond();
		return;
	}
			
	switch(AppStatus)
	{
		case STATUS_WAITING:	//等待5s，开启APP				
			AppStatus = STATUS_INIT_SENSOR;
			StartSensorMeasure();
			u1_printf(" Start getting sensor data\r\n");
			s_RTCSum = GetRTCSecond();
		break;
		
		case STATUS_INIT_SENSOR:	//先获取传感器数据
			
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) > 1)
			{
				GetPosition(&Latitude, &Longitude);
	//				Altitude = GetAltitude();
				u1_printf(" Ill:%2.1f,Vol:%d,Lati:%.1f,Logi:%.1f\r\n",\
				Get_Ill_Value(),\
				Get_Battery_Vol(),\
				Latitude,
				Longitude);
				
				RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);
				if(s_GPSDate !=  RTC_DateStructure.RTC_Date)	//不是同一天，开启GPS
				{
					s_GPSDate = RTC_DateStructure.RTC_Date;
					RTC_EasyShowTime();
					u1_printf(" Start getting GPS\r\n");
					while (DMA_GetCurrDataCounter(DMA1_Channel4));
					MotorPowerOff();
					delay_ms(200);
					GPSPowerOn();		//先将GPS电源开启，待云台停止后再获取GPS定位参数
					delay_ms(200);
					s_GPSGetCount = 0;
					s_GPSFlag = TRUE;
				}
				else											//还在同一天，跳过GPS，开启云台
				{
					if(s_GPSGetCount >= 2)
					{
						s_GPSGetCount = 0;
						s_GPSFlag = FALSE;
					}										
				}	

//				s_GPSFlag = FALSE;	//不开启GPS
				
				p_Config = GetPhotoConfig();
				
				if(p_Config != NULL)
				{						
					if(p_Config->PhotoPointNum > 1)
					{
						AppStatus = STATUS_CONSOLE_START;	
					}
					else if(p_Config->PhotoPointNum == 1)
					{
						if(s_FirstPresetPointFlag == FALSE)
						{
							AppStatus = STATUS_CONSOLE_START;	
						}
						else
						{
							while (DMA_GetCurrDataCounter(DMA1_Channel4));
	//							MotorPowerOn_NoRun();
							u1_printf(" No need to turn the Console\r\n");							
							AppStatus = STATUS_CONSOLE_WAIT;
							SetConsolePosition(1);
							delay_ms(100);
							MotorPowerOff();
						}
					}
					else
					{
						while (DMA_GetCurrDataCounter(DMA1_Channel4));
	//						MotorPowerOn_NoRun();
						u1_printf(" No need to turn the Console\r\n");					
						AppStatus = STATUS_CONSOLE_WAIT;	
						SetConsolePosition(1);
						delay_ms(100);
						MotorPowerOff();
					}
				}

				s_RTCSum = GetRTCSecond();
			}
		break;

		case STATUS_CONSOLE_START:		//启动云台
			
			p_Config = GetPhotoConfig();
		
			if(p_Config->PhotoPointNum >= 1)
			{		
				if(GetGPSInitSuccess() || (DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) > 9) || s_GPSFlag == FALSE)
				{
					RTC_EasyShowTime();
					delay_ms(100);
					MotorPowerOn();	
					AppStatus = STATUS_CONSOLE_WAIT;
					s_RTCSum = GetRTCSecond();
					if(s_FirstPresetPointFlag == FALSE)
					{
						s_FirstPresetPointFlag = TRUE;
					}
					
					if(GetGPSInitSuccess())
					{
						GPSCommunicationOff();
					}
				}
			}
			else
			{
				//AppStatus = STATUS_START_GPS;		//不需要转动云台
				AppStatus = STATUS_HI3519START;
				SetConsolePosition(1);
			}
					
		break;
		
		case STATUS_CONSOLE_WAIT:		//等待云台运行至预置点

			s_RTCSum = GetRTCSecond();
			
			if(s_GPSFlag && GetGPSPowerFlag())	//需要获取GPS数据时，转至获取数据状态
			{
				if(GetConsolePowerFlag() == FALSE)
				{
					AppStatus = STATUS_START_GPS;
					//AppStatus = STATUS_HI3519START;
					s_RTCSum = GetRTCSecond();
				}
			}
			else			//不需要获取GPS数据，开始Hi3519通信
			{
				if(GetConsolePowerFlag() == FALSE)
				{	
					AppStatus = STATUS_HI3519START;
					s_RTCSum = GetRTCSecond();
				}
			}				

		break;
			
		case STATUS_START_GPS:		//开启GPS电源，等待经纬度数据
			
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) > 2)
			{
				u1_printf(" GPS Communication Restore\r\n");
				while (DMA_GetCurrDataCounter(DMA1_Channel4));
				s_RTCSum = GetRTCSecond();
				GPSPowerOn();
				s_RTCSum = GetRTCSecond();
				AppStatus = STATUS_GET_GPS;
			}
		
		break;
		
		case STATUS_GET_GPS:	//得到经纬度数据
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) > GPSGETDATAWAITTIME)	//超时关闭GPS
			{
				s_RTCSum = GetRTCSecond();
				AppStatus = STATUS_HI3519START;
				GPSPowerOff();
				SetLogErrCode(LOG_CODE_GPS_FAIL);
				u1_printf(" No latitude and longitude data was obtained for timeout\r\n");
			}
			else if(GetGPSPowerFlag() == FALSE)
			{
				//SetLogErrCode(LOG_CODE_GPS_OK);
				AppStatus = STATUS_HI3519START;
			}
	//		s_RTCSum = GetRTCSecond();
//			AppStatus = STATUS_HI3519START;
		
		break;
							
		case STATUS_HI3519START:		//核心板开始运行
			
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) > 2)	
			{
				SetHi3519LinkFlag(FALSE);
				Hi3519PowerOn();
				RTC_EasyShowTime();
				u1_printf("HI3519 communication initiated\r\n\r\n");
				s_RTCSum = GetRTCSecond();
				
				AppStatus = STATUS_HI3519WAIT;
				
				p_sys = GetSystemConfig();
				if(p_sys->isNoSleepFlag)		//不休眠
				{
					u1_printf(" 进入不休眠模式\r\n");
					
					AppStatus = STATUS_DONOTSLEEP;
					
					s_CameraRTCTime = GetRTCSecond();
										
					TaskForHi3519CommunicationEnd();
				}
			}
		break;
		
		case STATUS_HI3519WAIT:
		
			if(s_GPSFlag)	//需要获取GPS数据，等待GPS数据获取完成
			{
				if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) > GPSGETDATAWAITTIME)	//等待GPS数据超时
				{
					u1_printf(" No latitude and longitude data was obtained for timeout\r\n");
					
					GPSPowerOff();
										
					s_RTCSum = GetRTCSecond();
					
					SetLogErrCode(LOG_CODE_GPS_FAIL);
					
					SendSTM32DetCommunicationbyRetryMode();
					
					AppStatus = STATUS_HI3519DETCOM;
					
					s_GPSGetCount++;
					
//					s_GPSFlag = FALSE;
				}
				else if((GetGPSPowerFlag() == FALSE) && (DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) > 20))	//GPS数据获取完成
				{
					SendSTM32DetCommunicationbyRetryMode();
					
					s_RTCSum = GetRTCSecond();
										
					AppStatus = STATUS_HI3519DETCOM;
					
					s_GPSFlag = FALSE;
					
					s_GPSGetCount = 0;
				}
			}
			else
			{
				if((DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) > HI3519STARTWAITTIME) && (GetConsolePowerFlag() == FALSE)) 	//等待核心板启动时间，且云台电源已关闭
				{
					s_RTCSum = GetRTCSecond();
					
					SendSTM32DetCommunicationbyRetryMode();		//检测与主板之间的通信
									
					AppStatus = STATUS_HI3519DETCOM;
				}
				else if(GetHi3519LinkFlag() == TRUE) 
				{
					AppStatus = STATUS_HI3519DETCOM;
					s_RTCSum = GetRTCSecond();
					u1_printf(" 与Hisi3519已建立通信\r\n");
				}
			}
			
		break;

		case STATUS_HI3519DETCOM:
					
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) > 1 && DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) <= HI3519DETWAITIME)	//与核心板通信正常，开始检测核心板状态
			{
				if(GetHi3519LinkFlag())
				{
//					SendSTM32GetStatusCmdbyRetryMode();
					s_RTCSum = GetRTCSecond();
					AppStatus = STATUS_HI3519RUN;
					s_WaitOverTimeCount = 0;
					//通信正常，发送拍照指令,等待拍照完成，转至下一个预置点
				}
			}
			else if((DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) > HI3519DETWAITIME) && GetConsolePowerFlag() == FALSE)
			{
				RTC_EasyShowTime();
				u1_printf(" No core board detected!\r\n");
				SetLogErrCode(LOG_CODE_NOHI3519);
				s_RTCSum = GetRTCSecond();
				s_CameraRTCTime = GetRTCSecond();
				
				s_WaitOverTimeCount++;
					
				if(s_WaitOverTimeCount > 10)
				{
					u1_printf("\r\n Excessive timeout, restart!!\r\n");
					SetLogErrCode(LOG_CODE_TIMEOUT);
					StoreOperationalData();
					while (DMA_GetCurrDataCounter(DMA1_Channel4));
					Sys_Soft_Reset();
				}
					
				AppStatus = STATUS_HI3519POWEROFF;
			}
			else if(GetTimeToPowerOffHi3519Flag())
			{
				RTC_EasyShowTime();
				SendSTM32GetStatusCmdbyRetryMode();
				s_RTCSum = GetRTCSecond();
				AppStatus = STATUS_HI3519POWEROFF;
				SetTimeToPowerOffHi3519Flag(FALSE);	
			}
			
		break;
		
		case STATUS_HI3519RUN:
						
			p_sys = GetSystemConfig();
			
			if(GetTimeToPowerOffHi3519Flag())	//可以关闭核心板电源
			{
				RTC_EasyShowTime();
				u1_printf(" Core board report data is completed, power can be cut off,3s after power\r\n");
				
				SendSTM32GetStatusCmdbyRetryMode();
				
				SetTimeToPowerOffHi3519Flag(FALSE);	//复位Flag
				
				s_RTCSum = GetRTCSecond();
				
				AppStatus = STATUS_HI3519POWEROFF;
				
				s_WaitOverTimeCount = 0;
			}
			else if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) > HI3519WAITTIME)	//超时未完成工作,立即关闭核心板电源,
			{
				if(GetSynchronizationTimeFlag())
				{
					SetSynchronizationTimeFlag(FALSE);
					s_RTCSum = GetRTCSecond();
				}
				else		//拍照第一张网络不通，休眠
				{
					RTC_EasyShowTime();
					u1_printf(" The core board is not connected to the network over time,Power Off\r\n");
					
					s_RTCSum = GetRTCSecond();
					
					SetLogErrCode(LOG_CODE_PHOTOFAIL);
					
					SendSTM32GetStatusCmdbyRetryMode();
					
					AppStatus = STATUS_HI3519POWEROFF;
					
					s_CameraRTCTime = GetRTCSecond();
			
				}
			}
			else if(GetHi3519StatusFlag() == TRUE)
			{
				s_NowPosition = GetConsolePosition();
				SetHi3519StatusFlag(FALSE);
				if(TimeIntheInterval() == TRUE)	//在拍照间隔
				{
					AppStatus = STATUS_HI3519GOTOPOS;
					
					p_Config = GetPhotoConfig();
				
					if(p_Config != NULL)
					{						
						if(p_Config->PhotoPointNum > 1)
						{
							AppStatus = STATUS_HI3519GOTOPOS;
						}
						else
						{
							AppStatus = STATUS_HI3519POWEROFF;
							
						}
					}
					if(GetPhotoSuccessFlag() == TRUE)
					{
						u1_printf("\r\n 拍照点(%d)完成\r\n\r\n", s_NowPosition);		
						s_RTCSum = GetRTCSecond();
						s_WaitOverTimeCount = 0;
						s_PhotoErrCount = 0;
						s_PhotoErrCount = 0;
					}
					else
					{
						u1_printf("\r\n 拍照点(%d)失败\r\n\r\n", s_NowPosition);
						s_PhotoErrCount++;
					}
				}
				else
				{
					u1_printf(" 相机不在拍照时间间隔内 20s后关闭\r\n");
					s_CameraRTCTime = GetRTCSecond();
					s_RTCSum = GetRTCSecond();
					AppStatus = STATUS_NOINTERVALTIMEWAI;

				}
				
				SetPhotoSuccessFlag(FALSE);
			}

		break;
			
		case STATUS_NOINTERVALTIMEWAI:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) > 12)
			{
				s_CameraRTCTime = GetRTCSecond();
				s_RTCSum = GetRTCSecond();
				AppStatus = STATUS_HI3519POWEROFF;
			}
		break;
		
		case STATUS_HI3519WAITPHOTO:	//等待一个点的拍照上传完成
			
			if(GetHi3519StatusFlag() == TRUE)
			{
				s_NowPosition = GetConsolePosition();
				SetHi3519StatusFlag(FALSE);
				AppStatus = STATUS_HI3519GOTOPOS;
				if(GetPhotoSuccessFlag() == TRUE)
				{
					PhotoUseTime = DifferenceOfRTCTime(GetRTCSecond(), PhotoUseTime);
					u1_printf("\r\n 拍照点(%d)完成,耗时:%ds\r\n\r\n", s_NowPosition, PhotoUseTime);
					s_WaitOverTimeCount = 0;
					s_PhotoErrCount = 0;
					s_PhotoErrCount = 0;
				}
				else
				{
					u1_printf("\r\n 拍照点(%d)失败\r\n\r\n", s_NowPosition);
					s_PhotoErrCount++;
				}
			}
			else if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) > HI3519_PHOTO_WAIT)
			{
				AppStatus = STATUS_HI3519GOTOPOS;
				u1_printf(" 拍照点(%d)等待超时\r\n", s_NowPosition);
			}
			
			SetPhotoSuccessFlag(FALSE);
		break;
		
		case STATUS_HI3519GOTOPOS:	//前往下一拍照点
			
			p_Config = GetPhotoConfig();
			
			s_NowPosition = GetConsolePosition();
			if(s_NowPosition != p_Config->PhotoPointNum)
			{		
				RTC_EasyShowTime();
				MotorPowerOn();	
				AppStatus = STATUS_HI3519WAITMOTOR;
				s_RTCSum = GetRTCSecond();
			}
			else
			{
				AppStatus = STATUS_HI3519POWEROFF;
				s_CameraRTCTime = GetRTCSecond();
				s_RTCSum = GetRTCSecond();
				u1_printf("\r\n 所有拍照点已完成, Power off\r\n");
			}
		break;
		
		case STATUS_HI3519WAITMOTOR:	//等待转至预置点
			
			if(GetConsolePowerFlag() == FALSE)
			{	
				s_NowPosition = GetConsolePosition();
				RTC_EasyShowTime();
				u1_printf(" 拍照点(%d)开始!\r\n\r\n", s_NowPosition);
				PhotoUseTime = GetRTCSecond();
				AppStatus = STATUS_HI3519WAITPHOTO;
				delay_ms(1000);
				SendSTM32TakeaPhotoCmdbyRetryMode();	//发送拍照指令
				s_RTCSum = GetRTCSecond();
			}
			
		break;
		
		case STATUS_HI3519POWEROFF:
			
			p_sys = GetSystemConfig();
		
			if(p_sys->isNoSleepFlag)		//不休眠
			{
				u1_printf(" 进入不休眠模式\r\n");
				
				AppStatus = STATUS_DONOTSLEEP;
				
				s_CameraRTCTime = GetRTCSecond();
				
				StoreOperationalData();
				
				TaskForHi3519CommunicationEnd();
			}
			else
			{
				if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) >= 11)	//13s后关闭
				{	
					STM32ToHi3519PowerOff(0);
					delay_ms(1000);
					delay_ms(1000);
					delay_ms(1000);
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
					u1_printf("\r\n [%0.2d:%0.2d:%0.2d]Go to Sleep\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
					s_RTCSum = GetRTCSecond();
					StoreOperationalData();
					s_CameraRTCTime = GetRTCSecond();
					TaskForLEDBlinkEnd();
					TaskForHi3519CommunicationEnd();
					Hi3519PowerOff();
					p_Config = GetPhotoConfig();
					
					if(p_Config->PhotoPointNum >= 1)
					{		

						s_NowPosition = GetConsolePosition();
						u1_printf(" 当前预置点(%d),角度为:%d, %d\r\n", s_NowPosition, p_Config->HorizontalAngle[s_NowPosition-1], p_Config->VerticalAngle[s_NowPosition-1]);
						if(p_Config->VerticalAngle[s_NowPosition-1] >= 10)
						{
							MotorGotoAnglePowerOn(p_Config->HorizontalAngle[s_NowPosition-1], 0);
							AppStatus = STATUS_MOTORGOTOZERO;
						}
						else
						{
							AppStatus = STATUS_SLEEP;
						}
					}
					else
					{
						AppStatus = STATUS_SLEEP;
					}
				}
			}
		break;
		
		case STATUS_MOTORGOTOZERO:		
			if((GetConsolePowerFlag() == FALSE) || (DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) >= 6))
			{	
				if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) >= 5)
				{
					MotorPowerOff();
					delay_ms(100);
					u1_printf(" Time Out,Sleep\r\n");
				}
				AppStatus = STATUS_SLEEP;
			}
		break;
		
		case STATUS_SLEEP:	//进入休眠
			
			if(GetMe909Status() == MeConnected)
			{
				SetStopModeTime(TRUE, STOP_TIME);
			}
			else
			{
				SetStopModeTime(TRUE, ERR_STOP_TIME);
			}
			
			AppStatus = STATUS_RUN;
			
		break;
		
		case STATUS_RUN:	//STM32开始运行
						
			p_Config = GetPhotoConfig();
			
			if(TimeIntheInterval())	//在拍照间隔
			{
				if((DifferenceOfRTCTime(GetRTCSecond(), s_CameraRTCTime) >= p_Config->PhotoInterval*60) || TimeToStart())	//每一段时间拍照一张照片
				{
					SetConsolePosition(p_Config->PhotoPointNum);
					f_Ill = Get_Ill_Value();
					SetLogErrCode(LOG_CODE_CLEAR);
					s_CameraRTCTime = GetRTCSecond();
								
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
					p_Config = GetPhotoConfig();
					
					TaskForLEDBlinkRun();
					ClearReportDataSuccessFlag();
					s_KeepAliveTime = GetRTCSecond();

					SetTimeToPowerOffHi3519Flag(FALSE);
					SetHi3519StatusFlag(FALSE);
					AppStatus = STATUS_INIT_SENSOR;
				}
				else if(s_PhotoErrCount && s_ErrRetryCount < 2)	//如果上一次拍照失败,则无论在什么时间将进行一次拍照尝试
				{
					u1_printf(" 上一次与服务器通信失败，进行重连(Err:%d)\r\n", s_PhotoErrCount); 
					s_ErrRetryCount++;
					s_PhotoErrCount = 0;
					SetTimeToPowerOffHi3519Flag(FALSE);
					SetLogErrCode(LOG_CODE_PHOTOFAIL);
					StoreOperationalData();
					AppStatus = STATUS_INIT_SENSOR;
				}
				else
				{
					if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) >= s_SensorInterval)	//每一段时间检测一次传感器数据
					{
						RTC_EasyShowTime();
						u1_printf(" Sensor Detect--");
						s_RTCSum = GetRTCSecond();
						StartSensorMeasure();
						AppStatus = STATUS_GET_SENSOR;
						
					}
					else
					{
						AppStatus = STATUS_SLEEP;		
					}						
				}
			}
			else if(DifferenceOfRTCTime(GetRTCSecond(), s_KeepAliveTime) >= 7200)	//不在拍照间隔，间隔一段时间连接一次服务器
			{
				SetTimeToPowerOffHi3519Flag(FALSE);

				AppStatus = STATUS_INIT_SENSOR;
				
				u1_printf("\r\n 不在拍照间隔，与服务器进行一次交互\r\n"); 
				SetLogErrCode(LOG_CODE_NOWORK);
				TaskForLEDBlinkRun();
				ClearReportDataSuccessFlag();
				s_KeepAliveTime = GetRTCSecond();
			}
			else if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) >= s_SensorInterval)	//每一段时间检测一次传感器数据
			{
				RTC_EasyShowTime();
				u1_printf(" Sensor Detect--");
				s_RTCSum = GetRTCSecond();
				StartSensorMeasure();
				AppStatus = STATUS_GET_SENSOR;
				
			}
			else if((GetReportDataSuccessFlag() == FALSE || s_PhotoErrCount) && s_ErrRetryCount < 2)	//如果上一次拍照失败,则无论在什么时间将进行一次拍照尝试
			{
				u1_printf(" 上一次与服务器通信失败，进行重连(Err:%d)\r\n", s_PhotoErrCount); 
				s_ErrRetryCount++;
				s_PhotoErrCount = 0;
				SetTimeToPowerOffHi3519Flag(FALSE);
				SetLogErrCode(LOG_CODE_PHOTOFAIL);
				StoreOperationalData();
				AppStatus = STATUS_INIT_SENSOR;
			}					
			else
			{
				AppStatus = STATUS_SLEEP;
			}
		break;
		
		case STATUS_GET_SENSOR:
			if((BatStatus != BAT_WORKING))	//等待传感器数据监测完成
			{
				//显示传感器数据
				GetPosition(&Latitude, &Longitude);
//				Altitude = GetAltitude();
				u1_printf(" Ill:%2.1f,Vol:%d,Lati:%.1f,Logi:%.1f\r\n",\
				Get_Ill_Value(),\
				Get_Battery_Vol(),\
				Latitude,\
				Longitude);
				
				p_sys = GetSystemConfig();
		
				if(p_sys->isNoSleepFlag)		//不休眠
				{
					if(Get_Battery_Vol() < 7200)
					{
						p_sys->isNoSleepFlag = 0;
						
						Set_System_Config(p_sys);
						
						u1_printf("\r\n - - -电池电量低，进入休眠模式- - -\r\n");
						AppStatus = STATUS_SLEEP;	
					}
					else
					{
						AppStatus = STATUS_DONOTSLEEP;	
					}						
				}
				else
				{
					AppStatus = STATUS_SLEEP;			
				}
			}
		break;
		
		case STATUS_DONOTSLEEP:
			
			p_Config = GetPhotoConfig();
			
			if(DifferenceOfRTCTime(GetRTCSecond(), s_CameraRTCTime) >= p_Config->PhotoInterval*60)	//每一段时间拍照一张照片
			{
				s_CameraRTCTime = GetRTCSecond();
				
				SendSTM32TakeaPhotoCmdbyRetryMode();
			}
			else if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSum) >= s_SensorInterval)	//每一段时间检测一次传感器数据
			{
				RTC_EasyShowTime();
				u1_printf(" Sensor Detect--");
				s_RTCSum = GetRTCSecond();
				StartSensorMeasure();
				AppStatus = STATUS_GET_SENSOR;
				
			}			
		break;
		
		default:
			
		break;
	}
}

static void TestMode(void)
{
	MotorTestPowerOn();	//云台电源开
	
	Hi3519PowerOn();	//Hi3519电源开
	
	EXTI17_Reset();	//关闭闹钟
}

void App_Run(void)
{
	s_TestFlag = FALSE;

	Task_Create(TaskForAppRun, 1);
}

void TestModeApp_Run(void)
{
	if(s_TestFlag == FALSE)
	{
		s_TestFlag = TRUE;
		
		Task_Kill(TaskForAppRun);
		
		TestMode();
		
		u1_printf(" Enter Test Mode...\r\n");
	}
}








