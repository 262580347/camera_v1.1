/**********************************
说明:调试使用的串口代码
	  
作者:关宇晟
版本:V2017.4.3
***********************************/
#include "Uart_Com.h"
#include "main.h"

unsigned char g_ComTestFlag = FALSE;

static void SendMsg_StatusOK(void)
{
	static u16 s_Seq = 1;
	CAMERA_COMM_MSG m_Msg;
	CAMERA_COMM_MSG *pMsg=&m_Msg;

	pMsg->Protocol	= 0x1234;	
	pMsg->DeviceID 	= 0x55667788;
	pMsg->Dir 	= 0;	//TARGET_HANDLE
	pMsg->Seq 	= s_Seq++;		
	pMsg->Length 	= 0x02;		//LEN
	pMsg->OPType	= 0x05;
	pMsg->UserBuf[0]	= 0xbb;		//STATUS OK
	pMsg->UserBuf[1]	= 0xcc;		//STATUS OK
	
	CameraCommGet()->SendMsg(pMsg);
}

static void COM1Protocol(void)
{
	unsigned char PackBuff[130], DataPack[130];
	u16   PackLength = 0, RecLength = 0, i;
	static CLOUD_HDR *hdr;
	
	UnPackMsg(g_USART_RX_BUF+1, g_USART_RX_CNT-2, PackBuff, &PackLength);	//解包

	if (PackBuff[(PackLength)-1] == Calc_Checksum(PackBuff, (PackLength)-1))
	{	
		hdr = (CLOUD_HDR *)PackBuff;
		hdr->protocol = swap_word(hdr->protocol);
		hdr->device_id = swap_dword(hdr->device_id);
		hdr->seq_no = swap_word(hdr->seq_no);
		hdr->payload_len = swap_word(hdr->payload_len);
		
		RecLength = hdr->payload_len + sizeof(CLOUD_HDR) + 1;
					
		if (RecLength != (PackLength))
		{
			u1_printf(" Pack Length Err\r\n");
		}
		else
		{
			PackLength--;	
			memcpy(&DataPack, PackBuff, sizeof(CLOUD_HDR));

			if(hdr->payload_len != 0)
				memcpy(&DataPack[sizeof(CLOUD_HDR)], &PackBuff[sizeof(CLOUD_HDR)], hdr->payload_len);

			OnDebug(DataPack, PackLength);
		}	
	}
	else
	{
		for(i=0; i<PackLength; i++)
		{
			u1_printf("%02X ", PackBuff[i]);
		}
		u1_printf("\r\n");
		u1_printf(" CRC Error :%02X\r\n", Calc_Checksum(PackBuff, (PackLength)-1));
	}	
}
static void TimeTest(void)
{
	static unsigned int Seq = 0;
	static cJSON *root;
	static char *out;
	
	
	root=cJSON_CreateObject();	
	cJSON_AddNumberToObject(root, "CMD", 		1);
	cJSON_AddNumberToObject(root,"Dir",			0);
	cJSON_AddNumberToObject(root,"Seq",			Seq++);
	cJSON_AddNumberToObject(root,"Temp",		25.53);
	cJSON_AddNumberToObject(root,"Humi", 		35.5);
	cJSON_AddNumberToObject(root,"Ill",			15000.5);
	cJSON_AddNumberToObject(root,"1231",		1013.5);
	cJSON_AddNumberToObject(root,"123",		25.53);
	cJSON_AddNumberToObject(root,"321", 		35.5);
	cJSON_AddNumberToObject(root,"222",			15000.5);
	cJSON_AddNumberToObject(root,"333",		1013.5);
	cJSON_AddNumberToObject(root,"a1231",		1013.5);
	cJSON_AddNumberToObject(root,"a123",		25.53);
	cJSON_AddNumberToObject(root,"a1231",		1013.5);
	
	out=cJSON_Print(root);	

	cJSON_Delete(root);	

	u1_printf("%s\n",out);	
	myfree(out);		
	
	u1_printf(" Memory utilization:%d%%\r\n\r\n", mem_perused());
}


void TaskForDebugCOM(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;	
	CHARGING_CFG*	p_Charg;
	u16   i, j, CRCVal;
	u8  databuf[256], Num, data[64];
	unsigned int  databuf4[64];
	unsigned int Addr, Hor, Ver;
	static float f_Lat = 0, f_Long = 0;
	static u16 s_Seq = 1;
	CAMERA_COMM_MSG m_Msg;
	CAMERA_COMM_MSG *pMsg=&m_Msg;
	ANGLESTORE_CFG PhotoConfig;
	
	
	if(g_UartRxFlag == TRUE)
	{
		g_UartRxFlag = FALSE;
		
		if(strncmp((char *)g_USART_RX_BUF, "RESET", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n RESET CMD\r\n");
			while (DMA_GetCurrDataCounter(DMA1_Channel4));
			Sys_Soft_Reset();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "CLEAR", 5) == 0)
		{		
			StartSensorMeasure();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "CMD ", 4) == 0)
		{		
			s_Seq++;
			Addr = ArrayToInt32(&g_USART_RX_BUF[4], g_USART_RX_CNT-4);
			u1_printf(" Cmd:%d\r\n", Addr);
			STM32ToSTM32TestCmd(s_Seq, Addr);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "GPSOFF", 6) == 0)
		{		
			GPSPowerOff();
			SetGPSDataFlag(TRUE);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "GPSON", 5) == 0)
		{		
			u1_printf(" GPS On!\r\n");
			GPSPowerOn();
			SetGPSDataFlag(FALSE);
		}
			
		else if(strncmp((char *)g_USART_RX_BUF, "HION", g_USART_RX_CNT) == 0)
		{	
			s_Seq++;
			Hi3519PowerControl(s_Seq%2);
			if(s_Seq%2)
			{
				u1_printf("Hi POWER ON\r\n");
			}
			else
			{
				u1_printf("Hi POWER OFF\r\n");
			}
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Histatus", g_USART_RX_CNT) == 0)
		{	
			s_Seq++;
			STM32ToHi3519GetStatus(s_Seq);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Hidet", g_USART_RX_CNT) == 0)
		{
			s_Seq++;
			STM32ToHi3519DetCommunicationMsg(s_Seq);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Hipower", g_USART_RX_CNT) == 0)
		{
			s_Seq++;
			STM32ToHi3519PowerOff(s_Seq);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "111", g_USART_RX_CNT) == 0)
		{
			TimeTest();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Hi3519", g_USART_RX_CNT) == 0)
		{
			STM32ToHi3519GetStatus(s_Seq++);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Hiexit", g_USART_RX_CNT) == 0)
		{
			STM32ToHi3519ExitProcess(s_Seq++);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Retry1", g_USART_RX_CNT) == 0)
		{
			SendSTM32DetCommunicationbyRetryMode();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Retry2", g_USART_RX_CNT) == 0)
		{
			SendSTM32GetStatusCmdbyRetryMode();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Retry3", g_USART_RX_CNT) == 0)
		{
			SendSTM32PowerOffbyRetryMode();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "SHOW", 4) == 0)
		{		
			u1_printf(" Temp: %2.2f, Humi: %2.2f, Ill:%2.2f, Pre: %2.2f, Vol: %d\r\n", Get_Ill_Value(),Get_Battery_Vol());
		}
		else if(strncmp((char *)g_USART_RX_BUF, "$", 1) == 0)
		{		
			g_USART_RX_BUF[g_USART_RX_CNT] = '\0';
			u1_printf("%s\r\n", g_USART_RX_BUF);
			u2_printf("%s\r\n", g_USART_RX_BUF);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "GPSRESET", g_USART_RX_CNT) == 0)
		{		
			u1_printf("GPS RESET\r\n");
					
			GPIO_ResetBits(GPIOB,GPIO_Pin_15);
			
			delay_ms(300);
			
			GPIO_SetBits(GPIOB,GPIO_Pin_15);

		}
		
		else if(strncmp((char *)g_USART_RX_BUF, "U3", 2) == 0)////////////////////////////////////////////////////////////////////////////
		{		
			u1_printf("\r\n Send:%s\r\n", &g_USART_RX_BUF[2]);
			u3_printf("%s", &g_USART_RX_BUF[2]);
		}
		
		else if(strncmp((char *)g_USART_RX_BUF, "TEST MODE", g_USART_RX_CNT) == 0)
		{
			if(g_ComTestFlag == FALSE)
			{
				g_ComTestFlag = TRUE;
				TestModeApp_Run();
			}
			else
			{
				u1_printf("\r\n Exit debugging mode, communication resume\r\n");
				g_ComTestFlag = FALSE;
				App_Run();
			}
			
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Console", g_USART_RX_CNT) == 0)
		{
			USART3_Config(9600);	//云台波特率为2400
		
			Uart3ChannelSet(CHANNEL_MOTOR);	//uart2与GPS共用，使用模拟开关选择
			
			MotorPowerControl(TRUE);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "ConsolePowerOff", g_USART_RX_CNT) == 0)
		{
			u1_printf("PowerOff\r\n");
			MotorPowerOff();
			
		}
		else if(strncmp((char *)g_USART_RX_BUF, "HisiPowerOff", g_USART_RX_CNT) == 0)
		{
			u1_printf("HisiPowerOff\r\n");
			Hi3519PowerOff();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Start Charging", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n Forced charging on\r\n");
			p_Charg = GetChargingStatus();
			
			p_Charg->ForceFlag = FORCED_CHARGE_ENABLE;
			p_Charg->Chksum = Calc_Checksum((unsigned char *)&p_Charg->Enable, 2);
			EEPROM_WriteBytes(BQ_FLAG_ADDR, (unsigned char *)&p_Charg, sizeof(CHARGING_CFG));

			SetBQ24650ENFlag(TRUE);	//关闭充电芯片的唯一入口	
			StartGetBatVol();
			
			GPIO_InitStructure.GPIO_Pin = BQ24650_EN_PIN ;	
			GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
			GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
			GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
			GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
			GPIO_Init(BQ24650_EN_TYPE, &GPIO_InitStructure);
			BQ24650_ENABLE();
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "Stop Charging", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n Forced charge shutdown\r\n");
			p_Charg = GetChargingStatus();
			
			p_Charg->ForceFlag = FORCED_CHARGE_DISABLE;
			p_Charg->Chksum = Calc_Checksum((unsigned char *)&p_Charg->Enable, 2);
			EEPROM_WriteBytes(BQ_FLAG_ADDR, (unsigned char *)&p_Charg, sizeof(CHARGING_CFG));

			SetBQ24650ENFlag(FALSE);	//关闭充电芯片的唯一入口	
			StartGetBatVol();
			GPIO_InitStructure.GPIO_Pin = BQ24650_EN_PIN ;	
			GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
			GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
			GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
			GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
			GPIO_Init(BQ24650_EN_TYPE, &GPIO_InitStructure);
			BQ24650_DISABLE();
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "PowerDetect", g_USART_RX_CNT) == 0)
		{
			u1_printf("PowerDetect\r\n");
			StartGetBatVol();
		}		
		else if(strncmp((char *)g_USART_RX_BUF, "EraseLog", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n Erase EEPROM Data\r\n");
			for(i=9; i< EEPROM_BLOCK_COUNT; i++)
			{
				EEPROM_EraseWords(i);
				u1_printf(" Clear %d Block\r\n", i+1);
			}
			
			LogStorePointerInit();		//日志功能区
		}		
		else if((strncmp((char *)g_USART_RX_BUF, "log", g_USART_RX_CNT) == 0) || (strncmp((char *)g_USART_RX_BUF, "LOG", g_USART_RX_CNT) == 0))
		{
			ShowLogContent();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "STM32 STOP", g_USART_RX_CNT) == 0)
		{
			u1_printf(" Stop\r\n");
			SetStopModeTime( TRUE, 10);
		}

		else if(strncmp((char *)g_USART_RX_BUF, "READ1", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\nEEPROM Read1\r\n");
			
			for(j=0; j<16; j++)
			{
				EEPROM_ReadBytes(EEPROM_BASE_ADDR + j*256, databuf, 256); 
				
				for(i=0; i<256; i++)
				{
					u1_printf("%02X ", databuf[i]);
				}
				u1_printf("\r\n");
			}
			u1_printf("\r\n");
		}
		else if(strncmp((char *)g_USART_RX_BUF, "READ4", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\nEEPROM Read4\r\n");
					
			for(j=0; j<64; j++)
			{
				EEPROM_ReadWords(EEPROM_BASE_ADDR + j*64, databuf4, 16); 
				
				for(i=0; i<16; i++)
				{
					u1_printf("%08X ", databuf4[i]);
				}
				u1_printf("\r\n");
			}
			u1_printf("\r\n");
		}
		else if(strncmp((char *)g_USART_RX_BUF, "WRITE1 ", 7) == 0)
		{
			Addr = ArrayToInt32(&g_USART_RX_BUF[7], g_USART_RX_CNT-7);

			u1_printf("\r\nWRITE1 :%08X\r\n", Addr*64 + EEPROM_BASE_ADDR);
			for(i=0; i<64; i++)
			{
				databuf[i] = i;
			}
			EEPROM_WriteBytes(Addr*64 + EEPROM_BASE_ADDR, databuf, 64); 
		}
		else if(strncmp((char *)g_USART_RX_BUF, "WRITE4 ", 7) == 0)
		{
			Addr = ArrayToInt32(&g_USART_RX_BUF[7], g_USART_RX_CNT-7);

			u1_printf("\r\nWRITE4 :%08X\r\n", Addr*64 + EEPROM_BASE_ADDR);
			for(i=0; i<16; i++)
			{
				databuf4[i] = i;
			}
			EEPROM_WriteWords(Addr*64 + EEPROM_BASE_ADDR, databuf4, 16); 
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Erase1 ", 7) == 0)
		{
			Addr = ArrayToInt32(&g_USART_RX_BUF[7], g_USART_RX_CNT-7);

			u1_printf("\r\nErase :%08X\r\n", Addr*64 + EEPROM_BASE_ADDR);
			
			EEPROM_EraseWords(Addr);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "EraseAll", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n Erase All EEPROM Data\r\n");
			for(i=0; i< EEPROM_BLOCK_COUNT; i++)
			{
				EEPROM_EraseWords(i);
				u1_printf(" Clear %d Block\r\n", i+1);
			}
		}
		else if(strncmp((char *)g_USART_RX_BUF, "TIME ", 5) == 0)
		{			
			Addr = ArrayToInt32(&g_USART_RX_BUF[5], g_USART_RX_CNT-5);
			u1_printf(" Set Time:%d\r\n", Addr);
//			Task_SetTime(TimeTest, Addr);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "KillTimer ", 10) == 0)
		{			
			Addr = ArrayToInt32(&g_USART_RX_BUF[10], g_USART_RX_CNT-10);
			u1_printf("Kill Timer Num:%d\r\n", Addr);
			KillTimer(Addr);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "TXD", g_USART_RX_CNT) == 0)
		{	
			SendMsg_StatusOK();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "CTR ", 4) == 0)
		{	
			Addr = ArrayToInt32(&g_USART_RX_BUF[4], g_USART_RX_CNT-4);
			u1_printf("CMD:%d\r\n", Addr);
			SendConsoleCMD(Addr, 0, 0);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "FIFO", g_USART_RX_CNT) == 0)
		{			
			pMsg->Protocol	= 0x9988;	
			pMsg->DeviceID 	= 0x12345678;
			pMsg->Dir 	= 0;	//TARGET_HANDLE
			pMsg->Seq 	= s_Seq;		
			pMsg->Length 	= 0x02;		//LEN
			pMsg->OPType	= 0x06;
			pMsg->UserBuf[0]	= 0xbb;		//STATUS OK
			pMsg->UserBuf[1]	= 0xcc;		//STATUS OK
			u1_printf(" FIFO:%d ", s_Seq++);
			CameraCommGet() -> SendMsgbyFIFO(pMsg);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "485Code ", 8) == 0)
		{
			Num = (g_USART_RX_CNT - 7)/3;
			u1_printf("Send 485 Code:");
			for(i=0; i<Num; i++)
			{
				StrToHex(&data[i], &g_USART_RX_BUF[8+i*3], Num);
				u1_printf("%02X ", data[i]);
			}				
			
			CRCVal = Calculate_CRC16(data, Num);
			data[Num++] = CRCVal&0xff;
			data[Num++] = CRCVal >> 8;
			
			u1_printf(" %02X %02X", CRCVal&0xff, CRCVal >> 8);
			
			u1_printf("\r\n");
			
			Uart_Send_Data(USART3, data, Num);
		}
		
		
		else if(strncmp((char *)g_USART_RX_BUF, "Lat ", 4) == 0)
		{		
			f_Lat = atof((char*)&g_USART_RX_BUF[4]);
			u1_printf(" Lat:%.5f\r\n", f_Lat);
			
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Lng ", 4) == 0)
		{		
			f_Long = atof((char*)&g_USART_RX_BUF[4]);
			u1_printf(" Lat:%.5f  Long:%.5f\r\n", f_Lat, f_Long);
			SetPosition(f_Lat, f_Long);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "ReadAngle", g_USART_RX_CNT) == 0)
		{
			ReadStoreAngle();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "ShowGPS", g_USART_RX_CNT) == 0)
		{
			u1_printf(" ShowGPS\r\n");
			SetGPSShowInfo(TRUE);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "ShowGPS0", g_USART_RX_CNT) == 0)
		{
			u1_printf(" ShowGPS0\r\n");
			SetGPSShowInfo(FALSE);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Photo", g_USART_RX_CNT) == 0)
		{
			SendSTM32TakeaPhotoCmdbyRetryMode();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "AngleConfig", g_USART_RX_CNT) == 0)
		{
			STM32ToHi3519MorePresetConfig(1);
		}		
		else if(strncmp((char *)g_USART_RX_BUF, "Angle ", 6) == 0)
		{
			Hor = ArrayToInt32(&g_USART_RX_BUF[6], 3);
			Ver = ArrayToInt32(&g_USART_RX_BUF[10], 3);
			Num = ArrayToInt32(&g_USART_RX_BUF[14], 2);
			if(Num >= 20)
			{
				u1_printf(" Num Over\r\n");
			}
			else
			{
				u1_printf(" Set:(%d) %d %d\r\n", Num, Hor, Ver);
				WriteNowAngle(Hor, Ver, Num);
			}
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Con ", 4) == 0)
		{
			PhotoConfig.PhotoPointNum = ArrayToInt32(&g_USART_RX_BUF[4], 1);
			PhotoConfig.PhotoInterval = ArrayToInt32(&g_USART_RX_BUF[5], 1);
			
			PhotoConfig.StartPhotoTime[0] = ArrayToInt32(&g_USART_RX_BUF[6], 1);
			PhotoConfig.StartPhotoTime[1] = ArrayToInt32(&g_USART_RX_BUF[7], 1);
			PhotoConfig.StartPhotoTime[2] = ArrayToInt32(&g_USART_RX_BUF[8], 1);
			
			PhotoConfig.EndPhotoTime[0] = ArrayToInt32(&g_USART_RX_BUF[9], 1);
			PhotoConfig.EndPhotoTime[1] = ArrayToInt32(&g_USART_RX_BUF[10], 1);
			PhotoConfig.EndPhotoTime[2] = ArrayToInt32(&g_USART_RX_BUF[11], 1);
			
			SetPhotoConfig(&PhotoConfig);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "UP", g_USART_RX_CNT) == 0)
		{
			ConsoleUp();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "DOWN", g_USART_RX_CNT) == 0)
		{
			ConsoleDown();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "LEFT", g_USART_RX_CNT) == 0)
		{
			ConsoleLeft();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "RIGHT", g_USART_RX_CNT) == 0)
		{
			ConsoleRight();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "STOP", g_USART_RX_CNT) == 0)
		{
			ConsoleStop();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "SetTime", g_USART_RX_CNT) == 0)
		{
			SysSetRTCTime(19, 5, 5, 5, 5, 5);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "ShowTime", g_USART_RX_CNT) == 0)
		{
			RTC_TimeShow();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "RunMode0", g_USART_RX_CNT) == 0)
		{
			u1_printf(" 云台进入正常模式\r\n");
			ConsoleRunModeisNormal();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "RunMode1", g_USART_RX_CNT) == 0)
		{
			u1_printf(" 云台进入测试模式\r\n");
			ConsoleRunModeisTest();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "GPIO6", g_USART_RX_CNT) == 0)
		{
			u1_printf("GPIO6\r\n");
			GPIO_ToggleBits(GPIOA, GPIO_Pin_6);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "GPIO7", g_USART_RX_CNT) == 0)
		{
			u1_printf("GPIO7\r\n");
			GPIO_ToggleBits(GPIOA, GPIO_Pin_7);
		}
		else if(g_USART_RX_BUF[0] == 0xff && g_USART_RX_BUF[1] == 0x01)
		{
			for(i=0; i<g_USART_RX_CNT; i++)
			{
				u1_printf("%02X ", g_USART_RX_BUF[i]);
			}
			u1_printf("\r\n\r\n");
		}
		else
		{		
			if(g_USART_RX_BUF[0] == BOF_VAL && g_USART_RX_BUF[g_USART_RX_CNT-1] == EOF_VAL && g_USART_RX_CNT >= 15)
			{
				COM1Protocol();
			}
		}
		

		Clear_Uart1Buff();
	}	
	

}










