#ifndef 	_UART_H_
#define		_UART_H_

#define 	USART_REC_LEN  				150  
#define 	USART2_REC_LEN  			550 
#define 	USART3_REC_LEN  			600   //GPS信息量大

#define		SIM_COM			USART2
#define		LORA_COM		USART2
#define		GPRS_COM		USART2
#define		NB_COM			USART3

#define		SIM_BAND_RATE		115200
#define		 LORA_BAND_RATE		115200

#define		USART3_CHANNEL_PIN		GPIO_Pin_8
#define		USART3_CHANNEL_TYPE		GPIOA

#include "stm32l1xx.h"

typedef enum
{
	CHANNEL_MOTOR 	= 0,//选择云台
	CHANNEL_GPS  	= 1,//选择GPS
	CHANNEL__SIZE 	= 2,
}COM3_CHANNEL;

void Uart_Send_Char(USART_TypeDef* USARTx, unsigned char ch);
void Uart_Send_Data(USART_TypeDef* USARTx, unsigned char *data, unsigned char len);
void Uart_Send_Str(USART_TypeDef* USARTx, char *data);
	
extern unsigned char   g_UartRxFlag;					//串口1接收完标志
extern unsigned char  	g_USART_RX_BUF[USART_REC_LEN];	//串口1缓冲区
extern unsigned short int 	g_USART_RX_CNT;					//串口1消息长度

extern unsigned char   g_Uart2RxFlag;					//串口2接收完标志
extern unsigned char  	g_USART2_RX_BUF[USART2_REC_LEN];	//串口2缓冲区
extern unsigned short int 	g_USART2_RX_CNT;					//串口2消息长度

extern unsigned char 	g_Uart3RxFlag;					//串口3接收完标志
extern unsigned char  	g_USART3_RX_BUF[USART3_REC_LEN];//串口3缓冲区
extern unsigned short int 	g_USART3_RX_CNT;				//串口3消息长度

void USART1_Config(unsigned int bound);
void USART2_Config(unsigned int bound);
void USART3_Config(unsigned int bound);

void Clear_Uart1Buff(void);
void Clear_Uart2Buff(void);
void Clear_Uart3Buff(void);

void Uart3ChannelSet(COM3_CHANNEL Com_Channel);

#endif

