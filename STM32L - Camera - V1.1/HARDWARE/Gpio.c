#include "main.h"
#include "Gpio.h"

#ifdef		CAMERA_VERSION_V1_1

#define		RUN_LED_GPIO_PIN		GPIO_Pin_6
#define		RUN_LED_GPIO_TYPE		GPIOB

#define		NET_LED_GPIO_PIN		GPIO_Pin_7
#define		NET_LED_GPIO_TYPE		GPIOB

#define		SENSOR_POWER_PIN		GPIO_Pin_8
#define		SENSOR_POWER_TYPE		GPIOB

#define		SDA_PIN		GPIO_Pin_0
#define		SDA_TYPE	GPIOA
#define		SCK_PIN		GPIO_Pin_1
#define		SCK_TYPE	GPIOA

#endif

#ifdef		CAMERA_VERSION_V1_2

#define		RUN_LED_GPIO_PIN		GPIO_Pin_3
#define		RUN_LED_GPIO_TYPE		GPIOB

#define		NET_LED_GPIO_PIN		GPIO_Pin_4
#define		NET_LED_GPIO_TYPE		GPIOB

#define		BLUE_LED_GPIO_PIN		GPIO_Pin_15
#define		BLUE_LED_GPIO_TYPE		GPIOA

#define		SENSOR_POWER_PIN		GPIO_Pin_7
#define		SENSOR_POWER_TYPE		GPIOB

#define		SDA_PIN		GPIO_Pin_6
#define		SDA_TYPE	GPIOB
#define		SCK_PIN		GPIO_Pin_5
#define		SCK_TYPE	GPIOB

#endif

#define		LED_DEFAULT_TIME		1000

void LEDNoWork(void)
{	
	GPIO_ResetBits(RUN_LED_GPIO_TYPE,RUN_LED_GPIO_PIN);
	GPIO_ResetBits(NET_LED_GPIO_TYPE,NET_LED_GPIO_PIN);
}

void LEDResuseWork(void)
{	
	GPIO_SetBits(RUN_LED_GPIO_TYPE,RUN_LED_GPIO_PIN);
	GPIO_SetBits(NET_LED_GPIO_TYPE,NET_LED_GPIO_PIN);
}

static void TaskForRunLED(void)
{
	static BOOL isFirst = FALSE;
	GPIO_InitTypeDef GPIO_InitStructure;	
	static u8 s_LastStatus = FALSE;
	
	if(!isFirst)
	{			
		GPIO_InitStructure.GPIO_Pin = RUN_LED_GPIO_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(RUN_LED_GPIO_TYPE, &GPIO_InitStructure);
		/* Force a low level on LEDs*/ 	
		GPIO_ResetBits(RUN_LED_GPIO_TYPE,RUN_LED_GPIO_PIN);
		
		isFirst = TRUE;
	}
		
	if(s_LastStatus != GetHi3519LinkFlag())
	{
		if(GetHi3519LinkFlag())
		{
			s_LastStatus = GetHi3519LinkFlag();
			Task_SetTime(TaskForRunLED, 5000);
		}
		else
		{
			s_LastStatus = GetHi3519LinkFlag();
			Task_SetTime(TaskForRunLED, LED_DEFAULT_TIME);
		}
	}

	GPIO_ToggleBits(RUN_LED_GPIO_TYPE, RUN_LED_GPIO_PIN);	
	
	#ifdef		CAMERA_VERSION_V1_2
	GPIO_ToggleBits(BLUE_LED_GPIO_TYPE, BLUE_LED_GPIO_PIN);
	#endif
}

static void TaskForNetLED(void)
{
	static BOOL isFirst = FALSE;
	GPIO_InitTypeDef GPIO_InitStructure;	
	static u8 s_LastStatus = FALSE;
	
	if(!isFirst)
	{			
		GPIO_InitStructure.GPIO_Pin = NET_LED_GPIO_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(NET_LED_GPIO_TYPE, &GPIO_InitStructure);
		/* Force a low level on LEDs*/ 	
		GPIO_ResetBits(RUN_LED_GPIO_TYPE,RUN_LED_GPIO_PIN);
		
		isFirst = TRUE;
	}
	
	if(s_LastStatus != GetHi3519LinkFlag())
	{
		if(GetHi3519LinkFlag())
		{
			s_LastStatus = GetHi3519LinkFlag();
			Task_SetTime(TaskForNetLED, 5000);
		}
		else
		{
			s_LastStatus = GetHi3519LinkFlag();
			Task_SetTime(TaskForNetLED, LED_DEFAULT_TIME);
		}
	}
	
	GPIO_ToggleBits(NET_LED_GPIO_TYPE, NET_LED_GPIO_PIN);	
	
}
void STM32_GPIO_Init(void)
{
	static u8 RunOnce = FALSE;
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC | RCC_AHBPeriph_GPIOH, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOC, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOH, &GPIO_InitStructure);		
	
	GPIO_InitStructure.GPIO_Pin = RUN_LED_GPIO_PIN | NET_LED_GPIO_PIN;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(RUN_LED_GPIO_TYPE, &GPIO_InitStructure);
		
	GPIO_SetBits(RUN_LED_GPIO_TYPE,RUN_LED_GPIO_PIN);
	GPIO_SetBits(NET_LED_GPIO_TYPE,NET_LED_GPIO_PIN);
	
	
	#ifdef		CAMERA_VERSION_V1_2
	GPIO_InitStructure.GPIO_Pin = BLUE_LED_GPIO_PIN;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(BLUE_LED_GPIO_TYPE, &GPIO_InitStructure);
		
	GPIO_SetBits(BLUE_LED_GPIO_TYPE,BLUE_LED_GPIO_PIN);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	

	#endif
	if(RunOnce == FALSE)		//开机即开启BQ24650  防止接入空电电池时无法充电
	{
		RunOnce = TRUE;
		GPIO_InitStructure.GPIO_Pin = BQ24650_EN_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(BQ24650_EN_TYPE, &GPIO_InitStructure);
		BQ24650_ENABLE();
	}

//	GPIO_PinAFConfig(GPIOA, GPIO_PinSource13, GPIO_AF_SWJ);

//	GPIO_PinAFConfig(GPIOA, GPIO_PinSource14, GPIO_AF_SWJ);
	
	Task_Create(TaskForRunLED, LED_DEFAULT_TIME);
	
	Task_Create(TaskForNetLED, LED_DEFAULT_TIME);
}

void TaskForLEDBlinkRun(void)
{
	Task_Create(TaskForRunLED, LED_DEFAULT_TIME);
	
	Task_Create(TaskForNetLED, LED_DEFAULT_TIME);
	
	GPIO_ToggleBits(RUN_LED_GPIO_TYPE,RUN_LED_GPIO_PIN);	
}

void TaskForLEDBlinkEnd(void)
{
	Task_Kill(TaskForRunLED);
	
	Task_Kill(TaskForNetLED);
}

void WakeUp_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC | RCC_AHBPeriph_GPIOH, ENABLE);
	
	#ifdef		CAMERA_VERSION_V1_1
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 |GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);	
	#endif
	
	#ifdef		CAMERA_VERSION_V1_2
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2  | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	#endif
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOC, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOH, &GPIO_InitStructure);	
				
	GPIO_InitStructure.GPIO_Pin = BAT_FULL_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(BAT_FULL_TYPE, &GPIO_InitStructure);	
	
	if(GetHi3519LinkFlag())	//找到核心板
	{
		GPIO_InitStructure.GPIO_Pin = NET_LED_GPIO_PIN;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(NET_LED_GPIO_TYPE, &GPIO_InitStructure);
		/* Force a low level on LEDs*/ 	
		GPIO_SetBits(NET_LED_GPIO_TYPE,NET_LED_GPIO_PIN);	
		
		if(GetMe909Status() == MeNoModule)	//找不到4G
		{
			GPIO_InitStructure.GPIO_Pin = RUN_LED_GPIO_PIN;	
			GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
			GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
			GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
			GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
			GPIO_Init(RUN_LED_GPIO_TYPE, &GPIO_InitStructure);

			GPIO_ResetBits(RUN_LED_GPIO_TYPE,RUN_LED_GPIO_PIN);
			
			#ifdef		CAMERA_VERSION_V1_2
			GPIO_InitStructure.GPIO_Pin = BLUE_LED_GPIO_PIN ;	
			GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
			GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
			GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
			GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
			GPIO_Init(BLUE_LED_GPIO_TYPE, &GPIO_InitStructure);

			GPIO_SetBits(BLUE_LED_GPIO_TYPE,BLUE_LED_GPIO_PIN);
			#endif
		}
		else
		{
			GPIO_InitStructure.GPIO_Pin = RUN_LED_GPIO_PIN;	
			GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
			GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
			GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
			GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
			GPIO_Init(RUN_LED_GPIO_TYPE, &GPIO_InitStructure);

			GPIO_SetBits(RUN_LED_GPIO_TYPE,RUN_LED_GPIO_PIN);
			
			#ifdef		CAMERA_VERSION_V1_2
			GPIO_InitStructure.GPIO_Pin = BLUE_LED_GPIO_PIN ;	
			GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
			GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
			GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
			GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
			GPIO_Init(BLUE_LED_GPIO_TYPE, &GPIO_InitStructure);

			GPIO_SetBits(BLUE_LED_GPIO_TYPE,BLUE_LED_GPIO_PIN);
			#endif
		}
	}
	else	//找不到核心板
	{
		GPIO_InitStructure.GPIO_Pin = NET_LED_GPIO_PIN;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(NET_LED_GPIO_TYPE, &GPIO_InitStructure);
		
		GPIO_SetBits(NET_LED_GPIO_TYPE,NET_LED_GPIO_PIN);
		
		GPIO_InitStructure.GPIO_Pin = RUN_LED_GPIO_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(RUN_LED_GPIO_TYPE, &GPIO_InitStructure);

		GPIO_SetBits(RUN_LED_GPIO_TYPE,RUN_LED_GPIO_PIN);
		
		#ifdef		CAMERA_VERSION_V1_2
		GPIO_InitStructure.GPIO_Pin = BLUE_LED_GPIO_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(BLUE_LED_GPIO_TYPE, &GPIO_InitStructure);

		GPIO_SetBits(BLUE_LED_GPIO_TYPE,BLUE_LED_GPIO_PIN);
		#endif
	}
}

void Stop_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC | RCC_AHBPeriph_GPIOH, ENABLE);

	#ifdef		CAMERA_VERSION_V1_1
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13| GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);	
	#endif
	
	#ifdef		CAMERA_VERSION_V1_2
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	#endif
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOC, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOH, &GPIO_InitStructure);	
		
	GPIO_InitStructure.GPIO_Pin = BAT_FULL_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(BAT_FULL_TYPE, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = BQ24650_EN_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(BQ24650_EN_TYPE, &GPIO_InitStructure);
	BQ24650_ENABLE();
		
}



