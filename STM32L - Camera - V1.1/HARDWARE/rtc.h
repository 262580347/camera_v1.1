
#ifndef __RTC_H
#define __RTC_H


void RTC_Config_Init(void);

void RTC_AlarmConfig(void);

void RTC_Updata(unsigned short nMain100ms);

void RTC_TimeShow(void);

void RTC_SetAlarmA(unsigned char Second);

unsigned int GetRTCSecond(void);

unsigned int DifferenceOfRTCTime(unsigned int NewTime, unsigned int OldTime);

void SysSetRTCTime(unsigned char Year, unsigned char Month, unsigned char Date, unsigned char Hours, unsigned char Minutes, unsigned char Seconds);

unsigned char GetSynchronizationTimeFlag(void);

void SetSynchronizationTimeFlag(unsigned char isTrue);

void RTC_EasyShowTime(void);
#endif /* __RTC_H */

