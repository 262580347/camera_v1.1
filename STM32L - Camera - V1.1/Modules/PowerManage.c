#include "main.h"
#include "PowerManage.h"

#define		VOLBUFCOUNT		10

static unsigned short s_Battery_Vol = 0;

static float s_VolBuff[VOLBUFCOUNT];

static unsigned char s_PowerDetectFlag = TRUE;

static unsigned char s_BQ24650_EN_Flag = 1;

static unsigned char s_ForcedChargeFlag = 0;

static unsigned int TimerID = 0;

BAT_STATUS BatStatus;	//电池电压检测状态

u8 s_CfgData[sizeof(CHARGING_CFG)] ;

CHARGING_CFG	ChargingConfig;
CHARGING_CFG	*p_this = NULL;

CHARGING_CFG* GetChargingStatus(void)
{
	return p_this;
}

unsigned char GetForcedChargeFlag(void)
{
	return s_ForcedChargeFlag;
}

unsigned char GetBQ24650ENFlag(void)
{
	return s_BQ24650_EN_Flag;
}

void SetBQ24650ENFlag(unsigned char isTrue)
{
	s_BQ24650_EN_Flag = isTrue;
}

void StartGetBatVol(void)
{
	s_PowerDetectFlag = TRUE;
	
	BatStatus = BAT_WORKING;
}

void ReadBQ24650Flag(void)
{		
	p_this = &ChargingConfig;
	
	BatStatus = BAT_IDLE;
	
	if (Check_Area_Valid(BQ_FLAG_ADDR))
	{
		EEPROM_ReadBytes(BQ_FLAG_ADDR, s_CfgData, sizeof(CHARGING_CFG));
		
		p_this = (CHARGING_CFG *)s_CfgData;
		
		ChargingConfig.Magic 		= p_this->Magic;
		ChargingConfig.Length 		= p_this->Length;
		ChargingConfig.Chksum 		= p_this->Chksum;
		ChargingConfig.Enable 		= p_this->Enable;
		ChargingConfig.ForceFlag 	= p_this->ForceFlag;
		
	}
	else
	{
		ChargingConfig.Magic 		= 0x55;
		ChargingConfig.Length 		= 0X02;
		ChargingConfig.Enable 		= BQ24650_ENABLE_FLAG;
		ChargingConfig.ForceFlag 	= FORCED_CHARGE_DISABLE;
		ChargingConfig.Chksum = Calc_Checksum((unsigned char *)&ChargingConfig.Enable, 2);
		EEPROM_WriteBytes(BQ_FLAG_ADDR, (unsigned char *)&ChargingConfig, sizeof(ChargingConfig));
		u1_printf("\r\n Initialize the chip for the first time and write the charging configuration\r\n");
	}
	
	if(ChargingConfig.ForceFlag == FORCED_CHARGE_ENABLE)
	{
		s_ForcedChargeFlag = 1;
		u1_printf("\r\n Forced charge On\r\n");
	}
	else if(ChargingConfig.ForceFlag == FORCED_CHARGE_DISABLE)
	{
		s_ForcedChargeFlag = 0;
		u1_printf("\r\n Forced charge Off\r\n");
	}
	
	if(ChargingConfig.Enable == BQ24650_ENABLE_FLAG)
	{
		s_BQ24650_EN_Flag = 1;
		u1_printf("\r\n Open the charging\r\n");
	}
	else if(ChargingConfig.Enable == BQ24650_DISABLE_FLAG)
	{
		s_BQ24650_EN_Flag = 0;
		u1_printf("\r\n Close the charging\r\n");
	}
	else
	{
		s_BQ24650_EN_Flag = 0;
		u1_printf("\r\n First\r\n");
	}	
}

unsigned short Get_Battery_Vol(void)
{
	if(s_BQ24650_EN_Flag)
	{
		return (s_Battery_Vol | 0x0001);
	}
	else
	{
		return (s_Battery_Vol & 0xfffe);		
	}
}

static void TimeToGetBatVol(void)
{
	u16 ADCdata[10], i, Sum_ADCdata = 0, BatVol = 0;
	static u8 s_BatFullCount = 0, s_DetCount = 0, s_DetNum = 0, s_First = FALSE;
	
	RCC_HSICmd(ENABLE);
					
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	
	/* Enable ADC1 */
	ADC_Cmd(ADC1, ENABLE);
	
	/* Wait until ADC1 ON status */
	while (ADC_GetFlagStatus(ADC1, ADC_FLAG_ADONS) == RESET)
	{
	}
	
	for(i=0; i<10; i++)
	{
		/* ADC1 regular channel5 or channel1 configuration */
		ADC_RegularChannelConfig(ADC1, ADC_Channel_19, 1, ADC_SampleTime_384Cycles);
		
		/* Start ADC1 Software Conversion */
		ADC_SoftwareStartConv(ADC1);

		/* Wait until ADC Channel 5 or 1 end of conversion */
		while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET)
		{	
		}	
		
		ADCdata[i] = ADC_GetConversionValue(ADC1);
	}
			
	for(i=0; i<10; i++)
	{
		Sum_ADCdata += ADCdata[i];
	}
	
	Sum_ADCdata /= 10;
	
	BatVol = (Sum_ADCdata*4.0)*3300/4095; 
						
	if(s_DetCount < 3)
	{
		s_VolBuff[s_DetCount] = BatVol;
		s_Battery_Vol = BatVol;
	}
	else
	{
		s_VolBuff[s_DetNum] = BatVol;
		s_Battery_Vol = Mid_Filter(s_VolBuff, s_DetCount);
	}

	u1_printf("\r\n Vol:%dmV\r\n", s_Battery_Vol);		

	
	s_DetCount++;
	s_DetNum++;
	
	if(s_DetCount >= VOLBUFCOUNT)
	{
		s_DetCount = VOLBUFCOUNT;
	}
	
	if(s_DetNum >= VOLBUFCOUNT)
	{
		s_DetNum = 0;
	}	
	
	Adc_Reset();

	if(s_Battery_Vol < 8100)//电池需要充电
	{
		s_BQ24650_EN_Flag = 1;
	}
	
//	if(s_Battery_Vol > 8400)
//	{
//		s_BatFullCount++;
//	}
//	else
//	{
//		s_BatFullCount = 0;
//	}
//	
//	if(s_BatFullCount >= 3)		//连续3次唤醒检测到电压大于8.4V，关闭充电电路，防止出现芯片一直给电池充电的问题
//	{
//		printf("\r\n High battery, protect the battery, cut off charging\r\n");
//		s_BatFullCount = 0;
//		s_BQ24650_EN_Flag = 0;
//	}
	
	if(GPIO_ReadInputDataBit(BAT_FULL_TYPE, BAT_FULL_PIN) == Bit_RESET)//电池已满
	{
		s_BQ24650_EN_Flag = 0;	//关闭充电芯片的唯一入口
		u1_printf("\r\n Battery full\r\n");
	}
	
	if(s_BQ24650_EN_Flag)
	{
		BQ24650_ENABLE();
		p_this->Enable = BQ24650_ENABLE_FLAG;
		p_this->Chksum = Calc_Checksum((unsigned char *)&p_this->Enable, 2);
		EEPROM_WriteBytes(BQ_FLAG_ADDR, (unsigned char *)p_this, sizeof(CHARGING_CFG));
	}
//	else
//	{
//		BQ24650_DISABLE();
//		p_this->Enable = BQ24650_DISABLE_FLAG;
//		p_this->Chksum = Calc_Checksum((unsigned char *)&p_this->Enable, 2);
//		EEPROM_WriteBytes(BQ_FLAG_ADDR, (unsigned char *)p_this, sizeof(CHARGING_CFG));
//	}
	
	BatStatus = BAT_IDLE;
	
	KillTimer(TimerID);
	
	if(s_First == FALSE)
	{
		s_First = TRUE;
		SetLogErrCode(LOG_CODE_START);
		StoreOperationalData();
		ClearLogErrCode(LOG_CODE_START);
	}
}

static void TaskForBatVolDet(void)
{
	if(s_PowerDetectFlag)
	{
		s_PowerDetectFlag = FALSE;
		
		Adc_Init();	

		Open_AdcChannel();
		
		TimerID = SetTimer(200, TimeToGetBatVol);
		
		BatStatus = BAT_WORKING;
	}
}

void BatVolDet_Init(void)
{
	Adc_Init();	

	Open_AdcChannel();
	
	StartGetBatVol();
	
	Task_Create(TaskForBatVolDet, 40);
}

