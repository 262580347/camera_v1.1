#ifndef		_GPSDRIVE_H_
#define		_GPSDRIVE_H_

#define		GPSPRODUCTINFORMATION		0x01
#define		GPSANTTEST					0x02
#define		GPSCONFING					0x03
#define		GPSGETLOCATION				0x04




void GPSPowerOn(void);

void GPSPowerOff(void);

void GetPosition(float *Latitude, float *Longitude);

unsigned char GetGPSDataFlag(void);

float GetAltitude(void);

unsigned char GetGPSPowerFlag(void);
	
void GPSPowerOnNoTask(void);

void SetGPSDataFlag(unsigned char isTrue);

void SetPosition(float Latitude, float Longitude);

void SetGPSShowInfo(unsigned char isTrue);

unsigned char GetGPSInitSuccess(void);

void GPSCommunicationOn(void);

void GPSCommunicationOff(void);

#endif



