#include "Hi3519.h"
#include "main.h"

#ifdef		CAMERA_VERSION_V1_1

#define		Hi3519_POWER_PIN	GPIO_Pin_4
#define		Hi3519_POWER_TYPE	GPIOA

#endif

#ifdef		CAMERA_VERSION_V1_2

#define		Hi3519_POWER_PIN	GPIO_Pin_6
#define		Hi3519_POWER_TYPE	GPIOA

#endif

void Hi3519PowerControl(unsigned char IsTrue)
{
	GPIO_InitTypeDef GPIO_InitStructure;
				
	if(IsTrue)
	{
		GPIO_InitStructure.GPIO_Pin = Hi3519_POWER_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(Hi3519_POWER_TYPE, &GPIO_InitStructure);
		
		GPIO_SetBits(Hi3519_POWER_TYPE,Hi3519_POWER_PIN);
	}
	else
	{
		GPIO_InitStructure.GPIO_Pin = Hi3519_POWER_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(Hi3519_POWER_TYPE, &GPIO_InitStructure);
		
		GPIO_ResetBits(Hi3519_POWER_TYPE,Hi3519_POWER_PIN);
		
	}
}

//static void TaskForHi3519Communication(void)
//{
//	
//}

void Hi3519PowerOn(void)
{
	#ifdef		CAMERA_VERSION_V1_1

	BQ24650_DISABLE();

	#endif

	Hi3519PowerControl(TRUE);	//打开Hi3519电源
	
	delay_ms(500);
	
	InitcJsonConfig();
	
//	USART2_Config(115200);	//GPS波特率为9600
			
//	Task_Create(TaskForHi3519Communication, 10);

}

void Hi3519PowerOff(void)
{
	Hi3519PowerControl(FALSE);
	
//	Task_Kill(TaskForHi3519Communication);
	DeinitcJsonConfig();
	
	USART_Cmd(USART2,DISABLE); 
	USART_DeInit(USART2);	
	
}












