#include "main.h"
#include "control.h"
#include "sys.h"

#define RUN_LED_PORT PAout(14)	//运作灯LED端口

//==============================================================================
//	led闪烁时间
//==============================================================================
#define FLICKER_NOT		0		//不闪烁
#define FLICKER_100MS	1		//100MS闪烁
#define FLICKER_200MS	2		//200MS闪烁
#define FLICKER_300MS	3		//300MS闪烁
#define FLICKER_400MS	4		//400MS闪烁
#define FLICKER_500MS	5		//500MS闪烁
#define FLICKER_800MS	8		//800MS闪烁
#define FLICKER_1S		10		//1000MS闪烁

//============================================
//全局变量定义
//============================================
UINT8 g_ucRunLedFlickerTime = FLICKER_500MS;	//500MS

unsigned char  g_ucDeviceResetFlalg = 0;

void SetDeviceResetFlag( UINT8 ucFlag )
{
	g_ucDeviceResetFlalg = ucFlag;
}
UINT8 GetDeviceResetFlag( void )
{
	return g_ucDeviceResetFlalg;
}


//===========================================================
//LED运行灯闪烁控制
//===========================================================

void RunLedOn( void )
{
	RUN_LED_PORT = 1;
}

void RunLedOff( void )
{
	RUN_LED_PORT = 0;
}



void InitRunLedPort( void )
{
	RCC->APB2ENR|=1<<2;    //使能PORTA时钟	

	#if( (TH_SENSOR_EN == 1) || (BMP_SENOR_EN == 1) )

	GPIOA->CRL&=0XFFFFFF0F; 
	GPIOA->CRL|=0X00000010;	//PA.1 推挽输出   	 
    GPIOA->ODR|=1<<1;      	//PA.1 输出高
	
	#elif ( MAX44009_EN == 1 )
	
	GPIOA->CRH&=0XF0FFFFFF; 
	GPIOA->CRH|=0X01000000;	//PA.14 推挽输出   	 
    GPIOA->ODR|=1<<14;      //PA.14 输出高
	
	#endif
	
}



void RunLedFlash( void )
{
	static UINT8 s_ucState = 1;
	if( s_ucState == 1 )
	{
		s_ucState = 0;
		RunLedOn();
	}
	else
	{
		s_ucState = 1;
		RunLedOff();
	}
}

void RunLedControl( UINT16 nMain100ms )
{
	static UINT16 s_nLast = 0;
	static UINT8 s_ucFirst = 1;
	
	if( s_ucFirst == 1 )
	{
		s_ucFirst = 0;
		InitRunLedPort();
		s_nLast = nMain100ms;
		return;
	}

	if( 1 )	//传感器正常	1S闪烁一次
	{
		if( CalculateTime(nMain100ms,s_nLast) >= FLICKER_1S)
		{
			s_nLast = nMain100ms;
			RunLedFlash();
		}
	}
	else		//传感器异常，100MS闪烁一次
	{
		if( CalculateTime(nMain100ms,s_nLast) >= FLICKER_100MS)
		{
			s_nLast = nMain100ms;
			RunLedFlash();
		}
	}
	return;
}


//======================================
//看门狗控制
//======================================
void UpdateWatchdog( UINT16 nMain100ms )
{
	static UINT16	s_nLast = 0;
//	static UINT8	s_ucFirst = 1;


	if ( GetDeviceResetFlag() == 0 )
	{
		s_nLast = nMain100ms;
		WatchDog();
	}
	else
	{
		if( CalculateTime(nMain100ms,s_nLast)>3 )
		{
			s_nLast = nMain100ms;
		}

	}
}

void ControlProcess(UINT16 nMain100ms)
{
	RunLedControl( nMain100ms );	//运行指示灯控制
	
	UpdateWatchdog( nMain100ms );	//喂狗
	
	DacControl( nMain100ms );		//将SHT3X读取的温湿度数值通过DAC接口输出
	
}










