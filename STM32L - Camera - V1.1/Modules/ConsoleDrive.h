#ifndef		_CONSOLEDRIVE_H_
#define		_CONSOLEDRIVE_H_



#define		PRESET_INDX		5	

#define		SPEED_SLOW		15
#define		SPEED_MID		16
#define		SPEED_FAST		17
#define		AUTOPOINT		18		//自动巡航点

#define		MAX_HOR_PLUSE		99060	//99474	满量程 		390 Pluse/度	0~254 度
#define		MAX_VER_PLUSE		17995	//18285	满量程		305 Pluse/度	0~59  度

#define		MAX_HOR_ANGLE		254
#define		MAX_VER_ANGLE		60

#define		HOR_ANGLETOPLUSE	390u
#define		VER_ANGLETOPLUSE	305u

//云台指令索引
#define		MOTOR_RUN_UP 		1
#define		MOTOR_RUN_DOWN  	2
#define		MOTOR_RUN_LEFT 	 	3
#define		MOTOR_RUN_RIGHT    	4
#define		MOTOR_STOP			5
#define		MOTOR_AUTO			6
#define		MOTOR_ANGLE			7
#define		MOTOR_STOP_ACK		8

#define		MOTOR_RUN_MODE		55

typedef __packed struct
{
	unsigned char 			Magic; 	//0x55
	unsigned short 			Length;	//存储内容长度	
	unsigned char 			Chksum;	//校验和
	unsigned short			HorizontalAngle[20];	//水平角度
	unsigned short			VerticalAngle[20];		//垂直角度
	unsigned char			PhotoPointNum;
	unsigned short			PhotoInterval;
	unsigned char			StartPhotoTime[3];
	unsigned char			EndPhotoTime[3];
}ANGLESTORE_CFG;

void SendConsoleCMD(unsigned char ConsoleCMD, unsigned short HorAngle, unsigned short VerAngle);

void SetPresetPoint(unsigned char Num);

void GotoPresetPoint(unsigned char Num);

void DelPresetPoint(unsigned char Num);

void ConsoleUp(void);

void ConsoleDown(void);

void ConsoleLeft(void);

void ConsoleRight(void);

void ConsoleStop(void);

void MotorPowerOn_NoRun(void);

void MotorPowerOn(void);

void MotorTestPowerOn(void);

void MotorGotoAnglePowerOn(unsigned short HorAngle, unsigned short VerAngle);

void MotorPowerOff(void);

unsigned char GetConsolePosition(void);

void MotorPowerControl(unsigned char IsTrue);

unsigned char GetConsolePowerFlag(void);

void SetConsolePosition(unsigned char Point);

unsigned char ConsoleAngleInit(void);

void ReadStoreAngle(void);

void WriteNowAngle(unsigned int HorizontalAngle, unsigned int VerticalAngle, unsigned char Num);

unsigned char GetMotorStopFlag(void);
void SetMotorStopFlag(unsigned char isTrue);

unsigned short GetHorAngle(void);
unsigned short GetVerAngle(void);

ANGLESTORE_CFG *GetPhotoConfig(void);

void SetPhotoConfig(ANGLESTORE_CFG *p_Config);

void ConsoleRunModeisNormal(void);

void ConsoleRunModeisTest(void);

#endif

